# Create a ssl certificate

1.  Create a record in /etc/hosts with the following:

    127.0.0.1 traveleala.org
    127.0.0.1 api.traveleala.org

2.  Execute this command

    openssl req -x509 -sha256 -nodes -newkey rsa:2048 -days 365 -keyout traveleala.key -out traveleala.crt

3.  Set this content

    Country Name (2 letter code) [AU]: AR
    State or Province Name (full name) [Some-State]: san luis
    Locality Name (eg, city) []: merlo
    Organization Name (eg, company) [Internet Widgits Pty Ltd]: traveleala
    Organizational Unit Name (eg, section) []: traveleala
    Common Name (e.g. server FQDN or YOUR name) []: \*.traveleala.org
    Email Address []: info@traveleala.org

4.  View content of certificate

    openssl x509 -text -noout -in traveleala.crt

5.  Add into virtual host

    server {
    listen \*:443 ssl http2;
    listen [::]:443 ssl http2;
    server_name api.traveleala.org;
    root /var/www/traveleala-backend/public;

        add_header X-Frame-Options "SAMEORIGIN";
        add_header X-XSS-Protection "1; mode=block";
        add_header X-Content-Type-Options "nosniff";

        index index.html index.htm index.php;
        error_log /var/www/traveleala-backend/storage/logs/php-error.log;

        charset utf-8;
        client_max_body_size 20m;

        ssl                  on;
        ssl_certificate      /var/www/traveleala-backend/ssl/traveleala.crt;
        ssl_certificate_key  /var/www/traveleala-backend/ssl/traveleala.key;
        ssl_ciphers          HIGH:!aNULL:!MD5;

        location / {
            try_files $uri $uri/ /index.php?$query_string;
        }

        location = /favicon.ico { access_log off; log_not_found off; }
        location = /robots.txt  { access_log off; log_not_found off; }

        error_page 404 /index.php;

        location ~ \.php$ {
            fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
            include fastcgi_params;
        }

        location ~ /\.(?!well-known).* {
            deny all;
        }

    }

    server {
    listen 80;
    listen [::]:80;

        server_name traveleala.org www.traveleala.org;

        return 302 https://$server_name$request_uri;

    }

    server {
    listen 80;
    listen [::]:80;

        server_name api.traveleala.org;

        return 302 https://$server_name$request_uri;

    }

    server {
    server_name traveleala.org;
    listen \*:443 ssl http2;
    listen [::]:443 ssl http2;

        add_header X-Frame-Options "SAMEORIGIN";
        add_header X-Content-Type-Options nosniff;
        add_header X-XSS-Protection "1; mode=block";

        #error_log stderr debug;
        #access_log /dev/stdout;

        #root /app/nada/public;
        #index index.html;

        client_max_body_size 20m;

        ssl                  on;
        ssl_certificate      /var/www/traveleala-backend/ssl/traveleala.crt;
        ssl_certificate_key  /var/www/traveleala-backend/ssl/traveleala.key;
        ssl_ciphers          HIGH:!aNULL:!MD5;

        location / {
            proxy_pass  https://localhost:8080;
            proxy_set_header Host localhost;
            proxy_set_header Origin localhost;
            proxy_hide_header Access-Control-Allow-Origin;
            add_header Access-Control-Allow-Origin "https://traveleala.org";
        }

        location /sockjs-node/ {
            proxy_pass https://localhost:8080;
            proxy_set_header Host localhost;
            proxy_set_header Origin localhost;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "Upgrade";
            proxy_hide_header Access-Control-Allow-Origin;
            add_header Access-Control-Allow-Origin "https://traveleala.org";
        }

        location /api/ {
            rewrite ^/api/(.*)$ /$1 break;
            proxy_pass  https://api.traveleala.org; #$1$is_args$args;
            proxy_set_header Host localhost;
            proxy_set_header Origin localhost;
            proxy_hide_header Access-Control-Allow-Origin;
            add_header Access-Control-Allow-Origin "https://traveleala.org";
        }

    }
