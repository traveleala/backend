# Commands

## Configuration

1. Define in cronjob

```
* * * * * cd /var/www/traveleala-backend && php artisan schedule:run >> /dev/null 2>&1
```

# Supervisor

## Commands

## Start supervisor

    sudo supervisorctl reread && sudo supervisorctl update && sudo supervisorctl start traveleala-worker:\*

## Config

Put this configuration in /etc/supervisor/conf.d/traveleala-worker.conf

    [program:traveleala-worker]
    process_name=%(program_name)s_%(process_num)02d
    command=php /var/www/travel-backend/artisan queue:work --sleep=3 --tries=3
    autostart=true
    autorestart=true
    user=quevlu
    numprocs=8
    redirect_stderr=true
    stdout_logfile=/var/log/traveleala-worker.log
