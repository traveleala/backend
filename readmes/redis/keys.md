# Public

## Activities

relateds: activities:relateds
recents: activities:recents
top: activities:top
stats: activities:stats:{userId}:year:{year}
total: activities:stats:total:{userId}
byUsername: activities:username:{username}:{page}
myActivities: activities:me:{userId}:{page}

### Difficulties

get: activities:difficulties
getByName: activities:difficulties:{name}

### Favorites

get: activities:favorites:{userId}:{page}

### Publishes

get: activities:publishes:types
getByName: activities:publishes:types:{name}

### Reverse geocoding

geocoding: activities:geocoding:{checksum}

### States

getByName: activities:states:{name}

### Types

get: activities:types
getByName: activities:types:{name}

### Visits

perMonth: visits:stats:{id}:year:{year}
total: visits:stats:total:{id}

## Qualification

total: qualification:stats:total:{userId}
totalByActivity: qualification:stats:total:{userId}:{activityId?}
stats: qualification:stats:{userId}:year:{year}:{activityId?}

## Qualifications

get: qualifications:{activityId}:{page}
stars: qualifications:{activityId}:stars
difficulties: qualifications:{activityId}:difficulties
total: qualifications:stats:total:{userId}:{activityId?}
stats: qualifications:stats:{userId}:year:{year}:{activityId?}

## Users

login: users:{userId}
token: tokens:{checksum}

### Notifications

get: notifications:{userId}:{page}

#### Types

getByName: notifications:types:{name}

### Followers

get: followers:{userId}:{page}

### Followings

get: followings:{userId}:{page}

### Profiles

getByUsername: profiles:{username}

### Roles

get: roles
getByName: roles:{name}

---

# Admin

## Report

reports: admin:reports:{page}

### Action

byEntity: reports:actions:entity:{entity}
byName: reports:actions:name:{name}

### States

byName: reports:states:{name}

### Types

get: reports:types
getByName: reports:types:{name}

## Users

getUsers: admin:users:{page}
