# Mercado pago

## Pre requisites

1. Install ngrok from https://ngrok.com/download

2. Execute ngrok http 443

3. Get the url from vhost created like this with version prefix of api

    https://79875141.ngrok.io/v1/

## Steps

1. Create two users one buyer and one seller with your prod access token

    curl -X POST \
    -H "Content-Type: application/json" \
    "https://api.mercadopago.com/users/test_user?access_token=PROD_ACCESS_TOKEN" \
    -d '{"site_id":"MLA"}'

2. Log in with the user seller, in the part of credentials copy de production access token and replace the token in the following .env section

    ACCESS_TOKEN_MP="NEW ACCESS TOKEN"

3. Set the url of ipn service with ngrok generated and set payment and merchant_order

    https://79875141.ngrok.io/v1/activities/subscriptions/notifications

4. Test with your buyer user account with the following credit cards

    Tarjeta Número CVV Vencimiento
    Mastercard 5031 7557 3453 0604 123 11/25
    Visa 4170 0688 1010 8020 123 11/25
    American 3711 8030 3257 522 1234 11/25s

## Examples

1. Syntax for user created buyer or seller

    Seller
    {"id":572256671,"nickname":"TT936497","password":"qatest5897","site_status":"active","email":"test_user_60136399@testuser.com"}

    Buyer
    {"id":572263474,"nickname":"TETE8758100","password":"qatest4547","site_status":"active","email":"test_user_88474938@testuser.com"}
