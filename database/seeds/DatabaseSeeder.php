<?php

use Database\Seeds\QualificationSeeder;
use Illuminate\Database\Seeder;
use Database\Seeds\UserSeeder;
use Database\Seeds\ActivitySeeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            UserSeeder::class,
            ActivitySeeder::class,
            QualificationSeeder::class
        ]);
    }
}
