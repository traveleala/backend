<?php

namespace Database\Seeds;

use App\Repositories\v1\Qualification\ImageRepository;
use Illuminate\Database\Seeder;
use App\Repositories\v1\Qualification\QualificationRepository;
use App\Repositories\v1\Qualification\VoteRepository;

class QualificationSeeder extends Seeder
{
    public function run()
    {
        factory(QualificationRepository::$model, 5000)->create()->each(function ($activityQualification) {
            $activityQualification->images()->saveMany(factory(ImageRepository::$model, 3)->make([
                'activity_qualification_id' => $activityQualification->id
            ]));

            $activityQualification->votes()->save(factory(VoteRepository::$model)->make([
                'user_id' => $activityQualification->user_id
            ]));
        });
    }
}
