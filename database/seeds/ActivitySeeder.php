<?php

namespace Database\Seeds;

use Illuminate\Database\Seeder;
use App\Repositories\v1\Activity\ActivityRepository;
use App\Repositories\v1\Activity\CharacteristicRepository;
use App\Repositories\v1\Activity\ContactRepository;
use App\Repositories\v1\Activity\ImageRepository;
use App\Repositories\v1\Activity\SubscriptionRepository;

class ActivitySeeder extends Seeder
{
    public function run()
    {
        factory(ActivityRepository::$model, 1000)->create()->each(function ($activity) {
            $activity->contact()->save(factory(ContactRepository::$model)->make());
            $activity->subscription()->save(factory(SubscriptionRepository::$model)->make([
                'activity_publish_type_id' => 1,
            ]));

            $activity->images()->saveMany(factory(ImageRepository::$model, 10)->make([
                'activity_id' => $activity->id
            ]));

            $activity->characteristics()->saveMany(factory(CharacteristicRepository::$model, 9)->make([
                'activity_id' => $activity->id
            ]));
        });
    }
}
