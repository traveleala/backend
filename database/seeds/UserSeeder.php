<?php

namespace Database\Seeds;

use Illuminate\Database\Seeder;
use App\Repositories\v1\User\UserRepository;
use App\Repositories\v1\User\ProfileRepository;
use App\Repositories\v1\User\RoleRepository;

class UserSeeder extends Seeder
{
    public function run()
    {
        factory(UserRepository::$model)->create([
            'email' => 'info@traveleala.com',
            'password' => password('info@traveleala.com'),
            'user_role_id' => RoleRepository::getAdminRole()->id
        ])->each(function ($user) {
            $user->profile()->save(factory(ProfileRepository::$model)->make());
        });

        factory(UserRepository::$model, 50)->create()->each(function ($user) {
            $user->profile()->save(factory(ProfileRepository::$model)->make());
        });
    }
}
