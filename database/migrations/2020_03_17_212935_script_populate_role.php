<?php

use Illuminate\Database\Migrations\Migration;
use App\Repositories\v1\User\RoleRepository;
use Illuminate\Support\Facades\Schema;
use App\Values\v1\UserRoleValues;

class ScriptPopulateRole extends Migration
{

    public function up()
    {
        RoleRepository::insert([
            [
                'name' => UserRoleValues::ADMIN_ROLE
            ],
            [
                'name' => UserRoleValues::PUBLISHER_ROLE
            ],
            [
                'name' => UserRoleValues::USER_ROLE
            ],
            [
                'name' => UserRoleValues::SPECIAL_USER_ROLE
            ]
        ]);
    }


    public function down()
    {
        Schema::disableForeignKeyConstraints();
        RoleRepository::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
