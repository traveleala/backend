<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ActivityImagesTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('activity_image')) {
            Schema::create('activity_image', function (Blueprint $table) {
                $table->id();
                $table->string('image_url');
                $table->unsignedBigInteger('activity_id');
                $table->foreign('activity_id')->references('id')->on('activity')->onDelete('cascade');
                $table->timestamps();
            });
        }
    }


    public function down()
    {
        Schema::dropIfExists('activity_image');
    }
}
