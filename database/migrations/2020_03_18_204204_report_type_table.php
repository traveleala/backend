<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReportTypeTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('report_type')) {
            Schema::create('report_type', function (Blueprint $table) {
                $table->id();
                $table->string('name')->unique()->index();
            });
        }
    }


    public function down()
    {
        Schema::dropIfExists('report_type');
    }
}
