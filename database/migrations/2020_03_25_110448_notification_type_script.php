<?php

use Illuminate\Database\Migrations\Migration;
use App\Values\v1\NotificationTypeValues;
use Illuminate\Support\Facades\Schema;
use App\Repositories\v1\Notification\TypeRepository;

class NotificationTypeScript extends Migration
{

    public function up()
    {
        TypeRepository::insert([
            [
                'name' => NotificationTypeValues::ACTIVITY_DELETED,
                'description' => 'La actividad {title} se ha eliminado por no cumplir con las normas.',
                'send_email' => false,
            ],
            [
                'name' => NotificationTypeValues::QUALIFICATION_DELETED,
                'description' => 'La calificación que realizaste en la actividad {title} se elimino porque no cumplia con las normas',
                'send_email' => false,
            ],
            [
                'name' => NotificationTypeValues::USER_DELETED,
                'description' => 'Usuario eliminado',
                'send_email' => true,
            ],
            [
                'name' => NotificationTypeValues::FOLLOW_USER,
                'description' => 'El usuario {username} te esta siguiendo',
                'send_email' => false,
            ],
            [
                'name' => NotificationTypeValues::QUALIFICATION_IN_ACTIVITY,
                'description' => 'El usuario {username} realizo una calificación en tu actividad {title}',
                'send_email' => false,
            ],


        ]);
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        TypeRepository::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
