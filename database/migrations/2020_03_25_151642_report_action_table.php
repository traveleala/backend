<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReportActionTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('report_action')) {
            Schema::create('report_action', function (Blueprint $table) {
                $table->id();
                $table->string('name')->index();
                $table->string('entity')->index();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('report_action');
    }
}
