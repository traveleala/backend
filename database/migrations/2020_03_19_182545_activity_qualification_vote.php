<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ActivityQualificationVote extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('activity_qualification_vote')) {
            Schema::create('activity_qualification_vote', function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('user_id');
                $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');
                $table->unsignedBigInteger('activity_qualification_id');
                $table->foreign('activity_qualification_id')->references('id')->on('activity_qualification')->onDelete('cascade');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('activity_qualification_vote');
    }
}
