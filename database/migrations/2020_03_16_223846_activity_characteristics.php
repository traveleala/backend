<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ActivityCharacteristics extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('activity_characteristic')) {
            Schema::create('activity_characteristic', function (Blueprint $table) {
                $table->id();
                $table->string('concept')->index();
                $table->string('value')->index();
                $table->unsignedBigInteger('activity_id');
                $table->foreign('activity_id')->references('id')->on('activity')->onDelete('cascade');
                $table->timestamps();
            });
        }
    }


    public function down()
    {
        Schema::dropIfExists('activity_characteristic');
    }
}
