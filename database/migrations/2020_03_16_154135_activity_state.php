<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ActivityState extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('activity_state')) {
            Schema::create('activity_state', function (Blueprint $table) {
                $table->id();
                $table->string('name')->unique()->index();
            });
        }
    }


    public function down()
    {
        Schema::dropIfExists('activity_state');
    }
}
