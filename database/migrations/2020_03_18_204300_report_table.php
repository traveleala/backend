<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReportTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('report')) {
            Schema::create('report', function (Blueprint $table) {
                $table->id();

                $table->string('comment')->nullable();
                $table->unsignedBigInteger('user_id')->nullable();
                $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');

                $table->unsignedBigInteger('activity_id')->nullable();
                $table->foreign('activity_id')->references('id')->on('activity')->onDelete('cascade');

                $table->unsignedBigInteger('activity_qualification_id')->nullable();
                $table->foreign('activity_qualification_id')->references('id')->on('activity_qualification')->onDelete('cascade');

                $table->unsignedBigInteger('report_type_id');
                $table->foreign('report_type_id')->references('id')->on('report_type');

                $table->unsignedBigInteger('report_state_id');
                $table->foreign('report_state_id')->references('id')->on('report_state');

                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('report');
    }
}
