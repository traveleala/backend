<?php

use Illuminate\Database\Migrations\Migration;
use App\Repositories\v1\Activity\PublishTypeRepository;
use App\Values\v1\PublishTypeValues;
use Illuminate\Support\Facades\Schema;

class TypeOfPublishScript extends Migration
{

    public function up()
    {
        $createdAt = now();

        PublishTypeRepository::insert([
            [
                'name' => PublishTypeValues::BASIC_TYPE,
                'price' => 0,
                'min_month' => 1,
                'max_month' => 1,
                'discount' => 0,
                'created_at' => $createdAt,
                'updated_at' => $createdAt
            ],
            [
                'name' => PublishTypeValues::STANDARD_TYPE,
                'price' => 75,
                'min_month' => 2,
                'max_month' => 6,
                'discount' => 0.10,
                'created_at' => $createdAt,
                'updated_at' => $createdAt
            ],
            [
                'name' => PublishTypeValues::PREMIUM_TYPE,
                'price' => 150,
                'min_month' => 6,
                'max_month' => 12,
                'discount' => 0.20,
                'created_at' => $createdAt,
                'updated_at' => $createdAt
            ]
        ]);
    }


    public function down()
    {
        Schema::disableForeignKeyConstraints();
        PublishTypeRepository::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
