<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NotificationTypeTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('notification_type')) {
            Schema::create('notification_type', function (Blueprint $table) {
                $table->id();
                $table->string('name')->index()->unique();
                $table->string('description')->index()->unique();
                $table->boolean('send_email')->default(false);
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('notification_type');
    }
}
