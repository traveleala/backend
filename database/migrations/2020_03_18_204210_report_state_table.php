<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReportStateTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('report_state')) {
            Schema::create('report_state', function (Blueprint $table) {
                $table->id();
                $table->string('name')->unique()->index();
            });
        }
    }


    public function down()
    {
        Schema::dropIfExists('report_state');
    }
}
