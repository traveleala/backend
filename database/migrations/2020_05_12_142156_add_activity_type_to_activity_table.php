<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddActivityTypeToActivityTable extends Migration
{
    public function up()
    {
        Schema::table('activity', function (Blueprint $table) {
            $table->unsignedBigInteger('activity_type_id')->nullable()->after('activity_state_id');
            $table->foreign('activity_type_id')->references('id')->on('activity_type')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('activity', function (Blueprint $table) {
            $table->dropForeign('activity_activity_type_id_foreign');
            $table->dropColumn('activity_type_id');
        });
    }
}
