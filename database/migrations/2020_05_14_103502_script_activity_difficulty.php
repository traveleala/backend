<?php

use App\Repositories\v1\Activity\DifficultyRepository;
use App\Values\v1\DifficultyValues;
use Illuminate\Database\Migrations\Migration;

class ScriptActivityDifficulty extends Migration
{
    public function up()
    {
        DifficultyRepository::insert([
            [
                'name' => DifficultyValues::EASY_DIFFICULTY
            ],
            [
                'name' => DifficultyValues::MEDIUM_DIFFICULTY
            ],
            [
                'name' => DifficultyValues::HARD_DIFFICULTY
            ]
        ]);
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        DifficultyRepository::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
