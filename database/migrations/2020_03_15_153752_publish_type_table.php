<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PublishTypeTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('activity_publish_type')) {
            Schema::create('activity_publish_type', function (Blueprint $table) {
                $table->id();
                $table->string('name')->unique()->index();
                $table->integer('price')->index();
                $table->integer('min_month')->index();
                $table->integer('max_month')->index();
                $table->float('discount', 2, 2)->index();
                $table->timestamps();
            });
        }
    }


    public function down()
    {
        Schema::dropIfExists('activity_publish_type');
    }
}
