<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ActivityPublishTypeIdInActivity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activity', function (Blueprint $table) {
            $table->unsignedBigInteger('activity_publish_type_id')->after('activity_difficulty_id');
            $table->foreign('activity_publish_type_id')->references('id')->on('activity_publish_type')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activity', function (Blueprint $table) {
            $table->dropForeign('activity_activity_publish_type_id_foreign');
            $table->dropColumn('activity_publish_type_id');
        });
    }
}
