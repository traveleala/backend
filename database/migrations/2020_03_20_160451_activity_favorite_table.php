<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ActivityFavoriteTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('activity_favorite')) {
            Schema::create('activity_favorite', function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('user_id');
                $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');
                $table->unsignedBigInteger('activity_id');
                $table->foreign('activity_id')->references('id')->on('activity')->onDelete('cascade');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('activity_favorite');
    }
}
