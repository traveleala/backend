<?php

use App\Values\v1\ActivityTypeValues;
use Illuminate\Database\Migrations\Migration;
use App\Repositories\v1\Activity\TypeRepository;

class ScriptActivityType extends Migration
{
    public function up()
    {
        TypeRepository::insert([
            [
                'name' => ActivityTypeValues::MOUNTAIN_BIKE_TYPE
            ],
            [
                'name' => ActivityTypeValues::KAYAK_TYPE
            ],
            [
                'name' => ActivityTypeValues::SKI_TYPE
            ],
            [
                'name' => ActivityTypeValues::CLIMBING_TYPE
            ],
            [
                'name' => ActivityTypeValues::MOTOCROSS_TYPE
            ],
            [
                'name' => ActivityTypeValues::BALLOON_TYPE
            ],
            [
                'name' => ActivityTypeValues::TREKKING_TYPE
            ],
            [
                'name' => ActivityTypeValues::KITE_BOARDING_TYPE
            ],
            [
                'name' => ActivityTypeValues::PARAGLIDING_TYPE
            ],
            [
                'name' => ActivityTypeValues::DIVING_TYPE
            ],
            [
                'name' => ActivityTypeValues::SNOWBOARD_TYPE
            ],
            [
                'name' => ActivityTypeValues::RIDE_TYPE
            ],
            [
                'name' => ActivityTypeValues::SLED_TYPE
            ],
            [
                'name' => ActivityTypeValues::BUNGEE_JUMPING_TYPE
            ],
            [
                'name' => ActivityTypeValues::DELTA_WING_TYPE
            ],
            [
                'name' => ActivityTypeValues::SNOWMOBILE_TYPE
            ],
            [
                'name' => ActivityTypeValues::RAFTING_TYPE
            ],
            [
                'name' => ActivityTypeValues::BIRD_OBSERVATIONS_TYPE
            ],
            [
                'name' => ActivityTypeValues::JET_SKI_TYPE
            ],
            [
                'name' => ActivityTypeValues::QUADS_TYPE
            ],
            [
                'name' => ActivityTypeValues::CAR_TYPE
            ],
            [
                'name' => ActivityTypeValues::FISHING_TYPE
            ],
            [
                'name' => ActivityTypeValues::SKYDIVING_TYPE
            ],
            [
                'name' => ActivityTypeValues::SURF_TYPE
            ],
            [
                'name' => ActivityTypeValues::CATAMARAN_TYPE
            ],
            [
                'name' => ActivityTypeValues::RAPPEL_TYPE
            ],
            [
                'name' => ActivityTypeValues::ZIP_LINE_TYPE
            ]
        ]);
    }

    public function down()
    {
        TypeRepository::truncate();
    }
}
