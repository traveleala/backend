<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserVerificationEmail extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('user_verification')) {
            Schema::create('user_verification', function (Blueprint $table) {
                $table->id();
                $table->string('token')->unique()->index();
                $table->unsignedBigInteger('user_id');
                $table->foreign('user_id')->references('id')->on('user');
                $table->integer('tries')->default(4);
                $table->timestamps();
            });
        }
    }


    public function down()
    {
        Schema::dropIfExists('user_verification');
    }
}
