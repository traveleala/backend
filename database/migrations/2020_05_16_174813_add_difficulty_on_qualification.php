<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDifficultyOnQualification extends Migration
{
    public function up()
    {
        Schema::table('activity_qualification', function (Blueprint $table) {
            $table->unsignedBigInteger('activity_difficulty_id')->after('user_id');
            $table->foreign('activity_difficulty_id')->references('id')->on('activity_difficulty')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('activity_qualification', function (Blueprint $table) {
            $table->dropForeign('activity_qualification_activity_difficulty_id_foreign');
            $table->dropColumn('activity_difficulty_id');
        });
    }
}
