<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use App\Repositories\v1\Report\StateRepository;

class ScriptReportState extends Migration
{
    public function up()
    {
        StateRepository::insert([
            [
                'name' => 'pending'
            ],
            [
                'name' => 'favorable_result'
            ],
            [
                'name' => 'unfavorable_result'
            ],
        ]);
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        StateRepository::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
