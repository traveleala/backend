<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserResetPassword extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('user_password_reset')) {
            Schema::create('user_password_reset', function (Blueprint $table) {
                $table->id();
                $table->string('token')->unique()->index();
                $table->unsignedBigInteger('user_id');
                $table->foreign('user_id')->references('id')->on('user');
                $table->integer('tries')->default(4);
                $table->timestamps();
            });
        }
    }


    public function down()
    {
        Schema::dropIfExists('user_password_reset');
    }
}
