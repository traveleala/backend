<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class QualificationTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('activity_qualification')) {
            Schema::create('activity_qualification', function (Blueprint $table) {
                $table->id();
                $table->text('review');
                $table->integer('qualification')->index();
                $table->date('activity_date')->nullable();

                $table->unsignedBigInteger('activity_id');
                $table->foreign('activity_id')->references('id')->on('activity')->onDelete('cascade');

                $table->unsignedBigInteger('user_id');
                $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');

                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    public function down()
    {
        Schema::dropIfExists('activity_qualification');
    }
}
