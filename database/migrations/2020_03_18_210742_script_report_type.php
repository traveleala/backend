<?php

use Illuminate\Database\Migrations\Migration;
use App\Values\v1\ReportTypeValues;
use Illuminate\Support\Facades\Schema;
use App\Repositories\v1\Report\TypeRepository;

class ScriptReportType extends Migration
{
    public function up()
    {
        TypeRepository::insert([
            [
                'name' => ReportTypeValues::FAKE_INFORMATION_TYPE
            ],
            [
                'name' => ReportTypeValues::VIOLENCE_TYPE
            ],
            [
                'name' => ReportTypeValues::SEX_CONTENT_TYPE
            ],
            [
                'name' => ReportTypeValues::CONTENT_DUPLICATED_TYPE
            ],
            [
                'name' => ReportTypeValues::OTHER_TYPE
            ],
        ]);
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        TypeRepository::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
