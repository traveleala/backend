<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ActivitySubscriptionTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('activity_subscription')) {
            Schema::create('activity_subscription', function (Blueprint $table) {
                $table->id();

                $table->integer('month')->nullable();
                $table->integer('amount');
                $table->boolean('paid_out')->default(false);

                $table->unsignedBigInteger('activity_publish_type_id');
                $table->foreign('activity_publish_type_id')->references('id')->on('activity_publish_type');

                $table->unsignedBigInteger('activity_id');
                $table->foreign('activity_id')->references('id')->on('activity')->onDelete('cascade');

                $table->dateTime('expire_at')->nullable(true);
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    public function down()
    {
        Schema::dropIfExists('activity_subscription');
    }
}
