<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ActivityContactTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('activity_contact')) {
            Schema::create('activity_contact', function (Blueprint $table) {
                $table->id();
                $table->text('phone_number')->nullable();
                $table->text('telephone_number')->nullable();
                $table->text('facebook_url')->nullable();
                $table->text('instagram_url')->nullable();
                $table->text('email')->nullable();
                $table->text('web')->nullable();

                $table->unsignedBigInteger('activity_id');
                $table->foreign('activity_id')->references('id')->on('activity')->onDelete('cascade');

                $table->timestamps();
            });
        }
    }


    public function down()
    {
        Schema::dropIfExists('activity_contact');
    }
}
