<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DifficultyTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('activity_difficulty')) {
            Schema::create('activity_difficulty', function (Blueprint $table) {
                $table->id();
                $table->string('name')->index()->unique();
            });
        }
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('activity_difficulty');
        Schema::enableForeignKeyConstraints();
    }
}
