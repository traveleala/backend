<?php

use App\Repositories\v1\Admin\Report\ActionRepository;
use Illuminate\Database\Migrations\Migration;
use App\Values\v1\ReportActionValues;
use Illuminate\Support\Facades\Schema;

class ScriptReportAction extends Migration
{
    public function up()
    {
        ActionRepository::insert([
            [
                'name' => ReportActionValues::ACTIVITY_ACTION,
                'entity' => ReportActionValues::ACTIVITY_ENTITY
            ],
            [
                'name' => ReportActionValues::USER_ACTION,
                'entity' => ReportActionValues::ACTIVITY_ENTITY
            ],
            [
                'name' => ReportActionValues::NOTHING_ACTION,
                'entity' => ReportActionValues::ACTIVITY_ENTITY
            ],
            [
                'name' => ReportActionValues::QUALIFICATION_ACTION,
                'entity' => ReportActionValues::QUALIFICATION_ENTITY
            ],
            [
                'name' => ReportActionValues::USER_ACTION,
                'entity' => ReportActionValues::QUALIFICATION_ENTITY
            ],
            [
                'name' => ReportActionValues::NOTHING_ACTION,
                'entity' => ReportActionValues::QUALIFICATION_ENTITY
            ],
        ]);
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        ActionRepository::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
