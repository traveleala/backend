<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class QualificationImages extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('activity_qualification_image')) {
            Schema::create('activity_qualification_image', function (Blueprint $table) {
                $table->id();
                $table->string('image_url');
                $table->unsignedBigInteger('activity_qualification_id');
                $table->foreign('activity_qualification_id')->references('id')->on('activity_qualification')->onDelete('cascade');
            });
        }
    }


    public function down()
    {
        Schema::dropIfExists('activity_qualification_image');
    }
}
