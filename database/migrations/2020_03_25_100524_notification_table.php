<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NotificationTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('notification')) {
            Schema::create('notification', function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('user_id');
                $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');
                $table->unsignedBigInteger('user_action_id');
                $table->foreign('user_action_id')->references('id')->on('user')->onDelete('cascade');

                $table->unsignedBigInteger('activity_id')->nullable();
                $table->foreign('activity_id')->references('id')->on('activity')->onDelete('cascade');

                $table->unsignedBigInteger('notification_type_id');
                $table->foreign('notification_type_id')->references('id')->on('notification_type')->onDelete('cascade');

                $table->boolean('read')->default(false);

                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('notification');
    }
}
