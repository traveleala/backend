<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserFollowTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('user_follow')) {
            Schema::create('user_follow', function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('user_follower_id');
                $table->foreign('user_follower_id')->references('id')->on('user')->onDelete('cascade');
                $table->unsignedBigInteger('user_following_id');
                $table->foreign('user_following_id')->references('id')->on('user')->onDelete('cascade');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('user_follow');
    }
}
