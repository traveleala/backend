<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ActivityTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('activity')) {
            Schema::create('activity', function (Blueprint $table) {
                $table->id();
                $table->string('title')->index();
                $table->text('description')->nullable();
                $table->integer('price')->index()->nullable()->default(0);
                $table->decimal('latitude', 10, 8)->index();
                $table->decimal('longitude', 11, 8)->index();
                $table->boolean('is_landscape')->default(false);
                $table->string('location')->index();

                $table->unsignedBigInteger('user_id');
                $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');

                $table->unsignedBigInteger('activity_state_id');
                $table->foreign('activity_state_id')->references('id')->on('activity_state');

                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    public function down()
    {
        Schema::dropIfExists('activity');
    }
}
