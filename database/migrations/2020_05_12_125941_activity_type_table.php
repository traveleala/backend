<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ActivityTypeTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('activity_type')) {
            Schema::create('activity_type', function (Blueprint $table) {
                $table->id();
                $table->string('name')->index()->unique();
            });
        }
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('activity_type');
        Schema::enableForeignKeyConstraints();
    }
}
