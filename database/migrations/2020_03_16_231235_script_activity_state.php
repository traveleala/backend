<?php

use App\Values\v1\ActivityStateValues;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use App\Repositories\v1\Activity\StateRepository;

class ScriptActivityState extends Migration
{

    public function up()
    {
        StateRepository::insert([
            [
                'name' => ActivityStateValues::AVAILABLE_STATE
            ],
            [
                'name' => ActivityStateValues::PAYMENT_PENDING_STATE
            ],
            [
                'name' => ActivityStateValues::EXPIRED_STATE
            ],
        ]);
    }


    public function down()
    {
        Schema::disableForeignKeyConstraints();
        StateRepository::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
