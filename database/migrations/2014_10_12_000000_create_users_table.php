<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('user')) {
            Schema::create('user', function (Blueprint $table) {
                $table->id();
                $table->string('email')->index()->unique();
                $table->boolean('verified')->default(false);
                $table->string('password')->nullable();
                $table->unsignedBigInteger('user_role_id');
                $table->foreign('user_role_id')->references('id')->on('user_role');
                $table->timestamp('reset_restricted_at')->index()->nullable();
                $table->string('facebook_id')->nullable()->index();
                $table->string('google_id')->nullable()->index();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    public function down()
    {
        Schema::dropIfExists('user');
    }
}
