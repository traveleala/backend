<?php

use Faker\Generator as Faker;
use App\Repositories\v1\Qualification\ImageRepository;

$factory->define(ImageRepository::$model, function (Faker $faker) {

    return [
        'image_url' => 'https://i.picsum.photos/id/' . $faker->numberBetween(1, 1000) . '/1024/768.jpg',
    ];
});
