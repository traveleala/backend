<?php

use Faker\Generator as Faker;
use App\Repositories\v1\Qualification\QualificationRepository;

$factory->define(QualificationRepository::$model, function (Faker $faker) {

    return [
        'review' => $faker->realText(200),
        'qualification' => $faker->numberBetween(1, 5),
        'activity_id' => $faker->numberBetween(1, 30),
        'user_id' => $faker->numberBetween(1, 15),
        'activity_difficulty_id' => $faker->numberBetween(1, 3)
    ];
});
