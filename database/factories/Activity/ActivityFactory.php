<?php

use App\Repositories\v1\Activity\ActivityRepository;
use App\Repositories\v1\Activity\DifficultyRepository;
use App\Repositories\v1\Activity\TypeRepository;
use Faker\Provider\es_AR\Address;
use Faker\Generator as Faker;
use App\Repositories\v1\Activity\StateRepository;

$factory->define(ActivityRepository::$model, function (Faker $faker) {

    $faker->addProvider(new Address($faker));

    $activityStateId = StateRepository::getAvailable()->id;
    $activityTypeId = TypeRepository::getFirstOrFailBy('id', rand(1, 27))->id;
    $activityDifficultyId = DifficultyRepository::getFirstOrFailBy('id', rand(1, 3))->id;

    $state = $faker->state;
    $state = $state == 'Tierra del Fuego, Antártida e Islas del Atlántico Sur' ? 'Chaco' : $state;

    return [
        'title' => $faker->catchPhrase(),
        'description' => $faker->realText(1000),
        'price' => $faker->randomNumber(2),
        'latitude' => $faker->latitude(-35, -33),
        'longitude' => $faker->longitude(-50, -23),
        'is_landscape' => $faker->boolean(50),
        'location' => $faker->secondaryAddress . ', ' . $state,
        'activity_state_id' => $activityStateId,
        'user_id' => $faker->numberBetween(1, 15),
        'activity_type_id' => $activityTypeId,
        'activity_difficulty_id' => $activityDifficultyId,
        'activity_publish_type_id' => 1
    ];
});
