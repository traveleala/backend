<?php

use App\Repositories\v1\Activity\ContactRepository;
use Faker\Generator as Faker;
use Faker\Provider\es_AR\PhoneNumber;

$factory->define(ContactRepository::$model, function (Faker $faker) {

    $faker->addProvider(new PhoneNumber($faker));

    return [
        'phone_number' => $faker->phoneNumber,
        'telephone_number' => $faker->phoneNumber,
        'facebook_url' => 'https://facebook.com/' . $faker->userName,
        'instagram_url' => 'https://instagram.com/' . $faker->userName,
        'email' => $faker->email,
        'web' => $faker->url
    ];
});
