<?php

use App\Repositories\v1\Activity\CharacteristicRepository;
use Faker\Generator as Faker;

$factory->define(CharacteristicRepository::$model, function (Faker $faker) {

    return [
        'concept' => $faker->realText(40),
        'value' => $faker->realText(40),
    ];
});
