<?php

use Faker\Generator as Faker;
use App\Repositories\v1\Activity\PublishTypeRepository;
use App\Values\v1\PublishTypeValues;
use App\Repositories\v1\Activity\SubscriptionRepository;

$factory->define(SubscriptionRepository::$model, function (Faker $faker) {

    $publishType = PublishTypeRepository::getByName(PublishTypeValues::BASIC_TYPE);

    return [
        'month' => $publishType->max_month,
        'amount' => 0,
        'activity_publish_type_id' => $publishType->id,
        'expire_at' => now()->addMonths($publishType->max_month)->format('Y-m-d h:i:s'),
        'paid_out' => true
    ];
});
