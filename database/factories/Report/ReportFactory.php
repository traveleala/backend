<?php

use Faker\Generator as Faker;
use App\Repositories\v1\Report\ReportRepository;
use App\Repositories\v1\Report\StateRepository;

$factory->define(ReportRepository::$model, function (Faker $faker) {

    $reportStateId = StateRepository::getStatePending()->id;

    return [
        'comment' => $faker->realText(200),
        'user_id' => $faker->numberBetween(1, 15),
        'report_state_id' => $reportStateId
    ];
});
