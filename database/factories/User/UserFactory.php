<?php

use Faker\Generator as Faker;
use App\Repositories\v1\User\RoleRepository;
use App\Repositories\v1\User\UserRepository;

$factory->define(UserRepository::$model, function (Faker $faker) {

    $roleId = RoleRepository::getUserRole()->id;
    $email = $faker->email;

    return [
        'email' => $email,
        'verified' => 1,
        'password' => password($email),
        'user_role_id' => $roleId
    ];
});
