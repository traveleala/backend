<?php

use Faker\Generator as Faker;
use App\Repositories\v1\User\ResetPasswordRepository;
use Illuminate\Support\Str;

$factory->define(ResetPasswordRepository::$model, function (Faker $faker) {

    return [
        'token' => Str::random(64),
    ];
});
