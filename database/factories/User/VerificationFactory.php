<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;
use App\Repositories\v1\User\VerificationRepository;

$factory->define(VerificationRepository::$model, function (Faker $faker) {

    return [
        'token' => Str::random(64),
    ];
});
