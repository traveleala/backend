<?php

use Faker\Generator as Faker;
use App\Repositories\v1\User\ProfileRepository;

$factory->define(ProfileRepository::$model, function (Faker $faker) {

    return [
        'full_name' => $faker->name,
        'portrait_url' => 'https://i.picsum.photos/id/' . $faker->numberBetween(1, 1000) . '/1024/768.jpg',
        'profile_image_url' => 'https://i.picsum.photos/id/' . $faker->numberBetween(1, 1000) . '/1024/768.jpg',
        'username' => $faker->userName,
        'change_restricted_at' => now()
    ];
});
