<?php

$router->group(['prefix' => 'v1', 'namespace' => 'v1', 'middleware' => ['cors', 'throttle:60,1']], function () use ($router) {
    require('user.php');
    require('public.php');
    require('admin.php');
    require('internal.php');
});
