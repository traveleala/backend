<?php

# Auth required
$router->group(['middleware' => ['user', 'isAuth']], function () use ($router) {

    # Follow
    $router->group(['prefix' => 'users'], function () use ($router) {
        $router->get('{username:[a-z0-9.]{3,20}}/follow', ['uses' => 'User\FollowController@show']);
        $router->put('{username:[a-z0-9.]{3,20}}/follow', ['as' => 'update', 'uses' => 'User\FollowController@update']);
    });

    $router->group(['prefix' => 'users/me'], function () use ($router) {

        # Information
        $router->get('', ['uses' => 'User\UserController@me']);

        # Profile
        $router->group(['prefix' => 'profile'], function () use ($router) {
            $router->get('', ['uses' => 'User\ProfileController@show']);
            $router->put('', ['as' => 'update', 'uses' => 'User\ProfileController@update']);
        });

        # Logout
        $router->delete('login', ['uses' => 'User\LoginController@delete']);

        # Delete
        $router->delete('', ['as' => 'delete', 'uses' => 'User\UserController@delete']);

        # Update password
        $router->put('password', ['as' => 'update', 'middleware' => 'hasPassword', 'uses' => 'User\UserController@update']);

        # Followers
        $router->get('followers', ['uses' => 'User\FollowController@getFollowers']);
        $router->get('followings', ['uses' => 'User\FollowController@getFollowings']);

        # Favorites
        $router->get('favorites/activities', ['uses' => 'Favorite\FavoriteController@get']);

        # Notifications
        $router->group(['prefix' => 'notifications'], function () use ($router) {
            $router->get('', ['uses' => 'Notification\NotificationController@get']);
            $router->get('news', ['uses' => 'Notification\NotificationController@hasNews']);
        });

        # Activities
        $router->group(['prefix' => 'activities'], function () use ($router) {

            # List
            $router->get('', ['uses' => 'Activity\ActivityController@myActivities']);

            # Create
            $router->post('', ['as' => 'store', 'uses' => 'Activity\ActivityController@store']);
            $router->post('steps/{stepId:[1-5]}', ['as' => 'store.step.', 'uses' => 'Activity\ActivityController@step']);

            # Update
            $router->get('{activityId}', ['uses' => 'Activity\ActivityController@showMyActivity']);
            $router->put('{activityId}', ['as' => 'update', 'uses' => 'Activity\ActivityController@update']);

            # Image
            $router->post('images', ['as' => 'upload', 'uses' => 'Activity\ImageController@upload']);
            $router->delete('images', ['as' => 'delete', 'uses' => 'Activity\ImageController@delete']);

            # Pay
            $router->post('{activityId}/subscriptions/pay', ['as' => 'store', 'uses' => 'Activity\SubscriptionController@pay']);

            # Delete
            $router->delete('{activityId}', ['as' => 'delete', 'uses' => 'Activity\ActivityController@delete']);

            # Renew / Recategorize
            $router->put('{activityId}/subscriptions', ['as' => 'recategorize', 'uses' => 'Activity\SubscriptionController@recategorize']);


            # Stats general
            $router->get('statistics/total', ['uses' => 'Activity\ActivityController@getQuantityTotal']);
            $router->get('statistics/{year}/per-month', ['uses' => 'Activity\ActivityController@getQuantityPerMonth']);

            # Stats by activity
            $router->group(['middleware' => 'canViewStatistics'], function () use ($router) {
                $router->get('{activityId}/statistics', ['uses' => 'Activity\ActivityController@statistics']);
                $router->get('{activityId}/statistics/visits/total', ['uses' => 'Activity\VisitController@getQuantityTotal']);
                $router->get('{activityId}/statistics/visits/{year}/per-month', ['uses' => 'Activity\VisitController@getQuantityPerMonth']);
                $router->get('{activityId}/statistics/qualifications/total', ['uses' => 'Qualification\QualificationController@getQuantityTotal']);
                $router->get('{activityId}/statistics/qualifications/{year}/per-month', ['uses' => 'Qualification\QualificationController@getQuantityPerMonth']);
                $router->get('{activityId}/statistics/qualification/total', ['uses' => 'Qualification\QualificationController@getQualificationTotalByActivity']);
                $router->get('{activityId}/statistics/qualification/{year}/per-month', ['uses' => 'Qualification\QualificationController@getQualificationPerMonth']);
                $router->get('{activityId}/statistics/shares/{sharedIn}', ['as' => 'share', 'uses' => 'Activity\ActivityController@getTotalSharesBy']);
            });
        });

        # Qualifications
        $router->group(['prefix' => 'qualifications/statistics'], function () use ($router) {
            $router->get('total', ['uses' => 'Qualification\QualificationController@getQuantityTotal']);
            $router->get('{year}/per-month', ['uses' => 'Qualification\QualificationController@getQuantityPerMonth']);
        });

        # Qualification
        $router->group(['prefix' => 'qualification/statistics'], function () use ($router) {
            $router->get('total', ['uses' => 'Qualification\QualificationController@getQualificationTotal']);
            $router->get('{year}/per-month', ['uses' => 'Qualification\QualificationController@getQualificationPerMonth']);
        });
    });

    $router->group(['prefix' => 'activities'], function () use ($router) {
        # Qualification
        $router->post('{activityId}/qualifications', ['middleware' => ['canQualifyActivity'], 'as' => 'store', 'uses' => 'Qualification\QualificationController@store']);

        # Favorites
        $router->put('{activityId}/favorites', ['as' => 'update', 'uses' => 'Favorite\FavoriteController@update']);

        # Publishes
        $router->get('publishes', ['uses' => 'Activity\PublishTypeController@get']);
    });

    # Report
    $router->get('reports/types', ['uses' => 'Report\TypeController@all']);

    $router->group(['middleware' => ['hasBeenReported']], function () use ($router) {
        $router->post('activities/{activityId}/reports', ['middleware' => ['canReportActivity'], 'as' => 'store', 'uses' => 'Report\ReportController@store']);
        $router->post('qualifications/{qualificationId}/reports', ['middleware' => ['canReportQualification'], 'as' => 'store', 'uses' => 'Report\ReportController@store']);
    });

    # Vote
    $router->put('qualifications/{qualificationId}/votes', ['as' => 'update', 'uses' => 'Qualification\VoteController@update']);
});
