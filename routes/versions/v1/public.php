<?php

# Activities
$router->group(['middleware' => ['user']], function () use ($router) {

    $router->group(['prefix' => 'activities'], function () use ($router) {
        $router->get('', ['uses' => 'Activity\ActivityController@search']);
        $router->get('top', ['uses' => 'Activity\ActivityController@top']);
        $router->get('recents', ['uses' => 'Activity\ActivityController@recents']);
        $router->get('types', ['uses' => 'Activity\TypeController@all']);
        $router->get('difficulties', ['uses' => 'Activity\DifficultyController@get']);
        $router->get('{activityId}', ['uses' => 'Activity\ActivityController@show']);
        $router->get('{activityId}/relateds', ['uses' => 'Activity\ActivityController@relateds']);
        $router->get('{activityId}/qualifications', ['uses' => 'Qualification\QualificationController@all']);
        $router->get('{activityId}/qualifications/difficulties', ['uses' => 'Qualification\QualificationController@getDifficulties']);
        $router->get('{activityId}/qualifications/stars', ['uses' => 'Qualification\QualificationController@getStars']);
        $router->get('{activityId}/qualifications/{qualificationId}', ['uses' => 'Qualification\QualificationController@show']);
        $router->get('{activityId}/users', ['uses' => 'User\ProfileController@byActivity']);
    });

    # Users
    $router->group(['prefix' => 'users'], function () use ($router) {

        # Auth
        $router->get('', 'User\UserController@auth');

        # Register
        $router->post('register', ['as' => 'store', 'uses' => 'User\UserController@store']);

        # Login
        $router->post('login', ['as' => 'store', 'uses' => 'User\LoginController@store']);
        $router->post('login/{provider}', ['as' => 'social', 'uses' => 'User\LoginController@socialAuth']);

        # Verification
        $router->post('verification', ['as' => 'store', 'uses' => 'User\VerificationController@store']);
        $router->put('verification/{token}', ['uses' => 'User\VerificationController@update']);

        # Reset password
        $router->post('reset/password', ['as' => 'store', 'uses' => 'User\ResetPasswordController@store']);
        $router->put('reset/password/{token}', ['as' => 'update', 'uses' => 'User\ResetPasswordController@update']);

        # Users
        $router->group(['prefix' => '{username:[a-z0-9.]{3,20}}'], function () use ($router) {
            $router->get('', ['uses' => 'User\ProfileController@byUsername']);
            $router->get('activities', ['uses' => 'Activity\ActivityController@byUsername']);
        });
    });

    # Locations
    $router->get('locations', ['uses' => 'Location\LocationController@search']);
    $router->get('locations/top', ['uses' => 'Location\LocationController@getRandomLocations']);
});
