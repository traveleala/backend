<?php

$router->group(['middleware' => ['user', 'isAuth', 'isAdmin'], 'prefix' => 'admins/me', 'namespace' => 'Admin'], function () use ($router) {

    # Reports
    $router->group(['prefix' => 'reports'], function () use ($router) {
        $router->get('', ['uses' => 'Report\ReportController@get']);
        $router->put('{reportId}', ['as' => 'update', 'uses' => 'Report\ReportController@update']);
        $router->get('actions', ['as' => 'get', 'uses' => 'Report\ActionController@getByEntity']);
    });

    # Users
    $router->group(['prefix' => 'users'], function () use ($router) {
        $router->get('', ['uses' => 'User\UserController@get']);
        $router->post('', ['as' => 'store', 'uses' => 'User\UserController@store']);
        $router->delete('', ['as' => 'delete', 'uses' => 'User\UserController@delete']);

        $router->get('roles', ['uses' => 'User\RoleController@all']);
    });
});
