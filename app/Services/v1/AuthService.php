<?php

namespace App\Services\v1;

use App\Exceptions\v1\BadRequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Hash;
use Firebase\JWT\JWT;
use Laravel\Socialite\Facades\Socialite;
use Throwable;
use Illuminate\Validation\UnauthorizedException;
use App\Repositories\v1\User\UserRepository;

class AuthService
{
    const AUTHORIZATION_HEADER = 'Authorization';
    const BEARER_PREFIX = 'Bearer ';
    const CACHE_PREFIX = 'tokens';

    public static function login($value, $column = 'id')
    {
        $userId = UserRepository::getFirstOrFailBy($column, $value, ['id'])->id;

        return AuthService::generate($userId);
    }

    public static function user()
    {
        $userId = self::getPayload();
        $user = UserRepository::getByIdWithRelations($userId);

        config(['app.user' => $user]);
    }

    public static function attempt($plainPassword, $hashedPassword = ''): void
    {
        $hashedPassword = !$hashedPassword ? user()->password : $hashedPassword;

        if (!Hash::check($plainPassword, $hashedPassword)) {
            throw new UnauthorizedException;
        }
    }

    public static function logout(): void
    {
        cache()->rememberForever(self::getPrefixAndToken(), function () {
            return '';
        });
    }

    public static function socialAuth($provider)
    {
        $url = Socialite::with($provider)->stateless()->redirect()->getTargetUrl();

        return $url;
    }

    public static function socialLogin($provider)
    {
        try {
            $user = Socialite::with($provider)->stateless()->user();
            $userModel = UserRepository::socialAuth($user->id, $user->email, $provider);
            $token = AuthService::generate($userModel->id);

            return $token;
        } catch (Throwable $th) {
            switch (get_class($th)) {
                case ClientException::class:
                    throw new BadRequestException;
                    break;
                default:
                    throw new $th;
                    break;
            }
        }
    }

    private static function generate($id, $limitDays = 30)
    {
        $iat = now()->timestamp;
        $exp = now()->addDays($limitDays)->timestamp;
        $payload = array(
            "iss" => conf('app.api_url'),
            "aud" => conf('app.api_url'),
            "iat" => $iat,
            "nbf" => $iat,
            'exp' => $exp,
            'payload' => $id
        );

        $jwt = JWT::encode($payload, self::getKey());

        return encrypt($jwt);
    }

    private static function isValid($token): void
    {
        $valid = false;

        if ($token) {
            $token = self::getPrefixAndToken($token);
            $valid = cache($token) === null;
        }

        if (!$valid) {
            throw new UnauthorizedException;
        }
    }

    private static function getPrefixAndToken($token = null)
    {
        $token = $token ? $token : self::getToken();
        return keyCache(self::CACHE_PREFIX, sha1($token));
    }

    private static function getPayload()
    {
        $token = self::getToken();
        $jwt = decrypt($token);
        $decoded = JWT::decode($jwt, self::getKey(), array(conf('jwt.algorithm')));

        return $decoded->payload;
    }

    private static function getToken(): string
    {
        $token = request()->header(self::AUTHORIZATION_HEADER);
        $token = str_replace(self::BEARER_PREFIX, '', $token);

        self::isValid($token);

        return $token;
    }

    private static function getKey()
    {
        return conf('app.key') . request()->ip();
    }
}
