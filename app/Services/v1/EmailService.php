<?php

namespace App\Services\v1;

use \SendGrid\Mail\Mail;
use \SendGrid;

class EmailService
{
    private $email;

    public function __construct()
    {
        $this->email = new Mail();
    }

    public function setFrom($emailFrom, $nameFrom)
    {
        $this->email->setFrom($emailFrom, $nameFrom);

        return $this;
    }

    public function setSubject($subject)
    {
        $this->email->setSubject($subject);

        return $this;
    }

    public function setTo($emailTo, $nameTo = '')
    {
        $this->email->addTo($emailTo, $nameTo);

        return $this;
    }

    public function setContent($content)
    {
        $this->email->addContent(
            'text/html',
            $content
        );

        return $this;
    }

    public function send()
    {
        $sendgrid = new SendGrid(conf('services.sendgrid.key'));
        $sendgrid->send($this->email);
    }
}
