<?php

namespace App\Services\v1;

use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use App\Values\v1\UploadTypeValues;

class FileService
{
    private $folder;

    public function __construct($typeOfFile)
    {
        $this->folder = $this->folderByTypeFile($typeOfFile);
    }

    private function folderByTypeFile($type)
    {
        $folders = [
            UploadTypeValues::PROFILE_TYPE => conf('filesystems.disks.s3.folder_profile'),
            UploadTypeValues::PORTRAIT_TYPE => conf('filesystems.disks.s3.folder_portrait'),
            UploadTypeValues::ACTIVITY_TYPE => conf('filesystems.disks.s3.folder_activity'),
            UploadTypeValues::QUALIFICATION_TYPE => conf('filesystems.disks.s3.folder_qualification'),
            UploadTypeValues::PROVINCE_TYPE => conf('filesystems.disks.s3.folder_province'),
        ];

        return $folders[$type];
    }

    private function makeProgressive($file)
    {
        $img = Image::make($file);
        $img->interlace();
        $img->save($file->getPathName(), 80);
    }

    public function upload(UploadedFile $file)
    {
        $this->makeProgressive($file);
        return conf('filesystems.disks.s3.url') . DIRECTORY_SEPARATOR . Storage::disk('s3')->put($this->folder, $file);
    }

    public function delete($urlImage)
    {
        $urlImage = explode('/', $urlImage);
        $file = end($urlImage);
        $absolutePath = $this->folder . DIRECTORY_SEPARATOR . $file;

        return Storage::disk('s3')->delete($absolutePath);
    }
}
