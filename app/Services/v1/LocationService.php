<?php

namespace App\Services\v1;

use Elasticsearch\ClientBuilder;
use App\Repositories\v1\Location\LocationRepository;
use Throwable;

class LocationService
{
    const MIN_CHARACTERS = 2;
    const LIMIT_CHUNK = 100;
    const INDEX_LOCATION = 'locations';

    private $client;

    public function __construct()
    {
        $this->createClient();
    }

    public function process()
    {
        $locations = LocationRepository::getLocations();
        $provinces = LocationRepository::getProvincesLocations();

        $builders = [
            $provinces,
            $locations
        ];

        $this->save($builders);
    }

    public function search($name)
    {
        if (!$name || strlen($name) <= self::MIN_CHARACTERS) {
            return [];
        }

        $params = [
            'index' => self::INDEX_LOCATION,
            'body'  => [
                'query' => [
                    // 'wildcard' => [
                    //     'location' => "*{$name}*"
                    // ]
                    'query_string' => [
                        'query' => "location:{$name}*",
                    ]
                ],
                "sort" => [
                    [
                        "_score" => [
                            "order" => "desc"
                        ]
                    ]
                ]
            ],
            'from' => 0,
            'size' => 50,
        ];


        $response = $this->client->search($params);
        $locations = collect($response['hits']['hits'])
            ->pluck('_source')
            ->pluck('location');

        return $locations;
    }

    private function deleteIndex()
    {
        try {
            $params = [
                'index' => self::INDEX_LOCATION,
                "body" => [
                    "query" => [
                        "match_all" => (object) []
                    ]
                ]
            ];

            $this->client->deleteByQuery($params);
        } catch (Throwable $th) {
        }
    }

    private function save(array $builders): void
    {
        $this->deleteIndex();

        foreach ($builders as $builder) {
            $builder->chunk(self::LIMIT_CHUNK, function ($activities) {

                foreach ($activities as $activity) {

                    $params['body'][] = [
                        'index' => [
                            '_index' => self::INDEX_LOCATION,
                            '_type' => 'location'
                        ],
                    ];

                    // "mappings": {
                    //     "doc": {
                    //         "properties": {
                    //             "name":{
                    //                 "type": "string",
                    //                 "index": "not_analyzed"
                    //             }
                    //         }
                    //     }
                    // }

                    $params['body'][] = [
                        'location' => $activity->customLocation,
                    ];
                }

                $responses = $this->client->bulk($params);
            });
        }
    }

    private function createClient()
    {
        $this->client = ClientBuilder::create()->build();
    }
}
