<?php

namespace App\Services\v1;

use MercadoPago\Item;
use App\Classes\v1\Log;
use MercadoPago\MerchantOrder;
use App\Values\v1\PaymentStatusValues;
use MercadoPago\Preference;
use MercadoPago\SDK;
use Throwable;

class PaymentService
{
    public function __construct()
    {
        $this->initialize();
    }

    private function initialize()
    {
        SDK::setAccessToken(conf('services.mp.access_token'));
    }

    public function pay($id, $amount, $type): string
    {
        $preference = new Preference();
        $preference->back_urls = array(
            "success" => conf('services.mp.url_payment_success'),
            "failure" => conf('services.mp.url_payment_failure'),
            "pending" => conf('services.mp.url_payment_pending'),
        );
        $preference->auto_return = "all";
        $preference->external_reference = encode($id);

        $title = 'Publicación ' . __($type) . ' ' . conf('app.name');

        $item = new Item();
        $item->title = $title;
        $item->quantity = 1;
        $item->unit_price = $amount;
        $preference->items = array($item);
        $preference->save();

        return $preference->init_point;
    }

    public function notifications($topic, $id)
    {
        $subscriptionId = '';

        try {
            if ($topic == 'merchant_order') {
                $merchantOrder = MerchantOrder::find_by_id($id);

                if ($merchantOrder->order_status == PaymentStatusValues::PAID_STATUS) {
                    $subscriptionId = decode($merchantOrder->external_reference);
                }
            }
        } catch (Throwable $th) {
            Log::app()->error(
                'We have a problem with the response of IPN service',
                ['error' => $th->getMessage()]
            );
        }

        return $subscriptionId;
    }
}
