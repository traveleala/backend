<?php

namespace App\Services\v1;

use App\Values\v1\EmailSubjectValues;
use App\Classes\v1\Log;
use Illuminate\Support\Facades\Mail;
use App\Repositories\v1\Notification\NotificationRepository;
use Throwable;

class NotificationService
{
    const LIMIT_CHUNK = 100;

    public function notify()
    {
        $notifications = $this->getUsersToNotificate();

        $notifications->chunk(self::LIMIT_CHUNK, function ($notifications) {

            $notificationsIds = [];

            foreach ($notifications as $notification) {
                $emailUserDeleted = $notification->user->email;
                $subject = EmailSubjectValues::USER_DELETED_SUBJECT;

                try {
                    Mail::send(
                        'v1.emails.userDeleted',
                        [
                            'email' => $emailUserDeleted
                        ],
                        function ($message) use ($emailUserDeleted, $subject) {
                            $message->to($emailUserDeleted)->subject($subject);
                        }
                    );

                    Log::notifications()->info("The notification was sent to : {$emailUserDeleted}");

                    $notificationsIds[] = $notification->id;
                } catch (Throwable $th) {
                    Log::notifications()->error("The notification wasn't sent to : {$emailUserDeleted}", [
                        'error' => $th->getMessage()
                    ]);
                }
            }

            $this->updateUsersNotification($notificationsIds);
        });
    }

    private function getUsersToNotificate()
    {
        return NotificationRepository::getUsersDeletedNotifications();
    }

    private function updateUsersNotification($notificationsIds)
    {
        NotificationRepository::setReadNotifications($notificationsIds);
    }
}
