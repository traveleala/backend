<?php

namespace App\Services\v1;

use App\Classes\v1\Cache;
use GuzzleHttp\Client;
use Exception;
use App\Classes\v1\Log;
use Illuminate\Support\Str;
use Throwable;

class ReverseGeocodingService
{
    const CACHE_PREFIX = 'activities:geocoding';

    private $latitude;
    private $longitude;
    private $checksum;

    public function __construct(float $latitude, float $longitude)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->checksum = $this->getChecksum();
    }

    private function getChecksum()
    {
        return sha1($this->latitude . $this->longitude);
    }

    private function find()
    {
        try {
            $client = new Client([
                'base_uri' => $this->getEndpoint(),
                'verify' => false
            ]);

            $response = $client->get('');

            return json_decode((string) $response->getBody(), true);
        } catch (Throwable $th) {
            Log::app()->error(
                "We can't get reverse geocoding from this coordinates {$this->latitude},{$this->longitude}",
                ['error' => $th->getMessage()]
            );
            throw new Exception;
        }
    }

    public function getLocation()
    {
        $key = $this->getCacheKey();

        $locationResponse = Cache::rememberForAnHour(function () {
            $response = $this->find();

            $country = $response['countryCode'];
            $province = $response['localityInfo']['administrative'][1]['isoName'];
            $city = $response['localityInfo']['administrative'][3]['name'];

            return [
                'latitude' => $this->latitude,
                'longitude' => $this->longitude,
                'location' => "{$city}, {$province}",
                'country' => $country
            ];
        }, $key);

        return $locationResponse;
    }

    private function getEndpoint()
    {
        $valuesToReplace = [
            $this->latitude,
            $this->longitude,
            conf('services.big_data_cloud.key'),
        ];

        return Str::replaceArray('{?}', $valuesToReplace, conf('services.big_data_cloud.url'));
    }

    private function getCacheKey()
    {
        return keyCache(self::CACHE_PREFIX, $this->checksum);
    }
}
