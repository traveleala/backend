<?php

namespace App\Services\v1;

use Widop\GoogleAnalytics\Client;
use Widop\GoogleAnalytics\Query;
use Widop\GoogleAnalytics\Service;
use Widop\HttpAdapter\CurlHttpAdapter;

class GoogleAnalyticsService
{
    private $profileId;
    private $email;
    private $privateKeyFile;
    private $service;

    public function __construct()
    {
        $this->setCredentials();
        $this->setService();
    }

    public function doQuery(array $parameters)
    {
        $parameters += [
            'dimensions' => [],
            'metrics' => [],
            'sort' =>  [],
            'filters' => [],
            'start-date' => null,
            'end-date' => null,
            'prettyprint' => 'true'
        ];

        $query = $this->getQuery();
        $query->setStartDate(new \DateTime($parameters['start-date']));
        $query->setEndDate(new \DateTime($parameters['end-date']));

        $query->setMetrics($parameters['metrics']);
        $query->setDimensions($parameters['dimensions']);

        $query->setSorts($parameters['sort']);
        $query->setFilters($parameters['filters']);

        $query->setPrettyPrint($parameters['prettyprint']);
        $query->setCallback(null);

        $response = $this->service->query($query);

        return $response;
    }

    private function setCredentials()
    {
        $this->profileId = 'ga:' . conf('services.analytics.profile_id');
        $this->email = conf('services.analytics.email');
        $this->privateKeyFile = base_path() . DIRECTORY_SEPARATOR . conf('services.analytics.key_file_name');
    }

    private function setService()
    {
        if (!$this->service) {
            $httpAdapter = new CurlHttpAdapter();
            $client = new Client($this->email, $this->privateKeyFile, $httpAdapter);

            $this->service = new Service($client);
        }

        return $this->service;
    }

    private function getQuery()
    {
        return new Query($this->profileId);
    }
}
