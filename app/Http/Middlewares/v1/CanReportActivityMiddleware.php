<?php

namespace App\Http\Middlewares\v1;

use App\Repositories\v1\Activity\ActivityRepository;
use Closure;

class CanReportActivityMiddleware
{
    public function handle($request, Closure $next)
    {
        $activityId = decode(request()->activityId);

        ActivityRepository::canReport($activityId);

        return $next($request);
    }
}
