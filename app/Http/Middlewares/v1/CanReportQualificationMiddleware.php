<?php

namespace App\Http\Middlewares\v1;

use Closure;
use App\Repositories\v1\Qualification\QualificationRepository;

class CanReportQualificationMiddleware
{
    public function handle($request, Closure $next)
    {
        $qualificationId = decode(request()->qualificationId);

        QualificationRepository::canReport($qualificationId);

        return $next($request);
    }
}
