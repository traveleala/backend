<?php

namespace App\Http\Middlewares\v1;

use Closure;
use Illuminate\Auth\Access\AuthorizationException;

class IsAdminMiddleware
{
    public function handle($request, Closure $next)
    {
        if (!isAdmin()) {
            throw new AuthorizationException;
        }

        return $next($request);
    }
}
