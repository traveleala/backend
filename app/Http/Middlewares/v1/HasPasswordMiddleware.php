<?php

namespace App\Http\Middlewares\v1;

use Illuminate\Auth\Access\AuthorizationException;
use Closure;

class HasPasswordMiddleware
{
    public function handle($request, Closure $next)
    {
        if (!user()->password) {
            throw new AuthorizationException;
        }

        return $next($request);
    }
}
