<?php

namespace App\Http\Middlewares\v1;

use Closure;
use App\Services\v1\AuthService;
use Throwable;

class UserMiddleware
{
    public function handle($request, Closure $next)
    {
        try {
            AuthService::user();
        } catch (Throwable $th) {
        }

        return $next($request);
    }
}
