<?php

namespace App\Http\Middlewares\v1;

use App\Repositories\v1\Activity\ActivityRepository;
use Closure;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repositories\v1\Qualification\QualificationRepository;

class CanQualifyActivityMiddleware
{
    public function handle($request, Closure $next)
    {
        $activityId = decode(request()->activityId);
        ActivityRepository::canReport($activityId);
        $canQualify = QualificationRepository::canQualify($activityId);

        if ($canQualify) {
            throw new ModelNotFoundException;
        }

        return $next($request);
    }
}
