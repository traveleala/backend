<?php

namespace App\Http\Middlewares\v1;

use Closure;
use Illuminate\Validation\UnauthorizedException;
use Throwable;

class IsAuthenticatedMiddleware
{
    public function handle($request, Closure $next)
    {
        try {
            user()->id;
        } catch (Throwable $th) {
            throw new UnauthorizedException;
        }

        return $next($request);
    }
}
