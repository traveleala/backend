<?php

namespace App\Http\Middlewares\v1;

use Closure;
use App\Exceptions\v1\LimitException;
use App\Repositories\v1\Report\ReportRepository;

class HasBeenReportedMiddleware
{
    public function handle($request, Closure $next)
    {
        $id = request()->activityId ? decode(request()->activityId) : decode(request()->qualificationId);
        $type = request()->activityId ? 'activity_id' : 'activity_qualification_id';

        $exists = ReportRepository::hasBeenReported($type, $id);

        if ($exists) {
            throw new LimitException;
        }

        return $next($request);
    }
}
