<?php

namespace App\Http\Middlewares\v1;

use App\Repositories\v1\Activity\SubscriptionRepository;
use Closure;

class CanViewStatisticsMiddleware
{
    public function handle($request, Closure $next)
    {
        $id = decode(request()->activityId);
        $createdAt = SubscriptionRepository::canViewStatistics($id)->created_at;
        request()->request->add(['created_at' => $createdAt]);

        return $next($request);
    }
}
