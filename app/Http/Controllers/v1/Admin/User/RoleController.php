<?php

namespace App\Http\Controllers\v1\Admin\User;

use App\Http\Resources\v1\BaseResource;
use App\Http\Controllers\v1\BaseController;
use App\Repositories\v1\User\RoleRepository;

class RoleController extends BaseController
{
    public function all()
    {
        $roles = RoleRepository::all()->pluck('name');
        return BaseResource::baseResource('roles', $roles);
    }
}
