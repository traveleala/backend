<?php

namespace App\Http\Controllers\v1\Admin\User;

use App\Http\Resources\v1\Admin\UserResource;
use App\Http\Controllers\v1\BaseController;
use App\Http\Requests\v1\Admin\User\UserRequest;
use App\Repositories\v1\Admin\User\UserRepository;

class UserController extends BaseController
{
    public function get()
    {
        $page = request()->page ?? 1;
        $users = UserRepository::get($page);
        return UserResource::basePagination('users', $users);
    }

    protected function store(UserRequest $request)
    {
        $properties = request()->all();
        $user = UserRepository::store($properties);
        return UserResource::bool($user);
    }

    public function delete()
    {
        $email = request()->email;
        $user = UserRepository::delete($email);
        return UserResource::bool($user);
    }
}
