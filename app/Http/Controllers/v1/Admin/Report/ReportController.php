<?php

namespace App\Http\Controllers\v1\Admin\Report;

use App\Http\Resources\v1\ReportResource;
use App\Http\Controllers\v1\BaseController;
use App\Repositories\v1\Admin\Report\ReportRepository;
use App\Http\Requests\v1\Admin\Report\ReportRequest;

class ReportController extends BaseController
{
    public function get()
    {
        $page = request()->page ?? 1;
        $reports = ReportRepository::get($page);
        return ReportResource::basePagination('reports', $reports);
    }

    protected function update(ReportRequest $request)
    {
        $reportId = request()->reportId;
        $action = request()->action;
        $report = ReportRepository::update($reportId, $action);
        return ReportResource::bool($report);
    }
}
