<?php

namespace App\Http\Controllers\v1\Admin\Report;

use App\Repositories\v1\Admin\Report\ActionRepository;
use App\Http\Resources\v1\ReportActionResource;
use App\Http\Controllers\v1\BaseController;
use App\Http\Requests\v1\Admin\Report\ActionRequest;

class ActionController extends BaseController
{
    protected function getByEntity(ActionRequest $request)
    {
        $entity = request()->entity ?? '';
        $actions = ActionRepository::getByEntity($entity);
        return ReportActionResource::baseResource('actions', $actions);
    }
}
