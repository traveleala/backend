<?php

namespace App\Http\Controllers\v1;

use Laravel\Lumen\Routing\Controller as BaseControllerCore;
use App\Http\Requests\v1\BaseRequest;
use ReflectionMethod;

class BaseController extends BaseControllerCore
{
    public function __call($method, $args)
    {
        $childClass = get_called_class();
        $check = new ReflectionMethod($childClass, $method);

        if ($check->isProtected()) {
            $request = $args[0];

            if ($request instanceof BaseRequest) {
                $request->validate();
            }

            return $this->$method(...$args);
        }
    }
}
