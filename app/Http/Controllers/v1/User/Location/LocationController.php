<?php

namespace App\Http\Controllers\v1\Location;

use App\Http\Controllers\v1\BaseController;
use App\Http\Resources\v1\BaseResource;
use App\Repositories\v1\Location\LocationRepository;
use App\Services\v1\LocationService;

class LocationController extends BaseController
{
    private $locationService;

    public function __construct(LocationService $locationService)
    {
        $this->locationService = $locationService;
    }

    public function search()
    {
        $name = request()->name ?? null;
        $locations = $this->locationService->search($name);
        return BaseResource::baseResource('locations', $locations);
    }

    public function getRandomLocations()
    {
        $locations = LocationRepository::getRandomLocations();
        return BaseResource::baseResource('locations', $locations);
    }
}
