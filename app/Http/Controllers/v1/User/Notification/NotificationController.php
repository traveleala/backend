<?php

namespace App\Http\Controllers\v1\Notification;

use App\Http\Controllers\v1\BaseController;
use App\Http\Resources\v1\NotificationResource;
use App\Repositories\v1\Notification\NotificationRepository;

class NotificationController extends BaseController
{
    public function get()
    {
        $page = request()->page ?? 1;
        $notifications = NotificationRepository::get($page);
        return NotificationResource::basePagination('notifications', $notifications);
    }

    public function hasNews()
    {
        $hasNews = NotificationRepository::hasNews();
        return NotificationResource::baseResource('notifications', $hasNews);
    }
}
