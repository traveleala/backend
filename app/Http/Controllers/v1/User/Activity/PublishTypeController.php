<?php

namespace App\Http\Controllers\v1\Activity;

use App\Http\Resources\v1\PublishTypeResource;
use App\Http\Controllers\v1\BaseController;
use App\Repositories\v1\Activity\PublishTypeRepository;

class PublishTypeController extends BaseController
{
    public function get()
    {
        $publishes = PublishTypeRepository::get();
        return PublishTypeResource::baseResource('publishes', $publishes);
    }
}
