<?php

namespace App\Http\Controllers\v1\Activity;

use App\Http\Resources\v1\BaseResource;
use App\Http\Controllers\v1\BaseController;
use App\Repositories\v1\Activity\DifficultyRepository;

class DifficultyController extends BaseController
{
    public function get()
    {
        $difficulties = DifficultyRepository::get();
        return BaseResource::baseResource('difficulties', $difficulties);
    }
}
