<?php

namespace App\Http\Controllers\v1\Activity;

use App\Repositories\v1\Activity\ActivityRepository;
use App\Http\Requests\v1\Activity\ActivityRequest;
use App\Http\Resources\v1\ActivityResource;
use App\Http\Controllers\v1\BaseController;
use App\Classes\v1\Cache;
use App\Repositories\v1\Favorite\FavoriteRepository;
use App\Services\v1\GoogleAnalyticsService;

class ActivityController extends BaseController
{
    protected function step(ActivityRequest $request)
    {
        return ActivityResource::bool(true);
    }

    protected function store(ActivityRequest $activityRequest)
    {
        $properties = $this->getPropertiesStoreOrUpdateActivity();
        $id = encode(ActivityRepository::store($properties));
        return ActivityResource::baseResource('activity', $id);
    }

    protected function update(ActivityRequest $activityRequest, $activityId)
    {
        $activityId = decode($activityId);
        $properties = $this->getPropertiesStoreOrUpdateActivity();
        $result = encode(ActivityRepository::update($activityId, $properties));
        return ActivityResource::bool($result);
    }

    public function top()
    {
        $activities = ActivityRepository::top();
        $activitiesId = $activities->pluck('activity')->pluck('id')->toArray();
        $favorites = FavoriteRepository::areInMyFavorites($activitiesId);
        return ActivityResource::top('activities', $activities, $favorites);
    }

    public function show()
    {
        $activityId = decode(request()->activityId);
        $activity = ActivityRepository::show($activityId);
        return ActivityResource::show('activity', $activity);
    }

    public function showMyActivity()
    {
        $activityId = decode(request()->activityId);
        $activity = ActivityRepository::showMyActivity($activityId);
        return ActivityResource::showMyActivity('activity', $activity);
    }

    public function search()
    {
        $properties = [
            'location' => request()->location ?? null,
            'title' => request()->title ?? null,
            'minPrice' => request()->minPrice ?? null,
            'maxPrice' => request()->maxPrice ?? null,
            'distance' => request()->distance ?? null,
            'coordinates' => request()->coordinates ?? null,
            'landscape' => request()->landscape ?? null,
            'orderBy' => request()->orderBy ?? 'created_at',
            'order' => request()->order ?? 'desc',
            'types' => array_filter(explode(',', request()->types)) ?? [],
            'difficulties' => array_filter(explode(',', request()->difficulties)) ?? [],
        ];

        $page = request()->page ?? 1;
        $total = request()->has('total');

        $activities = ActivityRepository::search($properties, $page, $total);

        if (!$total) {
            $activitiesId = $activities->pluck('activity')->pluck('id')->toArray();
            $favorites = FavoriteRepository::areInMyFavorites($activitiesId);
            return ActivityResource::basePagination('activities', $activities, $favorites);
        } else {
            return ActivityResource::baseResource('total', $activities);
        }
    }

    public function relateds()
    {
        $activityId = decode(request()->activityId);
        $activities = ActivityRepository::relateds($activityId);
        $activitiesId = $activities->pluck('activity')->pluck('id')->toArray();
        $favorites = FavoriteRepository::areInMyFavorites($activitiesId);

        return ActivityResource::thumbs('activities', $activities, $favorites);
    }

    public function recents()
    {
        $activities = ActivityRepository::recents();
        $activitiesId = $activities->pluck('activity')->pluck('id')->toArray();
        $favorites = FavoriteRepository::areInMyFavorites($activitiesId);
        return ActivityResource::thumbs('activities', $activities, $favorites);
    }

    public function byUsername($username)
    {
        $page = request()->page ?? 1;
        $activities = ActivityRepository::byUsername($username, $page);
        $activitiesId = $activities->pluck('activity')->pluck('id')->toArray();
        $favorites = FavoriteRepository::areInMyFavorites($activitiesId);
        return ActivityResource::basePagination('activities', $activities, $favorites);
    }

    public function getQuantityPerMonth()
    {
        $year = yearsOfUser(request()->year);
        $activitiesPerMonth = ActivityRepository::getQuantityPerMonth($year);
        return ActivityResource::baseResource('activities', $activitiesPerMonth);
    }

    public function getQuantityTotal()
    {
        $total = ActivityRepository::getQuantityTotal();
        return ActivityResource::baseResource('total', $total);
    }

    public function statistics($activityId)
    {
        $activityId = decode($activityId);
        $activity = [
            'created_at' => request()->created_at
        ];
        return ActivityResource::baseResource('activity', $activity);
    }

    public function myActivities()
    {
        $page = request()->page ?? 1;
        $total = request()->has('total');

        $activities = ActivityRepository::myActivities($page, $total);

        if (!$total) {
            $activitiesId = $activities->pluck('activity')->pluck('id')->toArray();
            $favorites = FavoriteRepository::areInMyFavorites($activitiesId);
            return ActivityResource::myActivities('activities', $activities, $favorites);
        } else {
            return ActivityResource::baseResource('total', $activities);
        }
    }

    public function delete()
    {
        $activity = decode(request()->activityId);
        $result = ActivityRepository::delete($activity);
        return ActivityResource::bool($result);
    }

    protected function getTotalSharesBy(ActivityRequest $request, $activityId, $sharedIn)
    {
        $id = decode($activityId);
        $key = keyCache(ActivityRepository::CACHE_PREFIX, "shares", $id, $sharedIn);
        $createdAt = request()->created_at->format('Y-m-d');

        $total = Cache::rememberDefault(function () use ($activityId, $sharedIn, $createdAt) {
            $googleAnalyticsService = new GoogleAnalyticsService;
            $parameters = [
                'metrics' => [
                    'ga:totalEvents'
                ],
                'start-date' => $createdAt,
                'end-date' => now()->format('Y-m-d'),
                'filters' => [
                    "ga:eventLabel==share_{$sharedIn}_activity_{$activityId}"
                ]
            ];

            $response = $googleAnalyticsService->doQuery($parameters);
            $total = $response->getTotalsForAllResults()['ga:totalEvents'];

            return $total;
        }, $key);

        return ActivityResource::baseResource('total', $total);
    }

    private function getPropertiesStoreOrUpdateActivity()
    {
        return request()->only(
            'title',
            'description',
            'price',
            'latitude',
            'longitude',
            'publish_type',
            'activity_type',
            'activity_difficulty',
            'phone_number',
            'telephone_number',
            'facebook_url',
            'instagram_url',
            'email',
            'web',
            'month',
            'publish_type',
            'images',
            'characteristics'
        );
    }
}
