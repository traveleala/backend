<?php

namespace App\Http\Controllers\v1\Activity;

use App\Http\Controllers\v1\BaseController;
use App\Http\Resources\v1\BaseResource;
use App\Repositories\v1\Activity\VisitRepository;

class VisitController extends BaseController
{
    public function getQuantityPerMonth($year, $activityId)
    {
        $year = yearsOfUser($year);
        $id = decode($activityId);

        $visitsPerMonth = VisitRepository::getQuantityPerMonth($id, $activityId, $year);
        return BaseResource::baseResource('visits', $visitsPerMonth);
    }

    public function getQuantityTotal($activityId)
    {
        $id = decode($activityId);
        $createdAt = request()->created_at->format('Y-m-d');

        $total = VisitRepository::getQuantityTotal($id, $activityId, $createdAt);
        return BaseResource::baseResource('total', $total);
    }
}
