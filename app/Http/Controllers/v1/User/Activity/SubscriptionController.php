<?php

namespace App\Http\Controllers\v1\Activity;

use App\Http\Resources\v1\BaseResource;
use App\Http\Controllers\v1\BaseController;
use App\Services\v1\PaymentService;
use App\Repositories\v1\Activity\SubscriptionRepository;
use App\Http\Requests\v1\Activity\SubscriptionRequest;

class SubscriptionController extends BaseController
{
    private $paymentService;

    public function __construct(PaymentService $paymentService)
    {
        $this->paymentService = $paymentService;
    }

    protected function pay(SubscriptionRequest $request)
    {
        $activityId = request()->activityId;
        $subscription = SubscriptionRepository::getFirstOrFailBy('activity_id', $activityId, ['id', 'amount', 'activity_publish_type_id']);
        $subscription->load('publishType');

        $url = $this->paymentService->pay($subscription->id, $subscription->amount, $subscription->publishType->name);

        return BaseResource::baseResource('url', $url);
    }

    protected function recategorize(SubscriptionRequest $request)
    {
        $properties = request()->all();
        $result = SubscriptionRepository::recategorize($properties);
        return BaseResource::bool($result);
    }

    public function notifications()
    {
        $topic = request()->topic ?? null;
        $paymentId = request()->id ?? null;
        $subscriptionId = $this->paymentService->notifications($topic, $paymentId);

        if ($subscriptionId) {
            SubscriptionRepository::paid($subscriptionId);
        }
    }
}
