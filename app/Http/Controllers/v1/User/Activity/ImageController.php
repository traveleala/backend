<?php

namespace App\Http\Controllers\v1\Activity;

use App\Http\Controllers\v1\BaseController;
use App\Http\Resources\v1\BaseResource;
use App\Http\Requests\v1\Activity\ImageRequest;
use App\Repositories\v1\Activity\ImageRepository;
use App\Services\v1\FileService;
use Throwable;
use App\Values\v1\UploadTypeValues;

class ImageController extends BaseController
{
    protected function upload(ImageRequest $request)
    {
        $image = request()->image;
        $fileService = new FileService(UploadTypeValues::ACTIVITY_TYPE);
        $url = $fileService->upload($image);

        return BaseResource::baseResource('image', $url);
    }

    protected function delete(ImageRequest $request)
    {
        $image = request()->image;

        try {
            ImageRepository::getActivityByImageUrl($image)->delete();
        } catch (Throwable $th) {
        }

        return BaseResource::bool('image', true);
    }
}
