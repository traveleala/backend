<?php

namespace App\Http\Controllers\v1\Activity;

use App\Http\Resources\v1\BaseResource;
use App\Http\Controllers\v1\BaseController;
use App\Repositories\v1\Activity\TypeRepository;

class TypeController extends BaseController
{
    public function all()
    {
        $types = TypeRepository::get();
        return BaseResource::baseResource('types', $types);
    }
}
