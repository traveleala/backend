<?php

namespace App\Http\Controllers\v1\Report;

use App\Http\Controllers\v1\BaseController;
use App\Http\Resources\v1\ReportTypeResource;
use App\Repositories\v1\Report\TypeRepository;

class TypeController extends BaseController
{
    public function all()
    {
        $types = TypeRepository::all();
        return ReportTypeResource::all('types', $types);
    }
}
