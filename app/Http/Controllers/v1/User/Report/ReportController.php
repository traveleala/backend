<?php

namespace App\Http\Controllers\v1\Report;

use App\Http\Controllers\v1\BaseController;
use App\Http\Resources\v1\BaseResource;
use App\Repositories\v1\Report\ReportRepository;
use App\Http\Requests\v1\Report\ReportRequest;

class ReportController extends BaseController
{
    protected function store(ReportRequest $request)
    {
        $properties = request()->only(
            'activity_qualification_id',
            'activity_id',
            'comment',
            'report_type'
        );

        $result = ReportRepository::store($properties);
        return BaseResource::bool($result);
    }
}
