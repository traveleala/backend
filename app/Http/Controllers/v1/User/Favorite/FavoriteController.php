<?php

namespace App\Http\Controllers\v1\Favorite;

use App\Http\Controllers\v1\BaseController;
use App\Http\Resources\v1\FavoriteResource;
use App\Repositories\v1\Favorite\FavoriteRepository;
use App\Http\Requests\v1\Favorite\FavoriteRequest;

class FavoriteController extends BaseController
{
    protected function update(FavoriteRequest $request)
    {
        $activityId = request()->activityId;
        $favorite = FavoriteRepository::update($activityId);
        return FavoriteResource::baseResource('favorite', $favorite);
    }

    public function get()
    {
        if (request()->activity_id) {
            return $this->show(request()->activity_id);
        }

        $page = request()->page ?? 1;
        $total = request()->has('total');

        $favorites = FavoriteRepository::get($page, $total);

        if (!$total) {
            return FavoriteResource::basePagination('favorites', $favorites);
        } else {
            return FavoriteResource::baseResource('total', $favorites);
        }
    }

    private function show($activityId)
    {
        $activityId = decode($activityId);
        $favorite = FavoriteRepository::has($activityId);
        return FavoriteResource::baseResource('favorite', $favorite);
    }
}
