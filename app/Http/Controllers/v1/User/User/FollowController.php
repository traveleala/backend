<?php

namespace App\Http\Controllers\v1\User;

use App\Http\Controllers\v1\BaseController;
use App\Http\Resources\v1\FollowResource;
use App\Repositories\v1\User\FollowRepository;
use App\Http\Requests\v1\User\FollowRequest;

class FollowController extends BaseController
{
    protected function update(FollowRequest $request, $username)
    {
        $result = FollowRepository::update($username);
        return FollowResource::baseResource('follow', $result);
    }

    public function show($username)
    {
        $result = FollowRepository::show($username);
        return FollowResource::baseResource('follow', $result);
    }

    public function getFollowers()
    {
        $page = request()->page ?? 1;
        $total = request()->has('total');

        $followers = FollowRepository::getFollowers($page, $total);

        if (!$total) {
            $userFollowersIds = collect($followers->items())->pluck('user_follower_id')->toArray();
            $areInMyFollowings = FollowRepository::areInMyFollowings($userFollowersIds);
            return FollowResource::followers('followers', $followers, $areInMyFollowings);
        } else {
            return FollowResource::baseResource('total', $followers);
        }
    }

    public function getFollowings()
    {
        $page = request()->page ?? 1;
        $total = request()->has('total');

        $followings = FollowRepository::getFollowings($page, $total);

        if (!$total) {
            return FollowResource::followings('followings', $followings);
        } else {
            return FollowResource::baseResource('total', $followings);
        }
    }
}
