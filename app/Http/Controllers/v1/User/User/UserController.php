<?php

namespace App\Http\Controllers\v1\User;

use App\Services\v1\AuthService;
use App\Http\Controllers\v1\BaseController;
use App\Http\Resources\v1\UserResource;
use App\Repositories\v1\User\UserRepository;
use App\Http\Requests\v1\User\UserRequest;

class UserController extends BaseController
{
    protected function update(UserRequest $request)
    {
        $properties = request()->all();
        AuthService::attempt($properties['password']);
        $result = UserRepository::update($properties);
        return UserResource::bool($result);
    }

    protected function store(UserRequest $request)
    {
        $properties = request()->all();
        $result = UserRepository::store($properties);
        return UserResource::bool($result);
    }

    protected function delete(UserRequest $request)
    {
        $password = request()->password;

        if (user()->password) {
            AuthService::attempt($password, user()->password);
        }

        UserRepository::delete();
        AuthService::logout();

        return UserResource::baseResource('user', true);
    }

    public function me()
    {
        return UserResource::me('user', user());
    }
}
