<?php

namespace App\Http\Controllers\v1\User;

use App\Http\Resources\v1\ProfileResource;
use App\Http\Controllers\v1\BaseController;
use App\Http\Requests\v1\User\ProfileRequest;
use App\Repositories\v1\User\ProfileRepository;

class ProfileController extends BaseController
{
    public function show()
    {
        if (user()->profile) {
            return $this->byUsername(user()->profile->username);
        } else {
            return ProfileResource::baseResource('profile', []);
        }
    }

    protected function update(ProfileRequest $request)
    {
        $properties = request()->all();
        $result = ProfileRepository::update($properties);
        return ProfileResource::bool($result);
    }

    public function byUsername($username)
    {
        $profile = ProfileRepository::getByUsername($username);
        return ProfileResource::byUsername('profile', $profile);
    }

    public function byActivity($activityId)
    {
        $activityId = decode($activityId);
        $profile = ProfileRepository::byActivity($activityId);
        return ProfileResource::byActivity('profile', $profile);
    }
}
