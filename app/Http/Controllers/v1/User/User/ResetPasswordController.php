<?php

namespace App\Http\Controllers\v1\User;

use App\Http\Controllers\v1\BaseController;
use App\Http\Resources\v1\BaseResource;
use App\Http\Requests\v1\User\ResetPasswordRequest;
use App\Repositories\v1\User\ResetPasswordRepository;

class ResetPasswordController extends BaseController
{
    protected function update(ResetPasswordRequest $request)
    {
        $properties = request()->all();
        $result = ResetPasswordRepository::update($properties);
        return BaseResource::bool($result);
    }

    protected function store(ResetPasswordRequest $request)
    {
        $properties = request()->all();
        $result = ResetPasswordRepository::store($properties);
        return BaseResource::bool($result);
    }
}
