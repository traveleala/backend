<?php

namespace App\Http\Controllers\v1\User;

use App\Services\v1\AuthService;
use App\Http\Controllers\v1\BaseController;
use App\Http\Resources\v1\BaseResource;
use App\Http\Requests\v1\User\LoginRequest;

class LoginController extends BaseController
{
    protected function store(LoginRequest $request)
    {
        $properties = request()->all();
        $token = AuthService::login($properties['email'], 'email');
        return BaseResource::baseResource('token', $token);
    }

    public function delete()
    {
        $result = AuthService::logout();
        return BaseResource::bool('user', $result);
    }

    protected function socialAuth(LoginRequest $request, $provider)
    {
        if (request()->code) {
            return $this->loginWith($provider);
        }

        $url = AuthService::socialAuth($provider);
        return BaseResource::baseResource('url', $url);
    }

    private function loginWith($provider)
    {
        $token = AuthService::socialLogin($provider);
        return BaseResource::baseResource('token', $token);
    }
}
