<?php

namespace App\Http\Controllers\v1\User;

use App\Http\Controllers\v1\BaseController;
use App\Http\Resources\v1\BaseResource;
use App\Repositories\v1\User\VerificationRepository;
use App\Http\Requests\v1\User\VerificationRequest;

class VerificationController extends BaseController
{
    protected function store(VerificationRequest $request)
    {
        $properties = request()->all();
        $result = VerificationRepository::store($properties);
        return BaseResource::bool($result);
    }

    public function update($token)
    {
        $result = VerificationRepository::update($token);
        return BaseResource::bool($result);
    }
}
