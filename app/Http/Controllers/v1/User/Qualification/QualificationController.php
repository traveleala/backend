<?php

namespace App\Http\Controllers\v1\Qualification;

use App\Http\Controllers\v1\BaseController;
use App\Http\Resources\v1\QualificationResource;
use App\Repositories\v1\Qualification\QualificationRepository;
use App\Http\Requests\v1\Qualification\QualificationRequest;
use App\Repositories\v1\Qualification\VoteRepository;

class QualificationController extends BaseController
{
    public function all($activityId)
    {
        $activityId = decode($activityId);
        $page = request()->page ?? 1;
        $total = request()->has('total');

        $qualifications = QualificationRepository::getByActivity($activityId, $page, $total);

        if (!$total) {
            $qualificationIds = $qualifications->pluck('id')->toArray();
            $votes = VoteRepository::areInMyVotes($qualificationIds);
            return QualificationResource::basePagination('qualifications', $qualifications, $votes);
        } else {
            return QualificationResource::baseResource('total', $qualifications);
        }
    }

    protected function store(QualificationRequest $request)
    {
        $properties = request()->all();
        $result = QualificationRepository::store($properties);
        return QualificationResource::bool($result);
    }

    public function getQuantityPerMonth($year)
    {
        $year = yearsOfUser($year);
        $activityId = request()->activityId ? decode(request()->activityId) : '';
        $quantitiesPerMonth = QualificationRepository::getQuantityPerMonth($year, $activityId);
        return QualificationResource::baseResource('qualifications', $quantitiesPerMonth);
    }

    public function getQuantityTotal()
    {
        $activityId = request()->activityId ? decode(request()->activityId) : '';
        $total = QualificationRepository::getQuantityTotal($activityId);
        return QualificationResource::baseResource('total', $total);
    }

    public function getQualificationPerMonth($year)
    {
        $year = yearsOfUser($year);
        $activityId = request()->activityId ? decode(request()->activityId) : '';
        $qualificationsPerMonth = QualificationRepository::getQualificationPerMonth($year, $activityId);
        return QualificationResource::baseResource('qualifications', $qualificationsPerMonth);
    }

    public function getQualificationTotal()
    {
        $total = QualificationRepository::getQualificationTotal();
        return QualificationResource::baseResource('total', $total);
    }

    public function getQualificationTotalByActivity()
    {
        $activityId = request()->activityId ? decode(request()->activityId) : '';
        $total = QualificationRepository::getQualificationTotalByActivity($activityId);
        return QualificationResource::baseResource('total', $total);
    }

    public function getDifficulties($activityId)
    {
        $activityId = decode($activityId);
        $percentages = QualificationRepository::getDifficultiesByActivity($activityId);
        return QualificationResource::baseResource('difficulties', $percentages);
    }

    public function getStars($activityId)
    {
        $activityId = decode($activityId);
        $stars = QualificationRepository::getStarsByActivity($activityId);
        return QualificationResource::baseResource('stars', $stars);
    }

    public function show($qualificationId)
    {
        $qualificationId = decode($qualificationId);
        $qualification = QualificationRepository::show($qualificationId);
        return QualificationResource::show('qualification', $qualification);
    }
}
