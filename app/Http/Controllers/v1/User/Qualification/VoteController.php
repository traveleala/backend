<?php

namespace App\Http\Controllers\v1\Qualification;

use App\Http\Resources\v1\BaseResource;
use App\Http\Controllers\v1\BaseController;
use App\Repositories\v1\Qualification\VoteRepository;
use App\Http\Requests\v1\Qualification\VoteRequest;

class VoteController extends BaseController
{
    protected function update(VoteRequest $request)
    {
        $qualificationId = request()->qualificationId;
        $voted = VoteRepository::update($qualificationId);
        $total = VoteRepository::getTotalVotes($qualificationId);
        $vote = [
            'voted' => $voted,
            'total' => $total
        ];
        return BaseResource::baseResource('vote', $vote);
    }
}
