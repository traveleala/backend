<?php

namespace App\Http\Resources\v1;

use App\Http\Resources\v1\BaseResource;

class PublishTypeResource extends BaseResource
{
    public static function baseResource($key, $types)
    {
        $types = $types->map(function ($type) {
            return $type->only(
                'name',
                'price',
                'min_month',
                'max_month',
                'discount'
            );
        });
        return parent::baseResource($key, $types);
    }
}
