<?php

namespace App\Http\Resources\v1;

use App\Http\Resources\v1\BaseResource;

class ActivityResource extends BaseResource
{
    public static function show($key, $activity)
    {
        $activity = [
            'description' => $activity->description,
            'title' => $activity->title,
            'price' => $activity->price,
            'latitude' => $activity->latitude,
            'longitude' => $activity->longitude,
            'location' => $activity->location,
            'qualification' => $activity->qualification,
            'contact' => [
                'phone_number' => $activity->contact->phone_number,
                'telephone_number' => $activity->contact->telephone_number,
                'facebook_url' => $activity->contact->facebook_url,
                'instagram_url' => $activity->contact->instagram_url,
                'email' => $activity->contact->email,
                'web' => $activity->contact->web
            ],
            'images' => $activity->images->pluck('image_url'),
            'user' => [
                'username' => $activity->user->profile->username
            ],
            'characteristics' => $activity->characteristics->map(function ($characteristic) {
                return [
                    'concept' => $characteristic->concept,
                    'value' => $characteristic->value
                ];
            }),
            'type' => [
                'name' => @$activity->type->name
            ],
            'difficulty' => [
                'name' => @$activity->difficulty->name
            ]
        ];

        return parent::baseResource($key, $activity);
    }

    public static function showMyActivity($key, $activity)
    {
        $activity = [
            'description' => $activity->description,
            'title' => $activity->title,
            'price' => $activity->price,
            'latitude' => $activity->latitude,
            'longitude' => $activity->longitude,
            'contact' => [
                'phone_number' => $activity->contact->phone_number,
                'telephone_number' => $activity->contact->telephone_number,
                'facebook_url' => $activity->contact->facebook_url,
                'instagram_url' => $activity->contact->instagram_url,
                'email' => $activity->contact->email,
                'web' => $activity->contact->web
            ],
            'images' => $activity->images->pluck('image_url'),
            'characteristics' => $activity->characteristics->map(function ($characteristic) {
                return [
                    'concept' => $characteristic->concept,
                    'value' => $characteristic->value
                ];
            }),
            'type' => [
                'name' => @$activity->type->name
            ],
            'difficulty' => [
                'name' => @$activity->difficulty->name
            ]
        ];

        return parent::baseResource($key, $activity);
    }

    public static function basePagination($key, $paginator, $favorites = [])
    {
        $paginator->getCollection()->transform(function ($activity) use ($favorites) {
            return [
                'id' => encode($activity->id),
                'title' => $activity->title,
                'price' => $activity->price,
                'latitude' => $activity->latitude,
                'longitude' => $activity->longitude,
                'favorite' => in_array($activity->id, $favorites),
                'location' => $activity->location,
                'qualification' => $activity->qualification,
                'portrait' => [
                    'image_url' => $activity->portrait->image_url
                ]
            ];
        });

        return parent::basePagination($key, $paginator);
    }

    public static function thumbs($key, $activities, $favorites = [])
    {
        $activities = $activities->map(function ($activity) use ($favorites) {
            return [
                'id' => encode($activity->id),
                'title' => $activity->title,
                'price' => $activity->price,
                'latitude' => $activity->latitude,
                'longitude' => $activity->longitude,
                'favorite' => in_array($activity->id, $favorites),
                'location' => $activity->location,
                'qualification' => $activity->qualification,
                'portrait' => [
                    'image_url' => $activity->portrait->image_url
                ]
            ];
        });

        return parent::baseResource($key, $activities);
    }

    public static function top($key, $activities, $favorites = [])
    {
        $activities = $activities->map(function ($qualification) use ($favorites) {
            return [
                'id' => encode($qualification->activity->id),
                'title' => $qualification->activity->title,
                'price' => $qualification->activity->price,
                'latitude' => $qualification->activity->latitude,
                'longitude' => $qualification->activity->longitude,
                'favorite' => in_array($qualification->activity->id, $favorites),
                'location' => $qualification->activity->location,
                'qualification' => $qualification->qualification,
                'portrait' => [
                    'image_url' => $qualification->activity->portrait->image_url
                ]
            ];
        });

        return parent::baseResource($key, $activities);
    }

    public static function myActivities($key, $paginator, $favorites)
    {
        $paginator->getCollection()->transform(function ($activity) use ($favorites) {

            return [
                'id' => encode($activity->id),
                'title' => $activity->title,
                'price' => $activity->price,
                'favorite' => in_array($activity->id, $favorites),
                'location' => $activity->location,
                'qualification' => $activity->qualification,
                'portrait' => [
                    'image_url' => $activity->portrait->image_url
                ],
                'state' => [
                    'name' => $activity->state->name
                ],
                'subscription' => [
                    'amount' => $activity->subscription->amount,
                    'paid_out' => $activity->subscription->paid_out,
                    'expire_at' => $activity->subscription->expire_at,
                    'publish_type' => [
                        'name' => $activity->subscription->publishType->name
                    ]
                ]
            ];
        });

        return parent::basePagination($key, $paginator);
    }
}
