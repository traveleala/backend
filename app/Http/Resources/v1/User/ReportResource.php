<?php

namespace App\Http\Resources\v1;

use App\Http\Resources\v1\BaseResource;

class ReportResource extends BaseResource
{
    public static function basePagination($key, $paginator)
    {
        $paginator->getCollection()->transform(function ($report) {
            return [
                'id' => encode($report->id),
                'comment' => $report->comment,
                'activity_id' => encode($report->activity_id),
                'activity_qualification_id' => encode($report->activity_qualification_id),
                'created_at' => $report->created_at,
                'type' => [
                    'name' => $report->type->name,
                ],
                'user' => [
                    'username' => $report->user->profile->username,
                ],
            ];
        });

        return parent::basePagination($key, $paginator);
    }
}
