<?php

namespace App\Http\Resources\v1;

use App\Http\Resources\v1\BaseResource;

class QualificationResource extends BaseResource
{
    public static function basePagination($key, $paginator, $votes = [])
    {
        $paginator->getCollection()->transform(function ($qualification) use ($votes) {
            return [
                'id' => encode($qualification->id),
                'review' => $qualification->review,
                'qualification' => $qualification->qualification,
                'votes' => $qualification->votes_count,
                'voted' => @in_array($qualification->id, $votes),
                'activity_date' => $qualification->activity_date,
                'user' => [
                    'username' => $qualification->user->profile->username,
                    'full_name' => $qualification->user->profile->full_name,
                    'profile_image_url' => $qualification->user->profile->profile_image_url,
                ],
                'images' => $qualification->images->pluck('image_url'),
                'difficulty' => [
                    'name' => @$qualification->difficulty->name
                ]
            ];
        });

        return parent::basePagination($key, $paginator);
    }

    public static function show($key, $qualification)
    {
        $qualification = [
            'id' => encode($qualification->id),
            'review' => $qualification->review,
            'qualification' => $qualification->qualification,
            'created_at' => $qualification->created_at,
            'votes' => $qualification->votes_count,
            'user' => [
                'username' => $qualification->user->profile->username,
                'full_name' => $qualification->user->profile->full_name,
                'profile_image_url' => $qualification->user->profile->profile_image_url,
            ],
            'images' => $qualification->images->pluck('image_url'),
            'difficulty' => [
                'name' => @$qualification->difficulty->name
            ]
        ];

        return parent::baseResource($key, $qualification);
    }
}
