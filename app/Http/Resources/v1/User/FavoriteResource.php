<?php

namespace App\Http\Resources\v1;

use App\Http\Resources\v1\BaseResource;

class FavoriteResource extends BaseResource
{
    public static function basePagination($key, $paginator)
    {
        $paginator->getCollection()->transform(function ($favorite) {
            return [
                'id' => encode($favorite->activity->id),
                'title' => $favorite->activity->title,
                'price' => $favorite->activity->price,
                'latitude' => $favorite->activity->latitude,
                'longitude' => $favorite->activity->longitude,
                'location' => $favorite->activity->location,
                'favorite' => true,
                'qualification' => $favorite->qualification,
                'portrait' => [
                    'image_url' => $favorite->activity->portrait->image_url,
                ],
            ];
        });

        return parent::basePagination($key, $paginator);
    }
}
