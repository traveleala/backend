<?php

namespace App\Http\Resources\v1;

use App\Http\Resources\v1\BaseResource;

class UserResource extends BaseResource
{
    public static function me($key, $user)
    {
        $user = [
            'role' => $user->role->name,
            'isSocial' => !$user->password,
            'profile' => [
                'created_at' => $user->created_at,
                'username' => @$user->profile->username,
                'profile_image_url' => @$user->profile->profile_image_url,
                'full_name' => @$user->profile->full_name
            ]
        ];

        return parent::baseResource($key, $user);
    }
}
