<?php

namespace App\Http\Resources\v1;

use App\Http\Resources\v1\BaseResource;

class ReportTypeResource extends BaseResource
{
    public static function all($key, $types)
    {
        $types = $types->pluck('name');
        return parent::baseResource($key, $types);
    }
}
