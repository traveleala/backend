<?php

namespace App\Http\Resources\v1;

use App\Http\Resources\v1\BaseResource;

class ProfileResource extends BaseResource
{
    public static function byUsername($key, $profile)
    {
        $profile = [
            'username' => $profile->username,
            'full_name' => $profile->full_name,
            'portrait_url' => $profile->portrait_url,
            'created_at' => $profile->created_at,
            'profile_image_url' => $profile->profile_image_url,
            'activities_count' => $profile->activities_available_count,
            'qualifications_count' => $profile->qualifications_count,
            'followers_count' => $profile->followers_count,
            'followings_count' => $profile->followings_count,
            'user_qualification' => $profile->user_qualification,
        ];

        return parent::baseResource($key, $profile);
    }

    public static function byActivity($key, $profile)
    {
        $profile = $profile->only(
            'username',
            'full_name',
            'profile_image_url',
            'created_at',
            'user_qualification'
        );

        return parent::baseResource($key, $profile);
    }
}
