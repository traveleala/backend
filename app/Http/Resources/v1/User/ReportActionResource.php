<?php

namespace App\Http\Resources\v1;

use App\Http\Resources\v1\BaseResource;

class ReportActionResource extends BaseResource
{
    public static function baseResource($key, $actions)
    {
        $actions = $actions->pluck('name');
        return parent::baseResource($key, $actions);
    }
}
