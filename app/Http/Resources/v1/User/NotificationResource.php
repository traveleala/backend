<?php

namespace App\Http\Resources\v1;

use App\Http\Resources\v1\BaseResource;
use App\Values\v1\NotificationTypeValues;

class NotificationResource extends BaseResource
{
    public static function basePagination($key, $paginator)
    {
        $paginator->getCollection()->transform(function ($notification) {

            $description = self::createDescription($notification);

            return [
                'description' => $description,
                'created_at' => $notification->created_at,
                'read' => $notification->read,
                'user' => [
                    'username' => $notification->profile->username,
                    'full_name' => $notification->profile->full_name,
                    'profile_image_url' => $notification->profile->profile_image_url
                ]
            ];
        });

        return parent::basePagination($key, $paginator);
    }

    private static function createDescription(object $notification)
    {
        $description = $notification->type->description;
        $variables = [];

        switch ($notification->type->name) {

            case NotificationTypeValues::QUALIFICATION_IN_ACTIVITY:
                $activityId = encode($notification->activity->id);
                $urlActivity = linkUrl("activities", $activityId);
                $variables['title'] = urlToHtml($urlActivity, $notification->activity->title);

                $urlProfile = linkUrl("users", $notification->profile->username);
                $variables['username'] = urlToHtml($urlProfile, $notification->profile->full_name);

                break;

            case NotificationTypeValues::FOLLOW_USER:
                $urlProfile = linkUrl("users", $notification->profile->username);
                $variables['username'] = urlToHtml($urlProfile, $notification->profile->full_name);
                break;

            case NotificationTypeValues::QUALIFICATION_DELETED:
                $activityId = encode($notification->activity->id);
                $urlActivity = linkUrl("activities", $activityId);
                $variables['title'] = urlToHtml($urlActivity, $notification->activity->title);
                break;

            case NotificationTypeValues::ACTIVITY_DELETED:
                $activityId = encode($notification->activity->id);
                $urlActivity = linkUrl("activities", $activityId);
                $variables['title'] = urlToHtml($urlActivity, $notification->activity->title);
                break;
        }

        $description = str_replace(
            array_map(function ($v) {
                return '{' . $v . '}';
            }, array_keys($variables)),
            $variables,
            $description
        );

        return $description;
    }
}
