<?php

namespace App\Http\Resources\v1;

use App\Http\Resources\v1\BaseResource;

class FollowResource extends BaseResource
{
    public static function followers($key, $paginator, $areInMyFollowings)
    {
        $paginator->getCollection()->transform(function ($follow) use ($areInMyFollowings) {
            return [
                'username' => $follow->follower->username,
                'profile_image_url' => $follow->follower->profile_image_url,
                'full_name' => $follow->follower->full_name,
                'favorite' => in_array($follow->user_follower_id, $areInMyFollowings)
            ];
        });

        return parent::basePagination($key, $paginator);
    }

    public static function followings($key, $paginator)
    {
        $paginator->getCollection()->transform(function ($follow) {
            return [
                'username' => $follow->following->username,
                'profile_image_url' => $follow->following->profile_image_url,
                'full_name' => $follow->following->full_name,
                'favorite' => true
            ];
        });

        return parent::basePagination($key, $paginator);
    }
}
