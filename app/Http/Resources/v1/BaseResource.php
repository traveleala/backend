<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Values\v1\HttpCodeValues;

class BaseResource extends JsonResource
{
    public static function baseResource($key, $value)
    {
        return [
            'status' => HttpCodeValues::OK_CODE,
            'success' => true,
            $key => $value,
        ];
    }

    public static function bool($value)
    {
        $value = (bool) $value;

        return [
            'status' => HttpCodeValues::OK_CODE,
            'success' => $value,
        ];
    }

    public static function basePagination($key, $value)
    {
        $responseArray = [
            'status' => HttpCodeValues::OK_CODE,
            'success' => true,
            'has_more' => $value->hasMorePages(),
            'current_page' => $value->currentPage(),
            $key => $value->items()
        ];

        return $responseArray;
    }
}
