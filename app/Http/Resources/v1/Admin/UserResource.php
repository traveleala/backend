<?php

namespace App\Http\Resources\v1\Admin;

use App\Http\Resources\v1\BaseResource;

class UserResource extends BaseResource
{
    public static function basePagination($key, $paginator)
    {
        $paginator->getCollection()->transform(function ($user) {
            return [
                'email' => $user->email,
                'role' => [
                    'name' => $user->role->name
                ],
                'profile' => [
                    'username' => @$user->profile->username
                ]
            ];
        });

        return parent::basePagination($key, $paginator);
    }
}
