<?php

namespace App\Http\Requests\v1\Activity;

use App\Http\Requests\v1\BaseRequest;
use App\Repositories\v1\Activity\ActivityRepository;
use App\Repositories\v1\Activity\PublishTypeRepository;
use Illuminate\Support\Str;
use App\Repositories\v1\Activity\SubscriptionRepository;

class SubscriptionRequest extends BaseRequest
{
    private static $rulesStepFive = [
        'publish_type' => 'bail|required|exists:{?},name',
        'month' => 'bail|required|integer|rangeMonthsPublishType:publish_type'
    ];

    public function before()
    {
        request()->request->add(['activityId' => decode(request()->activityId)]);

        switch ($this->nameRoute) {
            case 'store':
                SubscriptionRepository::canPay(request()->activityId);
                break;

            case 'recategorize':
                ActivityRepository::canRenewOrRecategorize(request()->activityId);
                break;
        }
    }

    protected function rulesRequest()
    {
        self::$rulesStepFive['publish_type'] = Str::replaceArray(
            '{?}',
            [PublishTypeRepository::getTableName()],
            self::$rulesStepFive['publish_type']
        );

        return [
            'store' => [],
            'recategorize' => self::$rulesStepFive,
        ];
    }
}
