<?php

namespace App\Http\Requests\v1\Activity;

use App\Http\Requests\v1\BaseRequest;

class ImageRequest extends BaseRequest
{
    protected function rulesRequest()
    {
        return [
            'upload' => [
                'image' => 'bail|required|image|dimensions:min_width=400,min_height=400|max:800'
            ],
            'delete' => [
                'image' => 'bail|required|isAUrlBucket|minImagesInActivity'
            ]
        ];
    }
}
