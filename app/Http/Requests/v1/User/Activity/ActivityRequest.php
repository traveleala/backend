<?php

namespace App\Http\Requests\v1\Activity;

use App\Http\Requests\v1\BaseRequest;
use App\Repositories\v1\Activity\DifficultyRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use NumberToWords\NumberToWords;
use App\Repositories\v1\Activity\PublishTypeRepository;
use App\Repositories\v1\Activity\TypeRepository;
use Illuminate\Support\Str;

class ActivityRequest extends BaseRequest
{
    const SHARES_AVAILABLE = [
        'linkedin',
        'email',
        'facebook',
        'twitter',
        'whatsapp',
        'telegram'
    ];
    const PREFIX_STEP_ROUTE = 'store.step.';
    const RULE_COORDINATE_POINT = 'regex:/^(-|)[0-9]{1,2}\.[0-9]{1,30}$/';
    const RULES_STEP_TWO = [
        'latitude' => [
            'bail',
            'required',
            self::RULE_COORDINATE_POINT
        ],
        'longitude' => [
            'bail',
            'required',
            self::RULE_COORDINATE_POINT,
            'coordinatesAreIn:AR,latitude'
        ]
    ];
    const RULES_STEP_THREE = [
        'images' => 'bail|required|array|min:8|max:40|areUrlsFromBucket',
    ];
    const RULES_STEP_FOUR = [
        'phone_number' => [
            'bail',
            'max:20',
            'regex:/^[0-9]{1,20}$/',
            'required_without_all:telephone_number,facebook_url,instagram_url,email,web'
        ],
        'telephone_number' => [
            'bail',
            'max:20',
            'regex:/^[0-9]{1,20}$/',
            'required_without_all:phone_number,facebook_url,instagram_url,email,web'
        ],
        'facebook_url' => [
            'bail',
            'nullable',
            'max:255',
            'regex:/^(http:\/\/(www.)?|https:\/\/(www.)?|www.)?facebook\.com\/[\w\W\d\D\.]{1,}$/',
            'required_without_all:phone_number,telephone_number,instagram_url,email,web'
        ],
        'instagram_url' => [
            'bail',
            'nullable',
            'max:255',
            'regex:/^(http:\/\/(www.)?|https:\/\/(www.)?|www.)?instagram\.com\/[\w\W\d\D\.]{1,}$/',
            'required_without_all:phone_number,telephone_number,facebook_url,email,web'
        ],
        'email' => 'bail|nullable|max:255|email:filter|required_without_all:phone_number,telephone_number,facebook_url,instagram_url,web',
        'web' => [
            'bail',
            'nullable',
            'max:255',
            'regex:/^(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})$/',
            'required_without_all:phone_number,telephone_number,facebook_url,instagram_url,email'
        ]
    ];

    private static $rulesStepOne = [
        'title' => 'bail|required|string|min:3|max:60|regex:/^[a-z\sA-Z0-9-]{3,60}$/',
        'description' => 'bail|nullable|string|min:10|max:1500',
        'price' => 'bail|nullable|numeric|max:100000',
        'characteristics' => 'nullable|array|max:10',
        'characteristics.*.concept' => 'bail|required|min:1|max:40|regex:/^[a-z\sA-Z0-9-]{1,40}$/',
        'characteristics.*.value' => 'bail|required|min:1|max:40|regex:/^[a-z\sA-Z0-9-]{1,40}$/',
        'activity_type' => 'bail|{?}|exists:{?},name',
        'activity_difficulty' => 'bail|{?}|exists:{?},name'
    ];

    private static $rulesStepFive = [
        'publish_type' => 'bail|required|exists:{?},name',
        'month' => 'bail|required|integer|rangeMonthsPublishType:publish_type'
    ];

    public function __construct()
    {
        parent::__construct();
        $this->setNameRoute();
    }

    private function getIdToWordIfExists()
    {
        $contains = Str::contains($this->nameRoute, [self::PREFIX_STEP_ROUTE]);

        if ($contains) {
            $numberToWords = new NumberToWords;
            $numberTransformer = $numberToWords->getNumberTransformer('en');
            return $numberTransformer->toWords(request()->stepId);
        }

        return null;
    }

    private function setNameRoute()
    {
        $this->nameRoute .= $this->getIdToWordIfExists();
    }

    public function before()
    {
        if ($this->nameRoute == 'share') {
            if (!in_array(request()->sharedIn, self::SHARES_AVAILABLE)) {
                throw new NotFoundHttpException;
            }
        }
    }

    protected function rulesRequest()
    {
        $baseIndexes = ['store', 'update'];
        $indexesForFiveStep = array_merge($baseIndexes, ['store.step.five']);
        $indexesForFirstStep = array_merge($baseIndexes, ['store.step.one']);

        if (in_array($this->nameRoute, $indexesForFirstStep)) {
            self::$rulesStepOne['activity_type'] = Str::replaceArray(
                '{?}',
                [
                    isNotUser() ? 'nullable' : 'required',
                    TypeRepository::getTableName()
                ],
                self::$rulesStepOne['activity_type']
            );

            self::$rulesStepOne['activity_difficulty'] = Str::replaceArray(
                '{?}',
                [
                    isNotUser() ? 'nullable' : 'required',
                    DifficultyRepository::getTableName()
                ],
                self::$rulesStepOne['activity_difficulty']
            );
        }

        if (in_array($this->nameRoute, $indexesForFiveStep)) {
            self::$rulesStepFive['publish_type'] = Str::replaceArray(
                '{?}',
                [PublishTypeRepository::getTableName()],
                self::$rulesStepFive['publish_type']
            );
        }

        $allRules = array_merge(
            self::$rulesStepOne,
            self::RULES_STEP_TWO,
            self::RULES_STEP_THREE,
            self::RULES_STEP_FOUR,
            self::$rulesStepFive
        );

        $allRulesLessLastStep = $allRules;

        if ($this->nameRoute == 'update') {
            unset($allRulesLessLastStep['publish_type'], $allRulesLessLastStep['month']);
        }

        return [
            self::PREFIX_STEP_ROUTE . 'one' => self::$rulesStepOne,
            self::PREFIX_STEP_ROUTE . 'two' => self::RULES_STEP_TWO,
            self::PREFIX_STEP_ROUTE . 'three' => self::RULES_STEP_THREE,
            self::PREFIX_STEP_ROUTE . 'four' => self::RULES_STEP_FOUR,
            self::PREFIX_STEP_ROUTE . 'five' => self::$rulesStepFive,
            'store' => $allRules,
            'update' => $allRulesLessLastStep,
            'share' => []
        ];
    }
}
