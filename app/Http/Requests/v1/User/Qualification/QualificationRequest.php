<?php

namespace App\Http\Requests\v1\Qualification;

use App\Http\Requests\v1\BaseRequest;
use App\Repositories\v1\Activity\DifficultyRepository;

class QualificationRequest extends BaseRequest
{
    protected function rulesRequest()
    {
        return [
            'store' => [
                'review' => 'bail|required|string|min:1|max:500',
                'qualification' => 'bail|required|numeric|min:1|max:5',
                'images' => 'bail|nullable|array|max:4',
                'images.*' => 'bail|image|max:800',
                'activity_difficulty' => 'bail|required|exists:' . DifficultyRepository::getTableName() . ',name',
                'activity_date' => 'bail|nullable|date|date_format:Y-m-d|after_or_equal:2010-01-01|before_or_equal:today'
            ],
        ];
    }

    public function after()
    {
        request()->request->add(['activity_id' => decode(request()->activityId)]);
    }
}
