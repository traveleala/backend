<?php

namespace App\Http\Requests\v1\Qualification;

use App\Http\Requests\v1\BaseRequest;
use App\Repositories\v1\Qualification\QualificationRepository;

class VoteRequest extends BaseRequest
{
    public function before()
    {
        $qualificationId = decode(request()->qualificationId);
        request()->request->add(['qualificationId' => $qualificationId]);

        QualificationRepository::existsOrFailBy('id', $qualificationId);
    }

    protected function rulesRequest()
    {
        return [
            'update' => []
        ];
    }
}
