<?php

namespace App\Http\Requests\v1\User;

use App\Http\Requests\v1\BaseRequest;
use App\Values\v1\CommonRulesValues;
use App\Repositories\v1\User\ProfileRepository;
use App\Values\v1\ProfileValues;
use App\Repositories\v1\User\UserRepository;

class UserRequest extends BaseRequest
{
    public function rulesRequest()
    {
        $profileTable = ProfileRepository::getTableName();
        $rulesToDelete = @user()->password ? 'bail|' . CommonRulesValues::PASSWORD_RULE . '|credentials' : '';

        return [
            'update' => [
                'password' => 'bail|' . CommonRulesValues::PASSWORD_RULE . '|credentials',
                'new_password' => 'bail|' . CommonRulesValues::PASSWORD_RULE . '|different:password|confirmed',
                'new_password_confirmation' => 'bail|required'
            ],
            'delete' => [
                'password' => $rulesToDelete
            ],
            'store' => [
                'email' => 'bail|required|email:filter|unique:' . UserRepository::getTableName() . ',email|regex:/^((?!\+{1,}).)*$/',
                'password' => 'bail|' . CommonRulesValues::PASSWORD_RULE,
                'username' => 'bail|required|min:3|max:20|unique:' . $profileTable . ',username|' . ProfileValues::USERNAME_RULE,
                'captcha' => 'required|captcha'
            ],
        ];
    }
}
