<?php

namespace App\Http\Requests\v1\User;

use App\Http\Requests\v1\BaseRequest;
use App\Values\v1\CommonRulesValues;
use App\Repositories\v1\User\UserRepository;

class ResetPasswordRequest extends BaseRequest
{
    public function rulesRequest()
    {
        return [
            'store' => [
                'email' => 'bail|required|email:filter|existsAnd:' . UserRepository::getTableName() . ',email,verified:1|isAvailableToReset|canResetPassword',
                'captcha' => 'captcha'
            ],
            'update' => [
                'password' => 'bail|' . CommonRulesValues::PASSWORD_RULE . '|confirmed',
                'password_confirmation' => 'bail|required'
            ]
        ];
    }

    public function after()
    {
        if ($this->nameRoute == 'update') {
            request()->request->add(['token' => request()->token]);
        }
    }
}
