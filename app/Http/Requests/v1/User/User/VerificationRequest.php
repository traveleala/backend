<?php

namespace App\Http\Requests\v1\User;

use App\Http\Requests\v1\BaseRequest;
use App\Repositories\v1\User\UserRepository;

class VerificationRequest extends BaseRequest
{
    protected function rulesRequest()
    {
        return [
            'store' => [
                'email' => 'bail|required|email:filter|existsAnd:' . UserRepository::getTableName() . ',email,verified:0|canResendVerification',
                'captcha' => 'captcha'
            ]
        ];
    }
}
