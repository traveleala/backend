<?php

namespace App\Http\Requests\v1\User;

use App\Http\Requests\v1\BaseRequest;
use App\Repositories\v1\User\FollowRepository;

class FollowRequest extends BaseRequest
{
    public function before()
    {
        FollowRepository::canFollow(request()->username);
    }

    public function rulesRequest()
    {
        return [
            'update' => []
        ];
    }
}
