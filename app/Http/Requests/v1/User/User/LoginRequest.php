<?php

namespace App\Http\Requests\v1\User;

use App\Http\Requests\v1\BaseRequest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Repositories\v1\User\UserRepository;

class LoginRequest extends BaseRequest
{
    const SOCIAL_PROVIDERS = [
        'facebook',
        'Google'
    ];

    public function before()
    {
        if ($this->nameRoute == 'social') {
            $this->checkProvider();
        }
    }

    public function rulesRequest()
    {
        return [
            'store' => [
                'email' => 'bail|required|email:filter|exists:' . UserRepository::getTableName() . ',email,deleted_at,NULL|isVerified|credentials:true',
                'password' => 'bail|required|min:6|max:30',
                'captcha' => 'captcha'
            ],
            'social' => []
        ];
    }

    private function checkProvider()
    {
        $provider = request()->provider;

        if (!in_array($provider, self::SOCIAL_PROVIDERS)) {
            throw new NotFoundHttpException;
        }
    }
}
