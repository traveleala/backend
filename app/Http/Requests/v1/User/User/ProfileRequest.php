<?php

namespace App\Http\Requests\v1\User;

use App\Http\Requests\v1\BaseRequest;
use App\Repositories\v1\User\ProfileRepository;
use App\Values\v1\ProfileValues;

class ProfileRequest extends BaseRequest
{
    protected function rulesRequest()
    {
        $profileTable = ProfileRepository::getTableName();
        $userId = user()->id;

        return [
            'update' => [
                'full_name' => 'bail|required|string|min:3|max:20|regex:/^[a-zA-ZaáéíóúÁÉÍÓÚñÑ\s]{3,20}$/',
                'username' => 'bail|required|min:3|max:20|iunique:' . $profileTable . ',username,user_id:' . $userId . '|' . ProfileValues::USERNAME_RULE . '|canChangeUserName',
                'profile_image' => 'bail|nullable|file|image|max:300',
                'portrait_image' => 'bail|nullable|file|image|max:800'
            ]
        ];
    }
}
