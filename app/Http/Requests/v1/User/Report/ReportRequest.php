<?php

namespace App\Http\Requests\v1\Report;

use App\Http\Requests\v1\BaseRequest;
use App\Repositories\v1\Report\TypeRepository;

class ReportRequest extends BaseRequest
{
    public function rulesRequest()
    {
        return [
            'store' => [
                'comment' => 'bail|nullable|string|min:3|max:200',
                'report_type' => 'bail|required|exists:' . TypeRepository::getTableName() . ',name'
            ]
        ];
    }

    public function after()
    {
        if (request()->activityId) {
            request()->request->add(['activity_id' => decode(request()->activityId)]);
        }

        if (request()->qualificationId) {
            request()->request->add(['activity_qualification_id' => decode(request()->qualificationId)]);
        }
    }
}
