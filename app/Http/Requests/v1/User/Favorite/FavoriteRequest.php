<?php

namespace App\Http\Requests\v1\Favorite;

use App\Repositories\v1\Activity\ActivityRepository;
use App\Http\Requests\v1\BaseRequest;

class FavoriteRequest extends BaseRequest
{
    public function before()
    {
        request()->request->add(['activityId' => decode(request()->activityId)]);
        ActivityRepository::canAddToFavorite(request()->activityId);
    }

    protected function rulesRequest()
    {
        return [
            'update' => [
                ''
            ]
        ];
    }
}
