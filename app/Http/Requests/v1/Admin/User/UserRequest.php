<?php

namespace App\Http\Requests\v1\Admin\User;

use App\Http\Requests\v1\BaseRequest;
use App\Values\v1\CommonRulesValues;
use App\Repositories\v1\Admin\User\UserRepository;
use App\Repositories\v1\User\RoleRepository;

class UserRequest extends BaseRequest
{
    public function rulesRequest()
    {
        return [
            'store' => [
                'email' => 'bail|required|email:filter|unique:' . UserRepository::getTableName() . ',email|regex:/^((?!\+{1,}).)*$/',
                'password' => 'bail|' . CommonRulesValues::PASSWORD_RULE,
                'role' => 'bail|required|exists:' . RoleRepository::getTableName() . ',name'
            ],
        ];
    }
}
