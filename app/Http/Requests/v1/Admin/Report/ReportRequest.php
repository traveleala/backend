<?php

namespace App\Http\Requests\v1\Admin\Report;

use App\Repositories\v1\Admin\Report\ActionRepository;
use App\Http\Requests\v1\BaseRequest;
use App\Values\v1\ReportActionValues;
use App\Repositories\v1\Admin\Report\ReportRepository;

class ReportRequest extends BaseRequest
{
    private $report;

    public function before()
    {
        request()->request->add(['reportId' => decode(request()->reportId)]);
        $this->report = ReportRepository::getReportAvailableById(request()->reportId);
    }

    public function rulesRequest()
    {
        $entity = $this->report->activity_id ? ReportActionValues::ACTIVITY_ENTITY : ReportActionValues::QUALIFICATION_ENTITY;

        return [
            'update' => [
                'action' => 'bail|required|existsAnd:' . ActionRepository::getTableName() . ',name,entity:' . $entity,
            ]
        ];
    }
}
