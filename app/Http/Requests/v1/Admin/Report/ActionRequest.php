<?php

namespace App\Http\Requests\v1\Admin\Report;

use App\Http\Requests\v1\BaseRequest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Values\v1\ReportActionValues;

class ActionRequest extends BaseRequest
{
    public function before()
    {
        $this->validateEntity();
    }

    public function rulesRequest()
    {
        return [
            'get' => []
        ];
    }

    private function validateEntity()
    {
        $entities = [
            ReportActionValues::ACTIVITY_ENTITY,
            ReportActionValues::QUALIFICATION_ENTITY
        ];

        $entity = request()->entity ?? '';

        if (!in_array($entity, $entities)) {
            throw new NotFoundHttpException;
        }
    }
}
