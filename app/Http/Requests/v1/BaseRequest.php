<?php

namespace App\Http\Requests\v1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class BaseRequest extends Request
{
    protected $nameRoute;

    public function __construct()
    {
        parent::__construct();
        $this->setNameRoute();
    }

    private function setNameRoute()
    {
        $route = app()->getCurrentRoute();
        $name = $route[1]['as'];
        $this->nameRoute = $name;
    }

    public function validate()
    {
        if (method_exists($this, 'before')) {
            $this->before();
        }

        try {
            Validator::make(request()->all(), $this->rules())->validate();
        } catch (ValidationException $e) {
            //$e->errorBag('');
            throw $e;
        }

        if (method_exists($this, 'after')) {
            $this->after();
        }
    }

    public function rules()
    {
        return $this->rulesRequest()[$this->nameRoute];
    }
}
