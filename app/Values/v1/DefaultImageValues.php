<?php

namespace App\Values\v1;

abstract class DefaultImageValues
{
    const DEFAULTS = [
        '2opZbpkO.jpg',
        'Ar3wg3ve.jpg',
        'GvMOQ36P.jpg',
        'lkpAx3NY.jpg',
        'O5M7xqPk.jpg',
        're3kVqG6.jpg',
        'XdqK9pBk.jpg',
        'XVp9RMb6.jpg',
        'Zl3nR3ny.jpg',
        'zxpG4MZJ.jpg'
    ];
}
