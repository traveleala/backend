<?php

namespace App\Values\v1;

abstract class UploadTypeValues
{
    const PROFILE_TYPE = 'profile';
    const PORTRAIT_TYPE = 'portrait';
    const ACTIVITY_TYPE = 'activity';
    const QUALIFICATION_TYPE = 'qualification';
    const PROVINCE_TYPE = 'province';
}
