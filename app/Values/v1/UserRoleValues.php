<?php

namespace App\Values\v1;

abstract class UserRoleValues
{
    const ADMIN_ROLE = 'admin';
    const PUBLISHER_ROLE = 'publisher';
    const USER_ROLE = 'user';
    const SPECIAL_USER_ROLE = 'special_user';
}
