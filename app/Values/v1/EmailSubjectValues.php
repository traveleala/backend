<?php

namespace App\Values\v1;

abstract class EmailSubjectValues
{
    const WELCOME_SUBJECT = 'Bienvenido - Traveleala';
    const RESET_PASSWORD_SUBJECT = 'Reinicio de contraseña - Traveleala';
    const USER_DELETED_SUBJECT = 'Usuario eliminado - Traveleala';
}
