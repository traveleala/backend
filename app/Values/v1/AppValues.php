<?php

namespace App\Values\v1;

abstract class AppValues
{
    const ENV_PRODUCTION = 'production';

    public static function OFFSET()
    {
        return conf('app.offset');
    }
}
