<?php

namespace App\Values\v1;

abstract class CacheValues
{
    const DEFAULT_TTL = 600;
    const HOURLY_TTL = 3600;
    const WEEKLY_TTL = 604800;
}
