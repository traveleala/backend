<?php

namespace App\Values\v1;

abstract class ActivityTypeValues
{
    const MOUNTAIN_BIKE_TYPE = 'mountain bike';
    const KAYAK_TYPE = 'kayak';
    const SKI_TYPE = 'ski';
    const CLIMBING_TYPE = 'escalada';
    const MOTOCROSS_TYPE = 'motocross';
    const BALLOON_TYPE = 'globo';
    const TREKKING_TYPE = 'trekking';
    const KITE_BOARDING_TYPE = 'kiteboarding';
    const SNOWBOARD_TYPE = 'snowboard';
    const BUNGEE_JUMPING_TYPE = 'bungee jumping';
    const RAFTING_TYPE = 'rafting';
    const CAR_TYPE = '4x4';
    const SURF_TYPE = 'surf';
    const RAPPEL_TYPE = 'rappel';
    const PARAGLIDING_TYPE = 'parapente';
    const DIVING_TYPE = 'buceo';
    const RIDE_TYPE = 'cabalgatas';
    const SLED_TYPE = 'trineo';
    const DELTA_WING_TYPE = 'ala delta';
    const SNOWMOBILE_TYPE = 'moto de nieve';
    const BIRD_OBSERVATIONS_TYPE = 'observaciones de aves';
    const JET_SKI_TYPE = 'moto de agua';
    const QUADS_TYPE = 'cuatriciclos';
    const FISHING_TYPE = 'pesca';
    const SKYDIVING_TYPE = 'paracaidismo';
    const CATAMARAN_TYPE = 'catamaran';
    const ZIP_LINE_TYPE = 'tirolesa';
}
