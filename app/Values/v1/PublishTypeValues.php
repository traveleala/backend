<?php

namespace App\Values\v1;

abstract class PublishTypeValues
{
    const BASIC_TYPE = 'basic';
    const STANDARD_TYPE = 'standard';
    const PREMIUM_TYPE = 'premium';
}
