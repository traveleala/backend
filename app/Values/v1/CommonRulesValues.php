<?php

namespace App\Values\v1;

abstract class CommonRulesValues
{
    const PASSWORD_RULE = 'required|min:6|max:30|case_diff|numbers|letters';
}
