<?php

namespace App\Values\v1;

abstract class HttpCodeValues
{
    const OK_CODE = 200;
    const INVALID_REQUEST_CODE = 400;
    const UNAUTHORIZED_CODE = 401;
    const FORBIDDEN_CODE = 403;
    const NOT_FOUND_CODE = 404;
    const METHOD_NOT_ALLOWED_CODE = 405;
    const UNPROCESSABLE_ENTITY_CODE = 422;
    const LIMIT_QUOTA_CODE = 429;
    const INTERNAL_SERVER_ERROR_CODE = 500;
}
