<?php

namespace App\Values\v1;

abstract class ActivityStateValues
{
    const AVAILABLE_STATE = 'available';
    const PAYMENT_PENDING_STATE = 'payment_pending';
    const EXPIRED_STATE = 'expired';
}
