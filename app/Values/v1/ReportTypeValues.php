<?php

namespace App\Values\v1;

abstract class ReportTypeValues
{
    const FAKE_INFORMATION_TYPE = 'fake_information';
    const VIOLENCE_TYPE = 'violence';
    const SEX_CONTENT_TYPE = 'sex_content';
    const CONTENT_DUPLICATED_TYPE = 'content_duplicated';
    const OTHER_TYPE = 'other';
}
