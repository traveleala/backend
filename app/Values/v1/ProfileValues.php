<?php

namespace App\Values\v1;

abstract class ProfileValues
{
    const USERNAME_RULE = 'regex:/^[a-z0-9.-]{3,20}$/';
}
