<?php

namespace App\Values\v1;

abstract class NotificationTypeValues
{
    const ACTIVITY_DELETED = 'activity_deleted';
    const QUALIFICATION_DELETED = 'qualification_deleted';
    const USER_DELETED = 'user_deleted';
    const FOLLOW_USER = 'follow_username';
    const QUALIFICATION_IN_ACTIVITY = 'qualification_in_activity';
}
