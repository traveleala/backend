<?php

namespace App\Values\v1;

abstract class ReportActionValues
{
    const ACTIVITY_ENTITY = 'activity';
    const QUALIFICATION_ENTITY = 'qualification';

    const ACTIVITY_ACTION = 'activity';
    const QUALIFICATION_ACTION = 'qualification';
    const USER_ACTION = 'user';
    const NOTHING_ACTION = 'nothing';
}
