<?php

namespace App\Values\v1;

abstract class DifficultyValues
{
    const EASY_DIFFICULTY = 'fácil';
    const MEDIUM_DIFFICULTY = 'media';
    const HARD_DIFFICULTY = 'difícil';
}
