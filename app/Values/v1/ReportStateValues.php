<?php

namespace App\Values\v1;

abstract class ReportStateValues
{
    const PENDING_STATE = 'pending';
    const FAVORABLE_RESULT_STATE = 'favorable_result';
    const UNFAVORABLE_RESULT_STATE = 'unfavorable_result';
}
