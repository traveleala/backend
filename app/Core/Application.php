<?php

namespace App\Core;

use Laravel\Lumen\Application as LumenApplication;

class Application extends LumenApplication
{
    /**
     * @return string
     */
    public function getCurrentRoute()
    {
        return $this->currentRoute;
    }
}
