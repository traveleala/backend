<?php

namespace App\Listeners\v1\User;

use App\Classes\v1\Log;
use Illuminate\Support\Facades\Mail;
use App\Events\v1\User\ResetPasswordEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Throwable;

class ResetPasswordListener implements ShouldQueue
{
    public function handle(ResetPasswordEvent $event)
    {
        $email = $event->userModel->email;
        $dataEmail = $event->dataEmail;

        try {
            Mail::send(
                'v1.emails.passwordReset',
                [
                    'token' => $dataEmail['token']
                ],
                function ($message) use ($email, $dataEmail) {
                    $message->to($email)->subject($dataEmail['subject']);
                }
            );

            Log::app()->info("The reset password email can be send to {$email}");
        } catch (Throwable $th) {
            Log::app()->error(
                "The reset password email can be send to {$email}",
                ['error' => $th->getMessage()]
            );
        }
    }
}
