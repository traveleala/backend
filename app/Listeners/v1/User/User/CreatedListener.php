<?php

namespace App\Listeners\v1\User;

use App\Events\v1\User\CreatedEvent;
use App\Classes\v1\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Throwable;

class CreatedListener implements ShouldQueue
{
    public function handle(CreatedEvent $event)
    {
        $email = $event->userModel->email;
        $token = $event->dataEmail['token'];
        $subject = $event->dataEmail['subject'];

        try {
            Mail::send(
                'v1.emails.verificationAccount',
                [
                    'token' => $token
                ],
                function ($message) use ($email, $subject) {
                    $message->to($email)->subject($subject);
                }
            );

            Log::app()->info("The verification email can be send to {$email}");
        } catch (Throwable $th) {
            Log::app()->error("The verification email can't be send to {$email}", [
                'error' => $th->getMessage()
            ]);
        }
    }
}
