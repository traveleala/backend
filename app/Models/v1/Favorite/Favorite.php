<?php

namespace App\Models\v1\Favorite;

use App\Models\v1\Activity\Activity;
use App\Models\v1\Base;
use Illuminate\Database\Eloquent\SoftDeletes;

class Favorite extends Base
{
    use SoftDeletes;

    protected $table = 'activity_favorite';
    protected $fillable = [
        'user_id',
        'activity_id',
    ];

    public function activity()
    {
        return $this->hasOne(Activity::class, 'id', 'activity_id');
    }
}
