<?php

namespace App\Models\v1\Report;

use App\Models\v1\Activity\Activity;
use App\Models\v1\Base;
use App\Models\v1\Qualification\Qualification;
use App\Repositories\v1\Report\StateRepository;
use App\Models\v1\User\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class Report extends Base
{
    use SoftDeletes;

    protected $table = 'report';
    protected $fillable = [
        'comment',
        'user_id',
        'activity_id',
        'activity_qualification_id',
        'report_type_id',
        'report_state_id'
    ];

    public function type()
    {
        return $this->hasOne(Type::class, 'id', 'report_type_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function activity()
    {
        return $this->hasOne(Activity::class, 'id', 'activity_id');
    }

    public function qualification()
    {
        return $this->hasOne(Qualification::class, 'id', "activity_qualification_id");
    }

    public function scopePending($query)
    {
        $stateId = StateRepository::getStatePending()->id;

        return $query->where('report_state_id', $stateId);
    }
}
