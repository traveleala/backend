<?php

namespace App\Models\v1\Report;

use App\Models\v1\Base;

class Type extends Base
{
    protected $table = 'report_type';
    protected $fillable = [
        'name'
    ];
}
