<?php

namespace App\Models\v1\Report;

use App\Models\v1\Base;

class State extends Base
{
    protected $table = 'report_state';
    protected $fillable = [
        'name'
    ];
}
