<?php

namespace App\Models\v1\Report;

use App\Models\v1\Base;

class Action extends Base
{
    protected $table = 'report_action';
    protected $fillable = [
        'name'
    ];
}
