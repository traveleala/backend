<?php

namespace App\Models\v1\Activity;

use App\Models\v1\Base;
use ShiftOneLabs\LaravelCascadeDeletes\CascadesDeletes;
use App\Models\v1\Favorite\Favorite;
use App\Models\v1\Qualification\Qualification;
use App\Models\v1\Report\Report;
use App\Models\v1\User\Profile;
use App\Repositories\v1\Activity\StateRepository;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\v1\User\User;

class Activity extends Base
{
    use SoftDeletes;
    use CascadesDeletes;

    protected $table = 'activity';
    protected $fillable = [
        'title',
        'description',
        'price',
        'latitude',
        'longitude',
        'user_id',
        'activity_state_id',
        'is_landscape',
        'location',
        'activity_type_id',
        'activity_difficulty_id',
        'activity_publish_type_id'
    ];

    public $cascadeDeletes = [
        'subscription',
        'qualifications',
        'favorites',
        'reports'
    ];

    public function favorites()
    {
        return $this->hasMany(Favorite::class, 'activity_id');
    }

    public function contact()
    {
        return $this->hasOne(Contact::class, 'activity_id');
    }

    public function images()
    {
        return $this->hasMany(Image::class, 'activity_id');
    }

    public function characteristics()
    {
        return $this->hasMany(Characteristic::class, 'activity_id');
    }

    public function portrait()
    {
        return $this->hasOne(Image::class, 'activity_id');
    }

    public function subscription()
    {
        return $this->hasOne(Subscription::class, 'activity_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function qualifications()
    {
        return $this->hasMany(Qualification::class, 'activity_id');
    }

    public function profile()
    {
        return $this->belongsTo(Profile::class, 'user_id', 'user_id');
    }

    public function reports()
    {
        return $this->hasMany(Report::class, 'activity_id');
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'activity_state_id', 'id');
    }

    public function type()
    {
        return $this->belongsTo(Type::class, 'activity_type_id', 'id');
    }

    public function difficulty()
    {
        return $this->belongsTo(Difficulty::class, 'activity_difficulty_id', 'id');
    }

    public function scopeAvailable($query)
    {
        $stateId = StateRepository::getAvailable()->id;

        return $query->where('activity_state_id', $stateId);
    }

    public function scopePaymentPending($query)
    {
        $stateId = StateRepository::getPaymentPending()->id;

        return $query->where('activity_state_id', $stateId);
    }

    public function scopeTypes($query, $types)
    {
        if ($types) {
            return $query->whereIn('activity_type_id', $types);
        }
    }

    public function scopeDifficulties($query, $difficulties)
    {
        if ($difficulties) {
            return $query->whereIn('activity_difficulty_id', $difficulties);
        }
    }

    public function scopeLocation($query, $location)
    {
        if ($location) {
            $location = !strpos($location, ',') ? ', ' . $location : $location;

            if ($location) {
                return $query->where('location', 'like', "%{$location}%");
            }
        }
    }

    public function scopeTitle($query, $title)
    {
        if ($title) {
            return $query->where('title', 'like', '%' . $title . '%');
        }
    }

    public function scopePrice($query, $min, $max)
    {
        if ((is_null($min) && is_null($max)) || (empty($min) && empty($max))) {
            return $query;
        }

        if (!is_null($min) && !is_null($max)) {
            return $query->whereBetween('price', [$min, $max]);
        }

        if (is_null($min)) {
            return $query->where('price', '<=', $max);
        }

        if (is_null($max)) {
            return $query->where('price', '>=', $min);
        }
    }

    public function scopeLandscape($query, $landscape)
    {
        if (is_null($landscape) || $landscape == '') {
            return $query;
        }

        $landscape = $landscape == 'false' ? false : true;

        return $query->whereIsLandscape($landscape);
    }

    public function scopeDistance($query, $kms, $coordinates)
    {
        if (!$coordinates || !$kms) {
            return $query;
        }

        $coordinates = explode(',', $coordinates);
        $latitude = $coordinates[0];
        $longitude = $coordinates[1];

        $haversine = "(6371 * acos(cos(radians($latitude)) 
                     * cos(radians(activity.latitude)) 
                     * cos(radians(activity.longitude) 
                     - radians($longitude)) 
                     + sin(radians($latitude))
                     * sin(radians(activity.latitude))))";

        return $query
            ->select()
            ->selectRaw("{$haversine} AS distance")
            ->whereRaw("{$haversine} < ?", [$kms]);
    }

    private static function getQualification(string $type, $value = null)
    {
        $query = Qualification::selectRaw(
            'IFNULL(REPLACE(FORMAT(sum(qualification) / count(activity_qualification.id),1),".",","),0) qualification'
        );

        switch ($type) {
            case 'activity':
                $query->whereColumn('activity_id', 'activity.id');
                break;
            case 'activity_id':
                $query->where('activity_id', $value);
                break;
            case 'favorite':
                $query->whereColumn('activity_id', 'activity_favorite.activity_id');
                break;
        }

        $query->groupBy('activity_id');

        return $query;
    }

    public static function getQualificationBy(string $type, $value = null)
    {
        return [
            'qualification' => self::getQualification($type, $value)
        ];
    }
}
