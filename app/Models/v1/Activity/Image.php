<?php

namespace App\Models\v1\Activity;

use App\Models\v1\BaseImage;

class Image extends BaseImage
{
    protected $table = 'activity_image';
    protected $fillable = [
        'image_url',
        'activity_id'
    ];

    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }
}
