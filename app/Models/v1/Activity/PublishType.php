<?php

namespace App\Models\v1\Activity;

use App\Models\v1\Base;

class PublishType extends Base
{
    protected $table = 'activity_publish_type';
    protected $fillable = [
        'name',
        'price',
    ];
}
