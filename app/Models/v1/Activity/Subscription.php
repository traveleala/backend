<?php

namespace App\Models\v1\Activity;

use App\Models\v1\Base;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends Base
{
    use SoftDeletes;

    protected $table = 'activity_subscription';
    protected $fillable = [
        'month',
        'amount',
        'paid_out',
        'activity_publish_type_id',
        'activity_id',
        'expire_at'
    ];

    public function publishType()
    {
        return $this->belongsTo(PublishType::class, 'activity_publish_type_id', 'id');
    }
}
