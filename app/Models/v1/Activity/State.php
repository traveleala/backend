<?php

namespace App\Models\v1\Activity;

use App\Models\v1\Base;

class State extends Base
{
    protected $table = 'activity_state';
    protected $fillable = [
        'name'
    ];
}
