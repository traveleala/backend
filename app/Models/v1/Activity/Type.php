<?php

namespace App\Models\v1\Activity;

use App\Models\v1\Base;

class Type extends Base
{
    protected $table = 'activity_type';
    protected $fillable = [
        'name',
    ];
}
