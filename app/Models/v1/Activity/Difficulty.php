<?php

namespace App\Models\v1\Activity;

use App\Models\v1\Base;

class Difficulty extends Base
{
    protected $table = 'activity_difficulty';
    protected $fillable = [
        'name',
    ];
}
