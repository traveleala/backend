<?php

namespace App\Models\v1\Activity;

use App\Models\v1\Base;

class Characteristic extends Base
{
    protected $table = 'activity_characteristic';
    protected $fillable = [
        'concept',
        'value',
        'activity_id'
    ];

    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }
}
