<?php

namespace App\Models\v1\Activity;

use App\Models\v1\Base;
use betterapp\LaravelDbEncrypter\Traits\EncryptableDbAttribute;

class Contact extends Base
{
    use EncryptableDbAttribute;

    protected $table = 'activity_contact';
    protected $fillable = [
        'phone_number',
        'telephone_number',
        'facebook_url',
        'instagram_url',
        'email',
        'activity_id',
        'web'
    ];

    protected $encryptable = [
        'phone_number',
        'telephone_number',
        'facebook_url',
        'instagram_url',
        'email',
        'web'
    ];
}
