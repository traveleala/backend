<?php

namespace App\Models\v1\Qualification;

use App\Models\v1\Base;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vote extends Base
{
    use SoftDeletes;

    protected $table = 'activity_qualification_vote';
    protected $fillable = [
        'user_id',
        'activity_qualification_id'
    ];
}
