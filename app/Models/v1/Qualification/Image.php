<?php

namespace App\Models\v1\Qualification;

use App\Models\v1\BaseImage;

class Image extends BaseImage
{
    protected $table = 'activity_qualification_image';
    public $timestamps = false;
    protected $fillable = [
        'image_url',
        'activity_qualification_id'
    ];
}
