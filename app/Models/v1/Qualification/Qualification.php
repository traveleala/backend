<?php

namespace App\Models\v1\Qualification;

use App\Models\v1\Activity\Activity;
use App\Models\v1\Base;
use ShiftOneLabs\LaravelCascadeDeletes\CascadesDeletes;
use App\Models\v1\Activity\Difficulty;
use App\Models\v1\Report\Report;
use App\Models\v1\User\Profile;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\v1\User\User;

class Qualification extends Base
{
    use SoftDeletes;
    use CascadesDeletes;

    protected $table = 'activity_qualification';
    protected $fillable = [
        'review',
        'qualification',
        'activity_id',
        'user_id',
        'activity_difficulty_id',
        'activity_date'
    ];

    protected $cascadeDeletes = [
        'votes',
        'reports'
    ];

    public function images()
    {
        return $this->hasMany(Image::class, 'activity_qualification_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function profile()
    {
        return $this->belongsTo(Profile::class, 'user_id', 'user_id');
    }

    public function activity()
    {
        return $this->belongsTo(Activity::class, 'activity_id');
    }

    public function reports()
    {
        return $this->hasMany(Report::class, 'activity_qualification_id');
    }

    public function votes()
    {
        return $this->hasMany(Vote::class, 'activity_qualification_id');
    }

    public function difficulty()
    {
        return $this->belongsTo(Difficulty::class, 'activity_difficulty_id');
    }
}
