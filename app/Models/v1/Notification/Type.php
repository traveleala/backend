<?php

namespace App\Models\v1\Notification;

use App\Models\v1\Base;

class Type extends Base
{
    protected $table = 'notification_type';
    protected $fillable = [
        'name',
        'description'
    ];
}
