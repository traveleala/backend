<?php

namespace App\Models\v1\Notification;

use App\Models\v1\Activity\Activity;
use App\Models\v1\Base;
use App\Models\v1\User\Profile;
use App\Models\v1\User\User;
use App\Repositories\v1\Notification\TypeRepository;
use App\Values\v1\NotificationTypeValues;

class Notification extends Base
{
    protected $table = 'notification';
    protected $fillable = [
        'user_id',
        'notification_type_id',
        'read',
        'user_action_id',
        'activity_id'
    ];

    public function type()
    {
        return $this->hasOne(Type::class, 'id', 'notification_type_id');
    }

    public function profile()
    {
        return $this->belongsTo(Profile::class, 'user_action_id', 'user_id');
    }

    public function activity()
    {
        return $this->belongsTo(Activity::class, 'activity_id', 'id')->withTrashed();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->withTrashed();
    }

    public function scopeUnread($query)
    {
        return $query->whereRead(false);
    }

    public function scopeTypeIsUserDeleted($query)
    {
        $id = TypeRepository::getByName(NotificationTypeValues::USER_DELETED)->id;

        return $query->whereNotificationTypeId($id);
    }
}
