<?php

namespace App\Models\v1\User;

use App\Models\v1\Activity\Activity;
use App\Models\v1\Base;
use App\Models\v1\Favorite\Favorite;
use Illuminate\Support\Facades\DB;
use App\Models\v1\Qualification\Qualification;
use App\Models\v1\Qualification\Vote;
use App\Models\v1\Report\Report;
use Illuminate\Database\Eloquent\SoftDeletes;
use ShiftOneLabs\LaravelCascadeDeletes\CascadesDeletes;

class User extends Base
{
    use SoftDeletes;
    use CascadesDeletes;

    protected $table = 'user';

    protected $fillable = [
        'email',
        'password',
        'user_role_id',
        'verified'
    ];

    protected $hidden = [
        'password',
    ];

    public $cascadeDeletes = [
        'profile',
        'reports',
        'qualifications',
        'activities',
        'favorites',
        'votes',
        'followers',
        'followings'
    ];

    public static function boot()
    {
        parent::boot();
    }

    public function verification()
    {
        return $this->hasOne(Verification::class, 'user_id');
    }

    public function resetPassword()
    {
        return $this->hasOne(ResetPassword::class, 'user_id');
    }

    public function profile()
    {
        return $this->hasOne(Profile::class, 'user_id', 'id');
    }

    public function favorites()
    {
        return $this->hasMany(Favorite::class, 'user_id');
    }

    public function reports()
    {
        return $this->hasMany(Report::class, 'user_id');
    }

    public function qualifications()
    {
        return $this->hasMany(Qualification::class, 'user_id');
    }

    public function role()
    {
        return $this->hasOne(Role::class, 'id', 'user_role_id');
    }

    public function activities()
    {
        return $this->hasMany(Activity::class, 'user_id');
    }

    public function votes()
    {
        return $this->hasMany(Vote::class, 'user_id', 'id');
    }

    public function followers()
    {
        return $this->hasMany(Follow::class, 'user_follower_id', 'id');
    }

    public function followings()
    {
        return $this->hasMany(Follow::class, 'user_following_id', 'id');
    }

    public function scopeRoleId($query, $userRoleId)
    {
        return $query->whereUserRoleId($userRoleId);
    }

    private static function getQualificationUserBy($condition, $value)
    {
        $query = [];
        $queryObject = Qualification::selectRaw(
            'IFNULL(REPLACE(FORMAT(sum(activity_qualification.qualification) / count(activity_qualification.id),1),".",","),0) qualification'
        )
            ->leftJoin('activity', 'activity_qualification.activity_id', 'activity.id');

        switch ($condition) {
            case 'activityId':
                $queryObject->where(
                    'activity.user_id',
                    DB::raw('(select user_id from activity where id = ' . $value . ' limit 1)')
                );
                break;
            case 'username':
                $queryObject->where(
                    'activity.user_id',
                    DB::raw('(select user_id from user_profile where username = "' . $value . '" limit 1)')
                );
                break;
            case 'userId':
                $queryObject->where(
                    'activity.user_id',
                    $value
                );
                break;
        }

        $query['user_qualification'] = $queryObject;

        return $query;
    }

    public static function getQualificationUserByActivityId($id)
    {
        return self::getQualificationUserBy('activityId', $id);
    }

    public static function getQualificationUserByUsername($username)
    {
        return self::getQualificationUserBy('username', $username);
    }

    public static function getQualificationUserByUserId($userId)
    {
        return self::getQualificationUserBy('userId', $userId);
    }
}
