<?php

namespace App\Models\v1\User;

use App\Models\v1\Base;
use Illuminate\Database\Eloquent\SoftDeletes;

class Follow extends Base
{
    use SoftDeletes;

    protected $table = 'user_follow';
    protected $fillable = [
        'user_follower_id',
        'user_following_id'
    ];

    public function follower()
    {
        return $this->belongsTo(Profile::class, 'user_follower_id', 'user_id');
    }

    public function following()
    {
        return $this->belongsTo(Profile::class, 'user_following_id', 'user_id');
    }
}
