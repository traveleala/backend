<?php

namespace App\Models\v1\User;

use App\Models\v1\Base;

class Role extends Base
{
    protected $table = 'user_role';
    protected $fillable = [
        'name'
    ];
}
