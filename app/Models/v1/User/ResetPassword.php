<?php

namespace App\Models\v1\User;

use App\Models\v1\Base;

class ResetPassword extends Base
{
    protected $table = 'user_password_reset';
    protected $fillable = [
        'token',
        'user_id'
    ];
}
