<?php

namespace App\Models\v1\User;

use App\Models\v1\Base;

class Verification extends Base
{
    protected $table = 'user_verification';
    protected $fillable = [
        'token',
        'user_id'
    ];
}
