<?php

namespace App\Models\v1\User;

use App\Models\v1\Activity\Activity;
use App\Models\v1\Base;
use App\Models\v1\Qualification\Qualification;
use App\Repositories\v1\Activity\StateRepository;
use Illuminate\Database\Eloquent\SoftDeletes;
use betterapp\LaravelDbEncrypter\Traits\EncryptableDbAttribute;

class Profile extends Base
{
    use SoftDeletes;
    use EncryptableDbAttribute;

    protected $table = 'user_profile';
    protected $fillable = [
        'full_name',
        'user_id',
        'profile_image_url',
        'portrait_url',
        'username',
        'change_restricted_at'
    ];

    protected $encryptable = [
        'full_name',
    ];

    public function activities()
    {
        return $this->hasMany(Activity::class, 'user_id', 'user_id');
    }

    public function activitiesAvailable()
    {
        $stateId = StateRepository::getAvailable()->id;

        return $this->hasMany(Activity::class, 'user_id', 'user_id')->where('activity_state_id', '=', $stateId);
    }

    public function qualifications()
    {
        return $this->hasMany(Qualification::class, 'user_id', 'user_id');
    }

    public function followers()
    {
        return $this->hasMany(Follow::class, 'user_following_id', 'user_id');
    }

    public function followings()
    {
        return $this->hasMany(Follow::class, 'user_follower_id', 'user_id');
    }
}
