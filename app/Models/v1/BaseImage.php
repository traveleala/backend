<?php

namespace App\Models\v1;

use App\Services\v1\FileService;

class BaseImage extends Base
{
    public function rollback(FileService $fileService)
    {
        return $fileService->delete($this->image_url);
    }
}
