<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class Base extends Model
{
    public static function getTableName()
    {
        $class = get_called_class();
        $model = new $class;
        return $model->getTable();
    }

    public static function boot()
    {
        parent::boot();
        self::saving(function ($model) {

            $userIdExists = array_search('user_id', $model->fillable);

            if ($userIdExists !== false && !$model->user_id) {
                $model->user_id = user()->id;
            }
        });
    }

    public function scopeUserId($query, $userId = '')
    {
        $id = $userId ? $userId : user()->id;
        return $query->whereUserId($id);
    }
}
