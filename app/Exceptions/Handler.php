<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use App\Exceptions\v1\BadRequestException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use App\Values\v1\HttpCodeValues;
use App\Exceptions\v1\LimitException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Http\Response;
use App\Exceptions\v1\SuccessException;
use Throwable;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class Handler extends ExceptionHandler
{
    protected $dontReport = [
        BadRequestException::class,
        NotFoundHttpException::class,
        LimitException::class,
        ModelNotFoundException::class,
        ValidationException::class,
        UnauthorizedException::class,
        AuthorizationException::class,
        MethodNotAllowedHttpException::class,
        SuccessException::class,
        UnprocessableEntityHttpException::class
    ];

    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    public function report(Throwable $exception)
    {
        if (app()->bound('sentry') && $this->shouldReport($exception)) {
            app('sentry')->captureException($exception);
        }

        parent::report($exception);
    }

    private function returnModelNotFoundException(Throwable $exception)
    {
        return response()->json([
            'success' => false,
            'message' => "The resource does not exist",
            'status' => HttpCodeValues::NOT_FOUND_CODE
        ], HttpCodeValues::NOT_FOUND_CODE);
    }

    private function returnValidationException(Throwable $exception)
    {
        return Response::errors(
            $exception->validator->errors(),
            $exception->validator->failed(),
            $exception->errorBag
        );
    }

    private function returnInternalErrorResponse()
    {
        return response()->json([
            'success' => false,
            'message' => 'Internal server error',
            'status' => HttpCodeValues::INTERNAL_SERVER_ERROR_CODE
        ], HttpCodeValues::INTERNAL_SERVER_ERROR_CODE);
    }

    private function returnUnauthorizedResponse()
    {
        return response()->json([
            'success' => false,
            'message' => 'Unauthorized',
            'status' => HttpCodeValues::UNAUTHORIZED_CODE
        ], HttpCodeValues::UNAUTHORIZED_CODE);
    }

    private function returnForbiddenResponse()
    {
        return response()->json([
            'success' => false,
            'message' => 'Forbidden',
            'status' => HttpCodeValues::FORBIDDEN_CODE
        ], HttpCodeValues::FORBIDDEN_CODE);
    }

    private function returnMethodNotAllowed()
    {
        return response()->json([
            'success' => false,
            'message' => 'Method not allowed',
            'status' => HttpCodeValues::METHOD_NOT_ALLOWED_CODE
        ], HttpCodeValues::METHOD_NOT_ALLOWED_CODE);
    }

    private function returnLimitResponse()
    {
        return response()->json([
            'success' => false,
            'message' => 'Limit quota',
            'status' => HttpCodeValues::LIMIT_QUOTA_CODE
        ], HttpCodeValues::LIMIT_QUOTA_CODE);
    }

    private function returnUnprocessableEntity()
    {
        return response()->json([
            'success' => false,
            'message' => 'Unprocessable entity',
            'status' => HttpCodeValues::UNPROCESSABLE_ENTITY_CODE
        ], HttpCodeValues::UNPROCESSABLE_ENTITY_CODE);
    }

    private function returnBadRequestException()
    {
        return response()->json([
            'success' => false,
            'message' => 'Bad request',
            'status' => HttpCodeValues::INVALID_REQUEST_CODE
        ], HttpCodeValues::INVALID_REQUEST_CODE);
    }

    private function returnSuccess($key = '')
    {
        $response = [
            'status' => HttpCodeValues::OK_CODE,
            'success' => true
        ];

        if ($key) {
            $response[$key] = true;
        }

        return response()->json($response, HttpCodeValues::OK_CODE);
    }

    public function render($request, Throwable $exception)
    {
        $response = true;
        $exceptionClass = get_class($exception);

        switch ($exceptionClass) {
            case NotFoundHttpException::class:
                $response = response()->json([
                    'status' => HttpCodeValues::NOT_FOUND_CODE,
                    'success' => false,
                    'message' => "Not found",
                ], HttpCodeValues::NOT_FOUND_CODE);
                break;
            case LimitException::class:
                $response = $this->returnLimitResponse();
                break;
            case ModelNotFoundException::class:
                $response = $this->returnModelNotFoundException($exception);
                break;
            case ValidationException::class:
                $response = $this->returnValidationException($exception);
                break;
            case UnauthorizedException::class:
                $response = $this->returnUnauthorizedResponse();
                break;
            case AuthorizationException::class:
                $response = $this->returnForbiddenResponse();
                break;
            case MethodNotAllowedHttpException::class:
                $response = $this->returnMethodNotAllowed();
                break;
            case SuccessException::class:
                $response = $this->returnSuccess($exception->getMessage());
                break;
            case UnprocessableEntityHttpException::class:
                $response = $this->returnUnprocessableEntity($exception->getMessage());
                break;
            case BadRequestException::class:
                $response = $this->returnBadRequestException();
                break;
            default:
                if (conf('app.debug')) {
                    $response = response($exception->getMessage(), 500);
                } else {
                    $response = $this->returnInternalErrorResponse();
                }
                break;
        }

        return $response;
    }
}
