<?php

use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Classes\v1\Hasher;
use Illuminate\Support\Facades\Redis;
use App\Services\v1\AuthService;
use App\Values\v1\UserRoleValues;

if (!function_exists('request')) {
    function request()
    {
        return app('request');
    }
}

if (!function_exists('bcrypt')) {
    function bcrypt($value, $options = [])
    {
        return app('hash')->make($value, $options);
    }
}

if (!function_exists('login')) {
    function login($id)
    {
        return AuthService::login($id);
    }
}

if (!function_exists('user')) {
    function user()
    {
        return config('app.user');
    }
}

if (!function_exists('conf')) {

    function conf($key, $default = null)
    {
        $keys = explode('.', $key);
        $fileName = reset($keys);
        $countKeys = count($keys);
        $prefix = 'app';

        if (config('app.cache')) {
            $valuesConfig = Cache::rememberForever($prefix . $fileName, function () use ($fileName) {
                return config($fileName);
            });

            if ($countKeys > 1) {
                unset($keys[0]);
                foreach ($keys as $key) {
                    $valuesConfig = @$valuesConfig[$key];
                }
            }

            return $valuesConfig ? $valuesConfig : $default;
        } else {
            return config($key, $default);
        }
    }
}

if (!function_exists('cache')) {
    function cache()
    {
        $arguments = func_get_args();

        if (empty($arguments)) {
            return app('cache');
        }

        if (is_string($arguments[0])) {
            return app('cache')->get(...$arguments);
        }

        if (!is_array($arguments[0])) {
            throw new Exception(
                'When setting a value in the cache, you must pass an array of key / value pairs.'
            );
        }

        return app('cache')->put(key($arguments[0]), reset($arguments[0]), $arguments[1] ?? null);
    }
}

if (!function_exists('now')) {
    function now()
    {
        return Carbon::now();
    }
}

if (!function_exists('removeCache')) {
    function removeCache($keysToDelete)
    {
        if (is_string($keysToDelete)) {
            $keysToDelete = [$keysToDelete];
        }

        foreach ($keysToDelete as $keyToDelete) {
            $redis = Redis::connection('cache');
            $keys = $redis->keys("*{$keyToDelete}*");

            array_walk($keys, function ($key) use ($redis) {
                $redis->del($key);
            });
        }
    }
}

if (!function_exists('password')) {
    function password($plainPassword, $hashedPassword = null)
    {
        if ($plainPassword) {
            return Hash::make($plainPassword);
        } else {
        }
    }
}

if (!function_exists('isSubdomain')) {
    function isSubdomain()
    {
        if ($_SERVER['HTTP_HOST'] != conf('app.api_url')) {
            abort(404);
        }
    }
}

if (!function_exists('keyCache')) {
    function keyCache(...$args)
    {
        return implode(':', $args);
    }
}

if (!function_exists('encode')) {
    function encode($value)
    {
        return Hasher::instance()->encode($value);
    }
}

if (!function_exists('decode')) {
    function decode($value)
    {
        return @Hasher::instance()->decode($value)[0];
    }
}

if (!function_exists('yearsOfUser')) {
    function yearsOfUser($year)
    {
        $now = now();
        $yearCreatedAt = user()->created_at->format('Y');
        $differenceYears = user()->created_at->diffInYears($now);

        $listYears = [];

        for ($i = 0; $i <= $differenceYears; $i++) {
            $listYears[] = $yearCreatedAt + $i;
        }

        return in_array($year, $listYears) ? $year : $now->format('Y');
    }
}

if (!function_exists('config_path')) {
    function config_path($path = '')
    {
        return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
    }
}

if (!function_exists('app_path')) {
    function app_path($path = '')
    {
        return app()->path($path);
    }
}

if (!function_exists('isUser')) {
    function isUser()
    {
        return user()->role->name == UserRoleValues::USER_ROLE;
    }
}

if (!function_exists('isNotUser')) {
    function isNotUser()
    {
        return user()->role->name == UserRoleValues::ADMIN_ROLE || user()->role->name == UserRoleValues::PUBLISHER_ROLE;
    }
}

if (!function_exists('isAdmin')) {
    function isAdmin()
    {
        return user()->role->name == UserRoleValues::ADMIN_ROLE;
    }
}

if (!function_exists('linkUrl')) {
    function linkUrl(...$paths)
    {
        return implode('/', $paths);
    }
}


if (!function_exists('urlToHtml')) {
    function urlToHtml($path, $name)
    {
        return "<a href='{$path}'>{$name}</a>";
    }
}
