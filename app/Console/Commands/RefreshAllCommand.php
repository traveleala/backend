<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Console\Command;

class RefreshAllCommand extends Command
{
    protected $signature = 'refresh:all';
    protected $description = 'Clean database and redis and migrate';

    public function handle()
    {
        Artisan::call('migrate:fresh');
        Artisan::call('cache:clear');
        Artisan::call('db:seed');
        Artisan::call('top:activities');
        Artisan::call('location:activities');
    }
}
