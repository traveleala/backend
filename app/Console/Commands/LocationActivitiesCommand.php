<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\v1\LocationService;

class LocationActivitiesCommand extends Command
{
    protected $signature = 'location:activities';
    protected $description = 'Get all available locations for activities';
    private $locationService;

    public function __construct(LocationService $locationService)
    {
        parent::__construct();
        $this->locationService = $locationService;
    }
    public function handle()
    {
        $this->locationService->process();
    }
}
