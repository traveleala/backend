<?php

namespace App\Console\Commands;

use App\Repositories\v1\Activity\ActivityRepository;
use Illuminate\Console\Command;
use App\Classes\v1\Log;

class UpdateOutDatedSubscriptionsActivitiesCommand extends Command
{
    protected $signature = 'updateOutdatedSubscriptions';
    protected $description = 'Update activities with any out dated subscriptions';

    public function handle()
    {
        Log::activities()->info('Update activities with any out dated subscriptions');
        $quantity = ActivityRepository::updateOutdatedActivities();
        Log::activities()->info("Activities updated: {$quantity}");
    }
}
