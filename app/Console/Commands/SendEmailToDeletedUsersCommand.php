<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\v1\NotificationService;

class SendEmailToDeletedUsersCommand extends Command
{
    protected $signature = 'sendEmailToDeletedUsers';
    protected $description = 'send notifications to deleted users';
    private $notificationService;

    public function __construct(NotificationService $notificationService)
    {
        parent::__construct();
        $this->notificationService = $notificationService;
    }

    public function handle()
    {
        $this->notificationService->notify();
    }
}
