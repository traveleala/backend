<?php

namespace App\Console\Commands;

use App\Repositories\v1\Activity\ActivityRepository;
use Illuminate\Console\Command;

class TopActivitiesCommand extends Command
{
    protected $signature = 'top:activities';
    protected $description = 'Get top activities published';

    public function handle()
    {
        ActivityRepository::removeTopActivities();
        ActivityRepository::top();
    }
}
