<?php

namespace App\Console;

use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use App\Console\Commands\LocationActivitiesCommand;
use App\Console\Commands\RefreshAllCommand;
use Illuminate\Console\Scheduling\Schedule;
use App\Console\Commands\SendEmailToDeletedUsersCommand;
use App\Console\Commands\TopActivitiesCommand;
use App\Console\Commands\UpdateOutDatedSubscriptionsActivitiesCommand;


class Kernel extends ConsoleKernel
{
    protected $commands = [
        TopActivitiesCommand::class,
        RefreshAllCommand::class,
        LocationActivitiesCommand::class,
        UpdateOutDatedSubscriptionsActivitiesCommand::class,
        SendEmailToDeletedUsersCommand::class
    ];

    protected function schedule(Schedule $schedule)
    {
        $schedule->command(UpdateOutDatedSubscriptionsActivitiesCommand::class)->hourly();
        $schedule->command(SendEmailToDeletedUsersCommand::class)->hourly();
    }
}
