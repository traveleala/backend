<?php

namespace App\Repositories\v1;

use App\Classes\v1\Cache;
use Illuminate\Database\Eloquent\ModelNotFoundException;

abstract class BaseRepository
{
    public static function getTableName()
    {
        return static::$model::getTableName();
    }

    public static function firstBy($column, $value)
    {
        return static::$model
            ::where($column, $value)
            ->first();
    }

    public static function existsFirstBy($column, $value)
    {
        return static::$model
            ::where($column, $value)
            ->exists();
    }

    public static function getFirstOrFailBy($column, $value, $select = [])
    {
        $query = static::$model
            ::where($column, $value);

        if ($select) {
            $query->select(...$select);
        }

        return $query->firstOrFail();
    }

    public static function existsOrFailBy($column, $value)
    {
        $result = static::$model
            ::where($column, $value)
            ->exists();

        if (!$result) {
            throw new ModelNotFoundException;
        }

        return $result;
    }

    public static function instance(array $properties)
    {
        return new static::$model($properties);
    }

    public static function getByIdWithCacheForever($id)
    {
        $key = self::getCacheKey($id);

        return self::withCacheForever(function () use ($id) {
            return static::getFirstOrFailBy('id', $id);
        }, $key);
    }

    public static function withCache($closure, $key = '')
    {
        $key = $key ? $key : self::getCacheKey();
        return Cache::rememberDefault($closure, $key);
    }

    public static function withCacheHourly($closure, $key = '')
    {
        $key = $key ? $key : self::getCacheKey();
        return Cache::rememberForAnHour($closure, $key);
    }

    public static function withCacheWeekly($closure, $key = '')
    {
        $key = $key ? $key : self::getCacheKey();
        return Cache::rememberForAWeek($closure, $key);
    }

    public static function withCacheForever($closure, $key = '')
    {
        $key = $key ? $key : self::getCacheKey();
        return Cache::rememberForever($closure, $key);
    }

    public static function all()
    {
        return static::$model::get();
    }

    public static function insert(array $properties)
    {
        return static::$model::insert($properties);
    }

    public static function store(array $properties)
    {
        return static::$model::create($properties);
    }

    public static function truncate()
    {
        return static::$model::truncate();
    }

    public static function getCacheKey(...$args)
    {
        $keyCacheModel = static::CACHE_PREFIX;
        if ($keyCacheModel) {
            array_unshift($args, $keyCacheModel);
        }
        return keyCache(...$args);
    }
}
