<?php

namespace App\Repositories\v1\Admin\Report;

use App\Repositories\v1\BaseRepository;
use DB;
use App\Models\v1\Report\Report;
use App\Repositories\v1\Notification\NotificationRepository;
use App\Values\v1\NotificationTypeValues;
use Throwable;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class ReportRepository extends BaseRepository
{
    const CACHE_PREFIX = 'admin:reports';

    public static $model = Report::class;

    public static function get($page = 1)
    {
        $key = self::getCacheKey($page);

        return self::withCache(function () {
            return self::$model
                ::pending()
                ->with(
                    'type',
                    'user:id',
                    'user.profile:user_id,username'
                )
                ->orderBy('created_at', 'asc')
                ->simplePaginate();
        }, $key);
    }

    public static function getReportAvailableById($id)
    {
        return self::$model
            ::select('activity_id')
            ->whereId($id)
            ->pending()
            ->firstOrFail();
    }

    public static function update($id, $action)
    {
        $report = self::getFirstOrFailBy('id', $id);

        try {
            DB::beginTransaction();

            $problematicUserId = '';
            $activityId = null;

            switch ($action) {
                case 'activity':
                    $problematicUserId = $report->activity->user_id;
                    $notificationType = NotificationTypeValues::ACTIVITY_DELETED;
                    $activityId = $report->activity->id;
                    $report->activity->delete();
                    break;

                case 'qualification':
                    $problematicUserId = $report->qualification->user_id;
                    $notificationType = NotificationTypeValues::QUALIFICATION_DELETED;
                    $activityId = $report->qualification->activity->id;
                    $report->qualification->delete();
                    break;

                case 'user':
                    $userToDelete = $report->activity_id ? $report->activity->user : $report->qualification->user;
                    $problematicUserId = $userToDelete->id;
                    $notificationType = NotificationTypeValues::USER_DELETED;
                    $userToDelete->delete();
                    break;

                default:
                    break;
            }

            if ($problematicUserId) {
                NotificationRepository::notify(
                    $notificationType,
                    $problematicUserId,
                    $activityId
                );
            }

            $report->delete();

            DB::commit();

            removeCache(self::getCacheKey());

            return true;
        } catch (Throwable $th) {
            DB::rollBack();
            throw new UnprocessableEntityHttpException;
        }
    }
}
