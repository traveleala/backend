<?php

namespace App\Repositories\v1\Admin\Report;

use App\Repositories\v1\BaseRepository;
use App\Values\v1\ReportStateValues;
use App\Models\v1\Report\State;

class StateRepository extends BaseRepository
{
    const CACHE_PREFIX = 'reports:states';

    public static $model = State::class;

    public static function getStateFavorable()
    {
        return self::getByName(ReportStateValues::FAVORABLE_RESULT_STATE);
    }

    public static function getStateUnfavorable()
    {
        return self::getByName(ReportStateValues::UNFAVORABLE_RESULT_STATE);
    }

    private static function getByName($name)
    {
        $key = self::getCacheKey($name);

        return self::withCacheForever(function () use ($name) {
            return self::getFirstOrFailBy('name', $name);
        }, $key);
    }
}
