<?php

namespace App\Repositories\v1\Admin\Report;

use App\Models\v1\Report\Action;
use App\Repositories\v1\BaseRepository;

class ActionRepository extends BaseRepository
{
    const CACHE_PREFIX = 'reports:actions';

    public static $model = Action::class;

    public static function getByEntity($entity)
    {
        $key = self::getCacheKey('entity', $entity);

        return self::withCacheForever(function ()  use ($entity) {
            return self::$model
                ::whereEntity($entity)
                ->get();
        }, $key);
    }

    public static function getByName($name)
    {
        $key = self::getCacheKey('name', $name);

        return self::withCacheForever(function ()  use ($name) {
            return self::$model
                ::whereName($name)
                ->firstOrFail();
        }, $key);
    }
}
