<?php

namespace App\Repositories\v1\Admin\User;

use App\Repositories\v1\BaseRepository;
use App\Repositories\v1\User\RoleRepository;
use App\Models\v1\User\User;

class UserRepository extends BaseRepository
{
    public static $model = User::class;

    const CACHE_PREFIX = 'admin:users';
    const RELATIONSHIPS_GET = [
        'profile:user_id,username',
        'role'
    ];

    public static function store(array $properties)
    {
        $roleUserId = RoleRepository::getByName($properties['role'])->id;

        $properties = [
            'email' => $properties['email'],
            'password' => password($properties['password']),
            'user_role_id' => $roleUserId,
            'verified' => 1
        ];

        removeCache(self::getCacheKey());

        return parent::store($properties);
    }

    public static function get($page)
    {
        $key = self::getCacheKey($page);

        return self::withCache(function () {
            return self::$model
                ::with(...self::RELATIONSHIPS_GET)
                ->orderBy('created_at', 'desc')
                ->simplePaginate();
        }, $key);
    }

    public static function delete($email)
    {
        $result = self::getFirstOrFailBy('email', $email, ['id'])->delete();

        removeCache(self::getCacheKey());

        return $result;
    }
}
