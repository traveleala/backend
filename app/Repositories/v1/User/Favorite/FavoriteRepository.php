<?php

namespace App\Repositories\v1\Favorite;

use App\Repositories\v1\BaseRepository;
use App\Models\v1\Favorite\Favorite;
use App\Repositories\v1\Activity\ActivityRepository;

class FavoriteRepository extends BaseRepository
{
    const CACHE_PREFIX = 'activities:favorites';

    public static $model = Favorite::class;

    public static function update($activityId): bool
    {
        $properties = [
            'activity_id' => $activityId
        ];

        $model = self::$model
            ::withTrashed()
            ->firstOrCreate(
                $properties,
                $properties
            );

        $result = true;

        if (!$model->wasRecentlyCreated) {
            if (!$model->trashed()) {
                $model->delete();
                $result = false;
            } else {
                $model->restore();
            }
        }

        removeCache(self::getCacheKey(user()->id));

        return $result;
    }

    public static function areInMyFavorites(array $activityIds)
    {
        $favoritesResult = [];

        if (user()) {
            $favoritesResult = self::$model
                ::whereIn('activity_id', $activityIds)
                ->whereUserId(user()->id)
                ->select('activity_id')
                ->get()
                ->pluck('activity_id')
                ->toArray();
        }

        return $favoritesResult;
    }

    public static function get($page, $total)
    {
        if (!$total) {
            $key = self::getCacheKey(user()->id, $page);
        } else {
            $key = self::getCacheKey(user()->id, 'total');
        }

        return self::withCache(function () use ($total) {
            $favoritesObject = self::$model
                ::select('activity_favorite.activity_id')
                ->with(
                    'activity:id,title,price,location,latitude,longitude',
                    'activity.portrait:activity_id,image_url'
                )
                ->addSelect(ActivityRepository::$model::getQualificationBy('favorite'))
                ->orderBy('created_at', 'desc');

            if (!$total) {
                $favorites = $favoritesObject->simplePaginate();
            } else {
                $favorites = $favoritesObject->count();
            }

            return $favorites;
        }, $key);
    }

    public static function has($activityId): bool
    {
        return self::$model
            ::whereActivityId($activityId)
            ->whereUserId(user()->id)
            ->exists();
    }
}
