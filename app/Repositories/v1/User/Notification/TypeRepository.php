<?php

namespace App\Repositories\v1\Notification;

use App\Repositories\v1\BaseRepository;
use App\Models\v1\Notification\Type;

class TypeRepository extends BaseRepository
{
    const CACHE_PREFIX = 'notifications:types';

    public static $model = Type::class;

    public static function getByName($name)
    {
        $key = self::getCacheKey($name);

        return self::withCacheForever(function () use ($name) {
            return self::getFirstOrFailBy('name', $name);
        }, $key);
    }
}
