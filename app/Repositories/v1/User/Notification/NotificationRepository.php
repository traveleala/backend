<?php

namespace App\Repositories\v1\Notification;

use App\Values\v1\AppValues;
use App\Repositories\v1\BaseRepository;
use App\Models\v1\Notification\Notification;

class NotificationRepository extends BaseRepository
{
    const CACHE_PREFIX = 'notifications';

    public static $model = Notification::class;

    public static function get($page)
    {
        $key = self::getCacheKey(user()->id, $page);

        $notifications = self::withCache(function () {
            return self::$model
                ::userId()
                ->with(
                    'type',
                    'profile:user_id,username,full_name,profile_image_url',
                    'activity:id,title'
                )
                ->orderBy('created_at', 'desc')
                ->simplePaginate();
        }, $key);

        self::readNotifications($page);

        return $notifications;
    }

    public static function hasNews()
    {
        return self::$model::userId()
            ->unread()
            ->exists();
    }

    public static function store(array $properties)
    {
        $properties['notification_type_id'] = TypeRepository::getByName($properties['notification_type'])->id;
        $properties['user_action_id'] = user()->id;

        removeCache(self::getCacheKey($properties['user_id']));

        return parent::store($properties);
    }

    public static function notify($notificationType, $userId, $activityId = null)
    {
        $properties = [
            'notification_type' => $notificationType,
            'user_id' => $userId,
            'activity_id' => $activityId
        ];

        return self::store($properties);
    }

    public static function getUsersDeletedNotifications()
    {
        return self::$model
            ::select('id', 'user_id')
            ->typeIsUserDeleted()
            ->with('user:id,email')
            ->unread();
    }

    public static function setReadNotifications($ids)
    {
        return self::$model
            ::whereIn('id', $ids)
            ->update([
                'read' => true
            ]);
    }

    private static function readNotifications($page = 1)
    {
        if (self::hasNews()) {
            $start = ($page - 1) * AppValues::OFFSET();

            self::$model
                ::whereUserId(user()->id)
                ->unread()
                ->skip($start)
                ->take(AppValues::OFFSET())
                ->update([
                    'read' => true
                ]);

            removeCache(self::getCacheKey(user()->id, $page));
        }
    }
}
