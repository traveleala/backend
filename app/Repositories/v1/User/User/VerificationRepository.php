<?php

namespace App\Repositories\v1\User;

use App\Repositories\v1\BaseRepository;
use App\Traits\v1\SendVerificationEmailTrait;
use Illuminate\Support\Str;
use App\Models\v1\User\Verification;

class VerificationRepository extends BaseRepository
{
    use SendVerificationEmailTrait;

    public static $model = Verification::class;

    public static function create($userId): Verification
    {
        $properties = [
            'token' => Str::random(64),
            'user_id' => $userId
        ];
        $model = parent::store($properties);

        return $model;
    }

    public static function update($token)
    {
        $userVerification = self::$model::whereToken($token)->firstOrFail();
        $userVerification->delete();

        $user = UserRepository::verified($userVerification->user_id);

        return $user;
    }

    public static function store(array $properties)
    {
        $user = UserRepository::getFirstOrFailBy('email', $properties['email'], ['id']);
        $user->load('verification');

        if (!$user->verification) {
            $token = self::create($user->id)->token;
        } elseif ($user->verification->tries <= 0) {
            $user->verification->delete();
            $userVerification = self::create($user->id);
            $token = $userVerification->token;
        } else {
            $user->verification->tries -= 1;
            $user->verification->save();
            $token = $user->verification->token;
        }

        self::send($user, $token);

        return $user;
    }
}
