<?php

namespace App\Repositories\v1\User;

use App\Repositories\v1\BaseRepository;
use App\Models\v1\User\Follow;
use App\Repositories\v1\Notification\NotificationRepository;
use App\Values\v1\NotificationTypeValues;

class FollowRepository extends BaseRepository
{
    const CACHE_PREFIX = 'followers';
    const CACHE_FOLLOWINGS_PREFIX = 'followings';

    public static $model = Follow::class;

    public static function show($username)
    {
        $userProfileToFollow = ProfileRepository::getFirstOrFailBy('username', $username, ['id']);

        return self::$model
            ::whereUserFollowerId(user()->id)
            ->whereUserFollowingId($userProfileToFollow->id)
            ->exists();
    }

    public static function getFollowers($page = 1, $total)
    {
        if (!$total) {
            $key = self::getCacheKey(user()->id, $page);
        } else {
            $key = self::getCacheKey(user()->id, 'total');
        }

        return self::withCache(function () use ($total) {
            $followersObject = self::$model
                ::select('user_follower_id')
                ->whereUserFollowingId(user()->id)
                ->with('follower:user_id,username,profile_image_url,full_name')
                ->orderBy('created_at', 'desc');

            if (!$total) {
                $followers = $followersObject->simplePaginate();
            } else {
                $followers = $followersObject->count();
            }

            return $followers;
        }, $key);
    }

    public static function getFollowings($page = 1, $total)
    {
        if (!$total) {
            $key = keyCache(self::CACHE_FOLLOWINGS_PREFIX, user()->id, $page);
        } else {
            $key = keyCache(self::CACHE_FOLLOWINGS_PREFIX, user()->id, 'total');
        }

        return self::withCache(function () use ($total) {
            $followingsObject = self::$model
                ::select('user_following_id')
                ->whereUserFollowerId(user()->id)
                ->with('following:user_id,username,profile_image_url,full_name')
                ->orderBy('created_at', 'desc');

            if (!$total) {
                $followings = $followingsObject->simplePaginate();
            } else {
                $followings = $followingsObject->count();
            }

            return $followings;
        }, $key);
    }

    public static function canFollow($username)
    {
        return ProfileRepository::$model
            ::select('id')
            ->whereUsername($username)
            ->where('user_id', '!=', user()->id)
            ->firstOrFail();
    }

    public static function areInMyFollowings(array $userIds = []): array
    {
        if ($userIds) {
            $userIds = self::$model
                ::whereUserFollowerId(user()->id)
                ->whereIn('user_following_id', $userIds)
                ->select('user_following_id')
                ->get()
                ->pluck('user_following_id')
                ->toArray();
        }

        return $userIds;
    }

    public static function update($username)
    {
        $userProfileToFollow = ProfileRepository::getFirstOrFailBy('username', $username, ['id']);

        $properties = [
            'user_follower_id' => user()->id,
            'user_following_id' => $userProfileToFollow->id
        ];

        $model = self::$model
            ::withTrashed()
            ->firstOrCreate(
                $properties,
                $properties
            );

        $result = true;

        if (!$model->wasRecentlyCreated) {
            if (!$model->trashed()) {
                $model->delete();
                $result = false;
            } else {
                $model->restore();
            }
        }

        self::notifyToFollower($userProfileToFollow->id);

        $key = keyCache(self::CACHE_FOLLOWINGS_PREFIX, user()->id);
        removeCache($key);

        return $result;
    }

    private static function notifyToFollower($userId)
    {
        if (!self::wasAFollower($userId)) {
            NotificationRepository::notify(
                NotificationTypeValues::FOLLOW_USER,
                $userId
            );
        }
    }

    private static function wasAFollower($userId)
    {
        return self::$model
            ::onlyTrashed()
            ->whereUserFollowerId(user()->id)
            ->whereUserFollowingId($userId)
            ->exists();
    }
}
