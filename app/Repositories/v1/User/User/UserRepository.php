<?php

namespace App\Repositories\v1\User;

use App\Repositories\v1\BaseRepository;
use DB;
use App\Values\v1\DefaultImageValues;
use App\Traits\v1\SendVerificationEmailTrait;
use Throwable;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use App\Models\v1\User\User;

class UserRepository extends BaseRepository
{
    use SendVerificationEmailTrait;

    const CACHE_PREFIX = 'users';

    public static $model = User::class;

    public static function verified($userId)
    {
        $user = self::getFirstOrFailBy('id', $userId, ['id']);
        $user->verified = true;
        $user->save();

        return $user;
    }

    public static function changePassword($userId, $password): void
    {
        $user = self::getFirstOrFailBy('id', $userId, ['id']);
        $user->password = password($password);
        $user->reset_restricted_at = now();
        $user->save();

        self::removeCacheById($user->id);
    }

    public static function update(array $properties): bool
    {
        self::removeCacheById(user()->id);

        user()->password = password($properties['new_password']);
        $result = user()->save();

        return $result;
    }

    public static function delete(): void
    {
        self::removeCacheById(user()->id);
        user()->delete();
    }

    public static function getByIdWithRelations($id)
    {
        $key = self::getCacheKey($id);

        return self::withCacheHourly(function () use ($id) {
            return self::getFirstOrFailBy('id', $id)
                ->load(
                    'role',
                    'profile'
                );
        }, $key);
    }

    public static function socialAuth($id, $email, $provider)
    {
        $column = $provider == 'Google' ? 'google_id' : 'facebook_id';
        $useDatabase = false;

        $user = self::$model
            ::select('id', 'email', 'google_id', 'facebook_id', 'deleted_at')
            ->withTrashed()
            ->whereEmail($email)
            ->orWhere($column, $id)
            ->first();

        $user = $user ?? self::instance([]);

        if ($user->trashed()) {
            throw new UnauthorizedException;
        }

        if (!$user->exists) {
            $user->email = $email;
            $user->user_role_id = RoleRepository::getUserRole()->id;
            $useDatabase = true;

            $images = self::defaultImages();
            $username = ProfileRepository::generateUsername();
            $fullName = ProfileRepository::generateFullname($username);

            $profileInstance = ProfileRepository::instance([
                'portrait_url' => $images['portrait'],
                'profile_image_url' => $images['profile'],
                'username' => $username,
                'full_name' => $fullName
            ]);
        }

        if (!$user->$column) {
            $user->$column = $id;
            $user->verified = true;
            $useDatabase = true;
        }

        if ($useDatabase) {
            $user->save();

            if (isset($profileInstance)) {
                $user->profile()->save($profileInstance);
            }
        }

        return $user;
    }

    public static function hasBeenVerified($email)
    {
        return self::$model
            ::whereEmail($email)
            ->whereVerified(true)
            ->exists();
    }

    public static function store(array $properties): User
    {
        try {
            DB::beginTransaction();

            $roleUserId = RoleRepository::getUserRole()->id;

            $userProperties = [
                'email' => $properties['email'],
                'password' => password($properties['password']),
                'user_role_id' => $roleUserId
            ];

            $images = self::defaultImages();
            $fullName = ProfileRepository::generateFullname($properties['username']);

            $profileInstance = ProfileRepository::instance([
                'username' => $properties['username'],
                'full_name' => $fullName,
                'change_restricted_at' => now(),
                'portrait_url' => $images['portrait'],
                'profile_image_url' => $images['profile'],
            ]);

            $user = parent::store($userProperties);
            $user->profile()->save($profileInstance);

            $userVerification = VerificationRepository::create($user->id);

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();

            throw new UnprocessableEntityHttpException;
        }

        self::send($user, $userVerification->token);

        return $user;
    }

    private static function removeCacheById($id)
    {
        removeCache(self::getCacheKey($id));
    }

    private static function defaultImages()
    {
        $randPortrait = rand(0, 9);
        $randProfile = rand(0, 9);

        $url = conf('filesystems.disks.s3.url') . DIRECTORY_SEPARATOR . conf('filesystems.disks.s3.folder_default') . DIRECTORY_SEPARATOR;

        $portraitFolder = conf('filesystems.disks.s3.folder_portrait');
        $profileFolder = conf('filesystems.disks.s3.folder_profile');

        $portraitUrl = $url . $portraitFolder . DIRECTORY_SEPARATOR . DefaultImageValues::DEFAULTS[$randPortrait];
        $profileImageUrl = $url . $profileFolder . DIRECTORY_SEPARATOR . DefaultImageValues::DEFAULTS[$randProfile];

        return [
            'portrait' => $portraitUrl,
            'profile' => $profileImageUrl
        ];
    }
}
