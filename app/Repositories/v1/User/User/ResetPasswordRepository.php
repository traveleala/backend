<?php

namespace App\Repositories\v1\User;

use App\Repositories\v1\BaseRepository;
use App\Models\v1\User\ResetPassword;
use App\Events\v1\User\ResetPasswordEvent;
use App\Values\v1\EmailSubjectValues;
use Illuminate\Support\Str;

class ResetPasswordRepository extends BaseRepository
{
    public static $model = ResetPassword::class;

    private static function create($userId): ResetPassword
    {
        return parent::store([
            'token' => Str::random(64),
            'user_id' => $userId
        ]);
    }

    public static function update(array $properties)
    {
        $userResetPassword = self::getFirstOrFailBy('token', $properties['token'], ['id', 'user_id']);
        $user = UserRepository::changePassword($userResetPassword->user_id, $properties['password']);
        $userResetPassword->delete();

        return $user;
    }

    public static function store(array $properties)
    {
        $user = UserRepository::getFirstOrFailBy('email', $properties['email']);
        $user->load('resetPassword');

        if (!$user->resetPassword) {
            $userResetPassword = self::create($user->id);
            $token = $userResetPassword->token;
        } else {
            if ($user->resetPassword->tries <= 0) {
                $user->resetPassword->delete();
                $userResetPassword = self::create($user->id);
                $token = $userResetPassword->token;
            } else {
                $user->resetPassword->tries -= 1;
                $user->resetPassword->save();
                $token = $user->resetPassword->token;
            }
        }

        self::dispatchEvent($token, $user);

        return $user;
    }

    private static function dispatchEvent($token, object $user)
    {
        $dataEmail = [
            'nameFrom' =>  conf('services.sendgrid.name_from'),
            'emailFrom' => conf('services.sendgrid.mail_from'),
            'subject' => EmailSubjectValues::RESET_PASSWORD_SUBJECT,
            'token' => $token
        ];

        event(new ResetPasswordEvent($user, $dataEmail));
    }
}
