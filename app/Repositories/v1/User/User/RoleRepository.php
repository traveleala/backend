<?php

namespace App\Repositories\v1\User;

use App\Repositories\v1\BaseRepository;
use App\Models\v1\User\Role;
use App\Values\v1\UserRoleValues;

class RoleRepository extends BaseRepository
{
    const CACHE_PREFIX = 'roles';

    public static $model = Role::class;

    public static function getUserRole()
    {
        $name = UserRoleValues::USER_ROLE;
        return self::getByName($name);
    }

    public static function getPublisherRole()
    {
        $name = UserRoleValues::PUBLISHER_ROLE;
        return self::getByName($name);
    }

    public static function getAdminRole()
    {
        $name = UserRoleValues::ADMIN_ROLE;
        return self::getByName($name);
    }

    public static function getByName($name)
    {
        $key = self::getCacheKey($name);

        return self::withCacheForever(function () use ($name) {
            return self::getFirstOrFailBy('name', $name);
        }, $key);
    }

    public static function all()
    {
        return self::withCacheForever(function () {
            return parent::all();
        });
    }
}
