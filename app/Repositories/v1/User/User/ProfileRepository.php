<?php

namespace App\Repositories\v1\User;

use App\Repositories\v1\Activity\ActivityRepository;
use App\Repositories\v1\BaseRepository;
use App\Services\v1\FileService;
use Uptodown\RandomUsernameGenerator\Generator;
use App\Models\v1\User\Profile;
use App\Values\v1\UploadTypeValues;

class ProfileRepository extends BaseRepository
{
    const CACHE_PREFIX = 'profiles';

    public static $model = Profile::class;

    public static function update(array $properties)
    {
        $propertiesToUpdate = [
            'full_name' => $properties['full_name']
        ];

        if (user()->profile->username != $properties['username']) {
            $propertiesToUpdate['username'] = $properties['username'];
            $propertiesToUpdate['change_restricted_at'] = now();
        }

        if (@$properties['profile_image']) {
            $fileService = new FileService(UploadTypeValues::PROFILE_TYPE);
            $propertiesToUpdate['profile_image_url'] = $fileService->upload($properties['profile_image']);
        }

        if (@$properties['portrait_image']) {
            $fileService = new FileService(UploadTypeValues::PORTRAIT_TYPE);
            $propertiesToUpdate['portrait_url'] = $fileService->upload($properties['portrait_image']);
        }

        $result = user()->profile->update($propertiesToUpdate);

        removeCache([
            keyCache(UserRepository::CACHE_PREFIX, user()->id),
            self::getCacheKey(user()->profile->username)
        ]);

        return $result;
    }

    public static function getByUsername($username)
    {
        $key = self::getCacheKey($username);

        return self::withCache(function () use ($username) {
            return self::$model
                ::whereUsername($username)
                ->where('full_name', '!=', null)
                ->withCount([
                    'activitiesAvailable',
                    'qualifications',
                    'followers',
                    'followings'
                ])
                ->addSelect(UserRepository::$model::getQualificationUserByUsername($username))
                ->firstOrFail();
        }, $key);
    }

    public static function byActivity($activityId)
    {
        $activity = ActivityRepository::getFirstOrFailBy('id', $activityId, ['user_id', 'id']);

        $profile = self::$model
            ::whereUserId($activity->user_id)
            ->addSelect(UserRepository::$model::getQualificationUserByActivityId($activity->id))
            ->firstOrFail();

        return $profile;
    }

    public static function existsUsername($username)
    {
        return self::$model
            ::whereUsername($username)
            ->exists();
    }

    public static function generateUsername()
    {
        $generator = new Generator();

        do {
            $username = strtolower(substr($generator->makeNew(), 0, 20));
            $result = ProfileRepository::existsUsername($username);
        } while ($result);

        return $username;
    }

    public static function generateFullname($username)
    {
        return preg_replace('/[0-9.-]+/', '', $username);
    }
}
