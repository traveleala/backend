<?php

namespace App\Repositories\v1\Report;

use App\Repositories\v1\BaseRepository;
use App\Values\v1\ReportStateValues;
use App\Models\v1\Report\State;

class StateRepository extends BaseRepository
{
    const CACHE_PREFIX = 'reports:states';

    public static $model = State::class;

    public static function getStatePending()
    {
        return self::getByName(ReportStateValues::PENDING_STATE);
    }

    private static function getByName($name)
    {
        $key = self::getCacheKey($name);

        return self::withCacheForever(function () use ($name) {
            return self::getFirstOrFailBy('name', $name);
        }, $key);
    }
}
