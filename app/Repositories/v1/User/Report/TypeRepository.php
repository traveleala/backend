<?php

namespace App\Repositories\v1\Report;

use App\Repositories\v1\BaseRepository;
use App\Models\v1\Report\Type;

class TypeRepository extends BaseRepository
{
    const CACHE_PREFIX = 'reports:types';

    public static $model = Type::class;

    public static function all()
    {
        return self::withCacheForever(function () {
            return parent::all();
        });
    }

    public static function getByName($name)
    {
        $key = self::getCacheKey($name);

        return self::withCacheForever(function () use ($name) {
            return self::getFirstOrFailBy('name', $name);
        }, $key);
    }
}
