<?php

namespace App\Repositories\v1\Report;

use App\Repositories\v1\BaseRepository;
use App\Models\v1\Report\Report;

class ReportRepository extends BaseRepository
{
    public static $model = Report::class;

    public static function store(array $properties)
    {
        $properties['report_type_id'] = TypeRepository::getByName($properties['report_type'])->id;
        $properties['report_state_id'] = StateRepository::getStatePending()->id;

        return parent::store($properties);
    }

    public static function hasBeenReported($type, $id)
    {
        $reportStateId = StateRepository::getStatePending()->id;

        return self::$model
            ::where($type, $id)
            ->whereReportStateId($reportStateId)
            ->whereUserId(user()->id)
            ->exists();
    }
}
