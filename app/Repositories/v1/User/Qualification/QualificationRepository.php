<?php

namespace App\Repositories\v1\Qualification;

use App\Repositories\v1\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Models\v1\Qualification\Qualification;
use App\Repositories\v1\Activity\ActivityRepository;
use App\Repositories\v1\Activity\DifficultyRepository;
use App\Repositories\v1\Activity\StateRepository;
use App\Repositories\v1\Notification\NotificationRepository;
use App\Values\v1\NotificationTypeValues;
use Throwable;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use App\Repositories\v1\User\UserRepository;

class QualificationRepository extends BaseRepository
{
    const CACHE_PREFIX = 'qualifications';
    const CACHE_QUALIFICATION_PREFIX = 'qualification';
    const RELATIONSHIPS_GET = [
        'user:id',
        'user.profile:full_name,user_id,username,profile_image_url',
        'images:image_url,activity_qualification_id',
        'difficulty'
    ];

    public static $model = Qualification::class;

    public static function getByActivity($activityId, $page, $total)
    {
        if (!$total) {
            $key = self::getCacheKey($activityId, $page);
        } else {
            $key = self::getCacheKey($activityId, 'total');
        }

        return self::withCache(function () use ($activityId, $total) {
            $qualificationsObject = self::$model
                ::whereActivityId($activityId)
                ->with(...self::RELATIONSHIPS_GET)
                ->withCount('votes')
                ->orderBy('created_at', 'desc');

            if ($total) {
                $qualifications = $qualificationsObject->count();
            } else {
                $qualifications = $qualificationsObject->simplePaginate();
            }

            return $qualifications;
        }, $key);
    }

    public static function canReport($qualificationId)
    {
        return self::$model
            ::select('id')
            ->whereId($qualificationId)
            ->where('user_id', '!=', user()->id)
            ->firstOrFail();
    }

    public static function canQualify($activityId)
    {
        $stateId = StateRepository::getAvailable()->id;

        return self::$model
            ::join('activity', function ($join) use ($activityId, $stateId) {
                $join->on('activity.id', 'activity_qualification.activity_id')
                    ->where('activity.user_id', '!=', user()->id)
                    ->where('activity.id', $activityId)
                    ->where('activity.activity_state_id', $stateId);
            })
            ->where('activity_qualification.user_id', user()->id)
            ->whereRaw('DATE_ADD(activity_qualification.created_at, INTERVAL 30 DAY) > now()')
            ->exists();
    }

    public static function getQuantityTotal($activityId = null)
    {
        $activityId = intval($activityId);
        $key = self::getCacheKey('stats', 'total', user()->id, $activityId);

        return self::withCache(function () use ($activityId) {
            return self::$model
                ::join('activity', function ($join) use ($activityId) {
                    $condition = $activityId ? '=' : '!=';

                    $join->on('activity.id', '=', 'activity_qualification.activity_id')
                        ->where('activity.id', $condition, $activityId);
                })
                ->where('activity.user_id', user()->id)
                ->count();
        }, $key);
    }

    public static function getQuantityPerMonth($year, $activityId = null)
    {
        $activityId = intval($activityId);

        $key = self::getCacheKey('stats', user()->id, 'year', $year, $activityId);

        return self::withCache(function () use ($year, $activityId) {
            return self::$model
                ::select(
                    DB::raw('count(activity_qualification.id) total'),
                    DB::raw('MONTH(activity_qualification.created_at) month')
                )
                ->leftJoin('activity', function ($join) use ($activityId) {
                    $condition = $activityId ? '=' : '!=';

                    $join->on('activity.id', '=', 'activity_qualification.activity_id')
                        ->where('activity.id', $condition, $activityId);
                })
                ->whereRaw("YEAR(activity_qualification.created_at) = {$year}")
                ->where('activity.user_id', user()->id)
                ->groupByRaw('month(activity_qualification.created_at)')
                ->get();
        }, $key);
    }

    public static function getDifficultiesByActivity($activityId)
    {
        $key = self::getCacheKey($activityId, "difficulties");

        return self::withCache(function () use ($activityId) {
            return self::$model
                ::select(
                    DB::raw('count(activity_difficulty_id) as total'),
                    'activity_difficulty.name'
                )
                ->leftJoin('activity_difficulty', 'activity_difficulty.id', 'activity_qualification.activity_difficulty_id')
                ->whereActivityId($activityId)
                ->groupBy('activity_difficulty_id')
                ->get();
        }, $key);
    }

    public static function getStarsByActivity($activityId)
    {
        $key = self::getCacheKey($activityId, "stars");

        return self::withCache(function () use ($activityId) {
            return self::$model
                ::select(
                    DB::raw('count(qualification) as total'),
                    'qualification'
                )
                ->whereActivityId($activityId)
                ->groupBy('activity_difficulty_id')
                ->orderBy('qualification', 'desc')
                ->get();
        }, $key);
    }

    public static function show($qualificationId)
    {
        $qualification =  self::$model
            ::whereId($qualificationId)
            ->with(...self::RELATIONSHIPS_GET)
            ->withCount('votes')
            ->orderBy('created_at', 'desc')
            ->firstOrFail();

        return $qualification;
    }

    public static function store(array $properties): bool
    {
        $properties = collect($properties);
        $qualificationProperties = $properties->only(
            'review',
            'qualification',
            'activity_id',
            'activity_difficulty',
            'activity_date'
        )->all();

        $qualificationImagesProperties = array_filter($properties->only('images')->all());

        $qualificationProperties['activity_difficulty_id'] = DifficultyRepository::getByName($qualificationProperties['activity_difficulty'])->id;

        if ($qualificationImagesProperties) {
            $qualificationImages = ImageRepository::uploadQualificationImages($qualificationImagesProperties);
        }

        try {
            DB::beginTransaction();

            $qualification = parent::store($qualificationProperties);

            if ($qualificationImagesProperties) {
                $qualification->images()->saveMany($qualificationImages);
            }

            DB::commit();

            $activity = ActivityRepository::getFirstOrFailBy('id', $properties['activity_id'], ['user_id', 'title', 'id']);

            NotificationRepository::notify(
                NotificationTypeValues::QUALIFICATION_IN_ACTIVITY,
                $activity->user_id,
                $activity->id
            );

            removeCache([
                self::getCacheKey($qualificationProperties['activity_id'])
            ]);

            return true;
        } catch (Throwable $th) {
            DB::rollBack();

            if ($qualificationImagesProperties) {
                ImageRepository::rollbackQualificationImages($qualificationImages);
            }

            throw new UnprocessableEntityHttpException;
        }
    }

    public static function getQualificationPerMonth($year, $activityId = null)
    {
        $activityId = intval($activityId);

        $key = keyCache(self::CACHE_QUALIFICATION_PREFIX, 'stats', user()->id, 'year', $year, $activityId);

        return self::withCache(function () use ($year, $activityId) {
            return self::$model
                ::select(
                    DB::raw('REPLACE(FORMAT(sum(activity_qualification.qualification) / count(activity_qualification.id),1),".",",") total'),
                    DB::raw('MONTH (activity_qualification.created_at ) month')
                )
                ->leftJoin('activity', function ($join) use ($activityId) {
                    $condition = $activityId ? '=' : '!=';

                    $join->on('activity.id', '=', 'activity_qualification.activity_id')
                        ->where('activity.id', $condition, $activityId);
                })
                ->whereRaw("YEAR(activity_qualification.created_at) = {$year}")
                ->where('activity.user_id', user()->id)
                ->groupByRaw('month(activity_qualification.created_at)')
                ->get();
        }, $key);
    }

    public static function getQualificationTotal()
    {
        $key = keyCache(self::CACHE_QUALIFICATION_PREFIX, "stats", 'total', user()->id);

        return self::withCache(function () {
            return UserRepository::$model
                ::getQualificationUserByUserId(user()->id)['user_qualification']
                ->first()
                ->qualification;
        }, $key);
    }

    public static function getQualificationTotalByActivity($activityId = null)
    {
        $activityId = intval($activityId);
        $key = keyCache(self::CACHE_QUALIFICATION_PREFIX, "stats", 'total', user()->id, $activityId);

        return self::withCache(function () use ($activityId) {
            $result = @ActivityRepository::$model
                ::getQualificationBy('activity_id', $activityId)['qualification']
                ->first();

            return $result ? $result->qualification : 0;
        }, $key);
    }
}
