<?php

namespace App\Repositories\v1\Qualification;

use App\Repositories\v1\BaseRepository;
use App\Models\v1\Qualification\Vote;

class VoteRepository extends BaseRepository
{
    public static $model = Vote::class;

    public static function update($qualificationId)
    {
        $properties = [
            'user_id' => user()->id,
            'activity_qualification_id' => $qualificationId
        ];

        $model = self::$model
            ::withTrashed()
            ->firstOrCreate(
                $properties,
                $properties
            );

        $result = true;

        if (!$model->wasRecentlyCreated) {
            if (!$model->trashed()) {
                $model->delete();
                $result = false;
            } else {
                $model->restore();
            }
        }

        $activityId = QualificationRepository::getFirstOrFailBy('id', $qualificationId, ['activity_id'])->activity_id;

        $key = keyCache(QualificationRepository::CACHE_PREFIX, $activityId);
        removeCache($key);

        return $result;
    }

    public static function getTotalVotes($qualificationId)
    {
        return self::$model
            ::whereActivityQualificationId($qualificationId)
            ->count();
    }

    public static function areInMyVotes(array $qualificationIds)
    {
        $ids = [];

        if (user()) {
            $ids = self::$model
                ::whereIn('activity_qualification_id', $qualificationIds)
                ->where('user_id', user()->id)
                ->select('activity_qualification_id')
                ->get()
                ->pluck('activity_qualification_id')
                ->toArray();
        }

        return $ids;
    }
}
