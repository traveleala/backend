<?php

namespace App\Repositories\v1\Qualification;

use App\Repositories\v1\BaseRepository;
use App\Traits\v1\HandleImageModelTrait;
use App\Models\v1\Qualification\Image;

class ImageRepository extends BaseRepository
{
    use HandleImageModelTrait;

    public static $model = Image::class;
}
