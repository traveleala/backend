<?php

namespace App\Repositories\v1\Location;

use App\Models\v1\Activity\Activity;
use App\Repositories\v1\BaseRepository;

class LocationRepository extends BaseRepository
{
    const CACHE_PREFIX = 'activities';

    public static $model = Activity::class;

    public static function getRandomLocations()
    {
        $key = self::getCacheKey('random');

        return self::withCacheWeekly(function () {
            return self::$model
                ::select('activity.location', 'activity_image.image_url')
                ->leftJoin('activity_image', 'activity_image.activity_id', 'activity.id')
                ->whereIsLandscape(true)
                ->groupBy('location')
                ->inRandomOrder()
                ->limit(20)
                ->get();
        }, $key);
    }

    public static function getLocations()
    {
        return self::$model
            ::select('location as customLocation')
            ->groupBy('customLocation')
            ->orderBy('location', 'desc');
    }

    public static function getProvincesLocations()
    {
        return self::$model
            ::selectRaw("substring_index(location, ', ',-1) as customLocation")
            ->groupBy('customLocation')
            ->orderBy('location', 'desc');
    }
}
