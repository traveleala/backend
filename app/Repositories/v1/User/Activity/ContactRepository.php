<?php

namespace App\Repositories\v1\Activity;

use App\Repositories\v1\BaseRepository;
use App\Models\v1\Activity\Contact;

class ContactRepository extends BaseRepository
{
    public static $model = Contact::class;
}
