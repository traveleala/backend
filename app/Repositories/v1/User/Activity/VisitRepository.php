<?php

namespace App\Repositories\v1\Activity;

use App\Repositories\v1\BaseRepository;
use App\Classes\v1\Cache;
use App\Services\v1\GoogleAnalyticsService;

class VisitRepository extends BaseRepository
{
    const CACHE_PREFIX = 'visits:stats';

    public static function getQuantityPerMonth($id, $activityId, $year)
    {
        $key = self::getCacheKey($id, 'year', $year);

        return Cache::rememberDefault(function () use ($activityId, $year) {
            $parameters = [
                'metrics' => [
                    'ga:pageviews'
                ],
                'start-date' => "{$year}-01-01",
                'end-date' => now()->format('Y-m-d'),
                'filters' => [
                    "ga:pagePath==/activities/{$activityId}"
                ],
                'dimensions' => [
                    'ga:month'
                ]
            ];

            $response = self::getGoogleAnalyticsService()->doQuery($parameters);
            $rows = $response->getRows();
            $data = [];

            foreach ($rows as $row) {
                $data[] = [
                    'month' => intval($row[0]),
                    'total' => $row[1]
                ];
            }

            return $data;
        }, $key);
    }

    public static function getQuantityTotal($id, $activityId, $createdAt)
    {
        $key = self::getCacheKey("total", $id);

        return Cache::rememberDefault(function () use ($activityId, $createdAt) {
            $parameters = [
                'metrics' => [
                    'ga:pageviews'
                ],
                'start-date' => $createdAt,
                'end-date' => now()->format('Y-m-d'),
                'filters' => [
                    "ga:pagePath==/activities/{$activityId}"
                ]
            ];

            $response = self::getGoogleAnalyticsService()->doQuery($parameters);
            $total = $response->getTotalsForAllResults()['ga:pageviews'];

            return $total;
        }, $key);
    }

    private static function getGoogleAnalyticsService()
    {
        return new GoogleAnalyticsService;
    }
}
