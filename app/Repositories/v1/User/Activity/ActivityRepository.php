<?php

namespace App\Repositories\v1\Activity;

use App\Models\v1\Activity\Activity;
use App\Repositories\v1\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Repositories\v1\Qualification\QualificationRepository;
use App\Repositories\v1\User\ProfileRepository;
use App\Services\v1\ReverseGeocodingService;
use Throwable;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class ActivityRepository extends BaseRepository
{
    const CACHE_PREFIX = 'activities';
    const LIMIT_RELATEDS = 4;
    const LIMIT_BEST = 12;
    const QUANTITY_VOTES_TOP = 0;
    const DEFAULT_ORDER_BY = 'activity.created_at';
    const RELATIONSHIPS_THUMBS = [
        'portrait:activity_id,image_url',
    ];

    public static $model = Activity::class;

    public static function show($id)
    {
        $activity = self::$model
            ::whereId($id)
            ->available()
            ->addSelect(self::$model::getQualificationBy('activity'))
            ->with(
                'contact:activity_id,phone_number,telephone_number,facebook_url,instagram_url,email,web',
                'images:activity_id,image_url',
                'user:id',
                'user.profile:user_id,username',
                'characteristics:activity_id,concept,value',
                'type',
                'difficulty'
            )
            ->firstOrFail();

        return $activity;
    }

    public static function showMyActivity($id)
    {
        $activity = self::$model
            ::whereId($id)
            ->whereUserId(user()->id)
            ->with(
                'contact:activity_id,phone_number,telephone_number,facebook_url,instagram_url,email,web',
                'images:activity_id,image_url',
                'user:id',
                'user.profile:user_id,username',
                'characteristics:activity_id,concept,value',
                'type',
                'difficulty'
            )
            ->firstOrFail();

        return $activity;
    }

    public static function relateds($activityId)
    {
        $key = self::getCacheKey('relateds', $activityId);

        return self::withCache(function () use ($activityId) {

            $activity = self::getFirstOrFailBy('id', $activityId, ['location', 'id']);

            return self::$model
                ::available()
                ->location($activity->location)
                ->where('activity.id', '!=', $activity->id)
                ->select('activity.id', 'title', 'price', 'location', 'latitude', 'longitude')
                ->addSelect(self::$model::getQualificationBy('activity'))
                ->with(...self::RELATIONSHIPS_THUMBS)
                ->inRandomOrder()
                ->limit(self::LIMIT_RELATEDS)
                ->get();
        }, $key);
    }

    public static function recents()
    {
        $key = self::getCacheKey('recents');

        return self::withCache(function () {

            return self::$model
                ::available()
                ->select('activity.id', 'title', 'price', 'location', 'latitude', 'longitude')
                ->addSelect(self::$model::getQualificationBy('activity'))
                ->leftJoin('activity_subscription', 'activity_subscription.activity_id', '=', 'activity.id')
                ->with(...self::RELATIONSHIPS_THUMBS)
                ->inRandomOrder()
                ->limit(self::LIMIT_BEST)
                ->orderBy('activity.created_at', 'desc')
                ->orderBy('activity_subscription.activity_publish_type_id', 'desc')
                ->get();
        }, $key);
    }

    public static function canReport($activityId)
    {
        return self::$model
            ::select('id')
            ->whereId($activityId)
            ->available()
            ->where('user_id', '!=', user()->id)
            ->firstOrFail();
    }

    public static function search(array $properties, $page, $total)
    {
        $types = [];
        $difficulties = [];
        $orderBy = $properties['orderBy'];
        $order = 'desc';
        $orderColumns = [
            'created_at' => 'activity.created_at',
            'price' => 'activity.price',
            'qualification' => 'qualification'
        ];

        if (!@$orderColumns[$orderBy]) {
            $orderBy = self::DEFAULT_ORDER_BY;
        }

        if ($properties['order'] != 'desc') {
            $order = 'asc';
        }

        if ($properties['types']) {
            $types = TypeRepository::getByNames($properties['types'])->pluck('id')->all();
        }

        if ($properties['difficulties']) {
            $difficulties = DifficultyRepository::getByNames($properties['difficulties'])->pluck('id')->all();
        }

        $activitiesObject = self::$model
            ::available()
            ->location($properties['location'])
            ->title($properties['title'])
            ->price($properties['minPrice'], $properties['maxPrice'])
            ->distance($properties['distance'], $properties['coordinates'])
            ->landscape($properties['landscape'])
            ->types($types)
            ->difficulties($difficulties)
            ->select('activity.id', 'title', 'price', 'location', 'latitude', 'longitude')
            ->addSelect(self::$model::getQualificationBy('activity'))
            ->with(...self::RELATIONSHIPS_THUMBS)
            ->orderBy('activity.activity_publish_type_id', 'desc')
            ->orderBy($orderBy, $order)
            ->distinct('activity.id');

        if ($total) {
            $activities = $activitiesObject->count();
        } else {
            $activities = $activitiesObject->simplePaginate();
        }

        return $activities;
    }

    public static function canAddToFavorite($id)
    {
        return self::$model
            ::select('id')
            ->whereId($id)
            ->available()
            ->firstOrFail();
    }

    public static function top()
    {
        $key = self::getCacheKey('top');

        return self::withCacheWeekly(function () {
            return QualificationRepository::$model
                ::select(
                    'activity_qualification.activity_id',
                    DB::raw('REPLACE(FORMAT(sum(activity_qualification.qualification) / count(activity_qualification.id), 1),".",",") as qualification')
                )
                ->leftJoin('activity_subscription', 'activity_subscription.activity_id', '=', 'activity_qualification.activity_id')
                ->orderBy('qualification', 'desc')
                ->orderBy('activity_subscription.activity_publish_type_id', 'desc')
                ->groupBy('activity_qualification.activity_id')
                ->havingRaw('count(activity_qualification.activity_id) > ' . self::QUANTITY_VOTES_TOP)
                ->with(
                    'activity:id,user_id,location,price,title',
                    'activity.portrait:activity_id,image_url',
                )
                ->limit(self::LIMIT_BEST)
                ->get();
        }, $key);
    }

    public static function removeTopActivities()
    {
        removeCache(self::getCacheKey('top'));
    }

    public static function getQuantityPerMonth($year)
    {
        $key = self::getCacheKey('stats', user()->id, 'year', $year);

        return self::withCache(function () use ($year) {
            return self::$model
                ::select(
                    DB::raw('count(id) total'),
                    DB::raw('MONTH(created_at) month')
                )
                ->whereRaw("YEAR(created_at) = {$year}")
                ->whereUserId(user()->id)
                ->groupByRaw('month(created_at)')
                ->get();
        }, $key);
    }

    public static function byUsername($username, $page)
    {
        $profile = ProfileRepository::getFirstOrFailBy('username', $username, ['user_id']);

        $key = self::getCacheKey("username", $username, $page);

        return self::withCache(function () use ($profile) {
            return self::$model
                ::available()
                ->whereUserId($profile->user_id)
                ->select('id', 'title', 'price', 'location', 'latitude', 'longitude')
                ->addSelect(self::$model::getQualificationBy('activity'))
                ->orderBy('created_at', 'desc')
                ->with(...self::RELATIONSHIPS_THUMBS)
                ->simplePaginate();
        }, $key);
    }

    public static function getQuantityTotal()
    {
        $key = self::getCacheKey('total', user()->id);

        return self::withCache(function () {
            return self::$model
                ::whereUserId(user()->id)
                ->count();
        }, $key);
    }

    public static function myActivities($page = 1, $total)
    {
        if (!$total) {
            $key = self::getCacheKey('me', user()->id, $page);
        } else {
            $key = self::getCacheKey('me', user()->id, 'total');
        }

        return self::withCache(function () use ($total) {
            $activitiesObject = self::$model
                ::whereUserId(user()->id)
                ->select('id', 'title', 'price', 'location', 'activity_state_id', 'latitude', 'longitude')
                ->addSelect(self::$model::getQualificationBy('activity'))
                ->with(
                    'portrait:activity_id,image_url',
                    'state',
                    'subscription:activity_id,amount,paid_out,expire_at,activity_publish_type_id',
                    'subscription.publishType:id,name'
                )
                ->orderBy('activity.created_at', 'desc');

            if (!$total) {
                $activities = $activitiesObject->simplePaginate();
            } else {
                $activities = $activitiesObject->count();
            }

            return $activities;
        }, $key);
    }

    public static function delete($activity): bool
    {
        $activity = self::$model
            ::select('id')
            ->whereId($activity)
            ->whereUserId(user()->id)
            ->firstOrFail();

        self::removeCacheOfActivityUser(user()->id, user()->profile->username);

        return $activity->delete();
    }

    public static function canRenewOrRecategorize($id)
    {
        return self::$model
            ::select('id')
            ->whereId($id)
            ->whereUserId(user()->id)
            ->firstOrFail();
    }

    public static function putAvailable($activityId)
    {
        $activity = self::getFirstOrFailBy('id', $activityId, ['id', 'user_id']);
        $activity->activity_state_id = StateRepository::getAvailable()->id;
        $result = $activity->save();

        self::removeCacheOfActivityUser($activity->user_id, $activity->profile->username);

        return $result;
    }

    public static function update($activityId, array $properties)
    {
        $activity = self::$model
            ::select('id')
            ->whereId($activityId)
            ->whereUserId(user()->id)
            ->firstOrFail();

        $activity = self::assignPropertiesToActivity($activity, $properties);
        $contactProperties = ContactRepository::instance($properties);

        $imagesProperties = self::getImageModels($properties['images']);

        if (@$properties['characteristics']) {
            $characteristicsProperties = self::getCharacteristicModels($properties['characteristics']);
        }

        try {
            DB::beginTransaction();

            $activity->update();
            $activityContact = $activity->contact();
            $activityImages = $activity->images();

            $activityContact->delete();
            $activityContact->save($contactProperties);
            $activityImages->delete();
            $activityImages->saveMany($imagesProperties);

            if (@$properties['characteristics']) {
                $activityCharacteristics = $activity->characteristics();
                $activityCharacteristics->delete();
                $activityCharacteristics->saveMany($characteristicsProperties);
            }

            self::removeCacheOfActivityUser(user()->id, user()->profile->username);

            DB::commit();

            return $activity->id;
        } catch (Throwable $th) {
            DB::rollBack();

            dd($th->getMessage());
            throw new UnprocessableEntityHttpException;
        }
    }

    public static function store(array $properties)
    {
        $activityProperties = self::getPropertiesToStore($properties);
        $contactProperties = ContactRepository::instance($properties);

        $properties['activity_publish_type_id'] = $activityProperties['activity_publish_type_id'];
        $subscriptionProperties = SubscriptionRepository::instance($properties);

        $imagesProperties = self::getImageModels($properties['images']);

        if (@$properties['characteristics']) {
            $characteristicsProperties = self::getCharacteristicModels($properties['characteristics']);
        }

        try {
            DB::beginTransaction();

            $activity = parent::store($activityProperties);
            $activity->contact()->save($contactProperties);
            $activity->subscription()->save($subscriptionProperties);
            $activity->images()->saveMany($imagesProperties);

            if (@$properties['characteristics']) {
                $activity->characteristics()->saveMany($characteristicsProperties);
            }

            self::removeCacheOfActivityUser(user()->id, user()->profile->username);

            DB::commit();

            return $activity->id;
        } catch (Throwable $th) {
            DB::rollBack();
            throw new UnprocessableEntityHttpException;
        }
    }

    public static function removeCacheOfActivityUser($userId, $username)
    {
        removeCache([
            self::getCacheKey('me', $userId),
            self::getCacheKey('username', $username)
        ]);
    }

    public static function updateOutdatedActivities()
    {
        $now = now();
        $stateExpiredId = StateRepository::getExpiredAt()->id;
        $stateAvailableId = StateRepository::getAvailable()->id;

        return self::$model
            ::select('id')
            ->join('activity_subscription', function ($join) use ($now) {
                $join->on('activity_subscription.activity_id', 'activity.id')
                    ->where('activity_subscription.expire_at', '<=', $now);
            })
            ->whereActivityStateId($stateAvailableId)
            ->update([
                'activity.activity_state_id' => $stateExpiredId
            ]);
    }

    private static function assignPropertiesToActivity($activity, $properties)
    {
        $properties = self::getProperties($properties);

        $activity->title = $properties['title'];
        $activity->description = $properties['description'];
        $activity->price = $properties['price'];
        $activity->latitude = $properties['latitude'];
        $activity->longitude = $properties['longitude'];
        $activity->location = $properties['location'];
        $activity->activity_type_id = $properties['activity_type_id'];
        $activity->activity_difficulty_id = $properties['activity_difficulty_id'];
        $activity->is_landscape = $properties['is_landscape'];

        return $activity;
    }

    private static function getPropertiesToStore(array $properties)
    {
        $properties = array_merge($properties, self::getProperties($properties));

        $publishType = PublishTypeRepository::getByName($properties['publish_type']);
        $properties['activity_state_id'] = StateRepository::getStateByPublishType($publishType)->id;
        $properties['activity_publish_type_id'] = $publishType->id;

        return $properties;
    }

    private static function getProperties(array $properties)
    {
        $properties['is_landscape'] = isAdmin();
        $properties['price'] = intval($properties['price']);
        $properties['activity_type_id'] = !isNotUser() ? TypeRepository::getByName($properties['activity_type'])->id : null;
        $properties['activity_difficulty_id'] = !isNotUser() ? DifficultyRepository::getByName($properties['activity_difficulty'])->id : null;

        $reverseGeocodingService = new ReverseGeocodingService($properties['latitude'], $properties['longitude']);
        $location = $reverseGeocodingService->getLocation()['location'];
        $properties['location'] = $location;

        return $properties;
    }

    private static function getImageModels(array $images)
    {
        $models = [];

        foreach ($images as $image) {
            $models[] = ImageRepository::instance([
                'image_url' => $image
            ]);
        }

        return $models;
    }

    private static function getCharacteristicModels(array $characteristics)
    {
        $models = [];

        foreach ($characteristics as $characteristic) {
            $models[] = CharacteristicRepository::instance([
                'concept' => $characteristic['concept'],
                'value' => $characteristic['value']
            ]);
        }

        return $models;
    }
}
