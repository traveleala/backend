<?php

namespace App\Repositories\v1\Activity;

use App\Repositories\v1\BaseRepository;
use DB;
use App\Classes\v1\Log;
use App\Values\v1\PublishTypeValues;
use App\Models\v1\Activity\Subscription;
use Throwable;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class SubscriptionRepository extends BaseRepository
{
    public static $model = Subscription::class;

    public static function getPaidOutByPublishType(object $publishType)
    {
        return !isUser() ? true : $publishType->name == PublishTypeValues::BASIC_TYPE;
    }

    public static function instance($properties)
    {
        $publishType = PublishTypeRepository::getByIdWithCacheForever($properties['activity_publish_type_id']);
        $subscription = parent::instance([]);
        $subscription->paid_out = self::getPaidOutByPublishType($publishType);
        $subscription->activity_publish_type_id = $publishType->id;
        $subscription->expire_at = self::calculateExpireAt($properties['month']);
        $subscription->amount = self::calculateAmount($properties['month'], $publishType);
        $subscription->month = self::calculateMonths($properties['month']);

        return $subscription;
    }

    public static function calculateMonths($month)
    {
        return !isUser() ? null : $month;
    }

    public static function calculateAmount($month, object $publishType)
    {
        if (!$publishType->price) {
            return '0';
        } else {
            $partial = $month * $publishType->price;
            $discount = $partial * $publishType->discount + 1;
            return $partial - $discount;
        }
    }

    public static function calculateExpireAt($months)
    {
        return isUser() ? now()->addMonths($months) : null;
    }

    public static function canPay($id)
    {
        return ActivityRepository::$model
            ::paymentPending()
            ->select('id')
            ->whereId($id)
            ->whereUserId(user()->id)
            ->firstOrFail();
    }

    public static function canViewStatistics($activityId)
    {
        return self::$model
            ::select('activity.created_at')
            ->join('activity', function ($join) {
                $join->on('activity.id', '=', 'activity_subscription.activity_id');
            })
            ->whereActivityId($activityId)
            ->where('activity.user_id', user()->id)
            ->where('amount', '!=', '0')
            ->where('paid_out', true)
            ->firstOrFail();
    }

    public static function recategorize($properties)
    {
        $publishType = PublishTypeRepository::getByName($properties['publish_type']);
        $properties['activity_publish_type_id'] = $publishType->id;

        try {
            DB::beginTransaction();

            self::getFirstOrFailBy('activity_id', $properties['activityId'], ['id'])->delete();

            $activity = ActivityRepository::getFirstOrFailBy('id', $properties['activityId'], ['id']);
            $activity->activity_state_id = StateRepository::getStateByPublishType($publishType)->id;
            $activitySubscription = self::instance($properties);
            $activity->subscription()->save($activitySubscription);
            $activity->activity_publish_type_id = $publishType->id;
            $activity->save();

            DB::commit();

            ActivityRepository::removeCacheOfActivityUser(user()->id, user()->profile->username);

            return true;
        } catch (Throwable $th) {
            DB::rollBack();

            Log::app()->error(
                "We can't recategorize the subscription activity {$properties['activityId']}",
                ['error' => $th->getMessage()]
            );

            throw new UnprocessableEntityHttpException;
        }
    }

    public static function paid($subscriptionId)
    {
        try {
            DB::beginTransaction();

            $subscription = self::getFirstOrFailBy('id', $subscriptionId, ['id', 'activity_id']);
            $subscription->paid_out = true;
            $subscription->save();

            ActivityRepository::putAvailable($subscription->activity_id);

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();

            Log::app()->error(
                "We can't update the subscription {$subscriptionId}",
                ['error' => $th->getMessage()]
            );
        }
    }
}
