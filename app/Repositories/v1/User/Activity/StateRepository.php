<?php

namespace App\Repositories\v1\Activity;

use App\Values\v1\ActivityStateValues;
use App\Repositories\v1\BaseRepository;
use App\Values\v1\PublishTypeValues;
use App\Models\v1\Activity\State;

class StateRepository extends BaseRepository
{
    const CACHE_PREFIX = 'activities:states';

    public static $model = State::class;

    public static function getStateByPublishType(object $publishType)
    {
        if (!isUser()) {
            $activityStateName = ActivityStateValues::AVAILABLE_STATE;
        } else {
            $activityStateName =  $publishType->name != PublishTypeValues::BASIC_TYPE ?
                ActivityStateValues::PAYMENT_PENDING_STATE :
                ActivityStateValues::AVAILABLE_STATE;
        }

        $key = self::getCacheKey($activityStateName);

        return self::withCacheForever(function () use ($activityStateName) {
            return self::getFirstOrFailBy('name', $activityStateName);
        }, $key);
    }

    public static function getPaymentPending()
    {
        $state = ActivityStateValues::PAYMENT_PENDING_STATE;
        return self::getByName($state);
    }

    public static function getAvailable()
    {
        $state = ActivityStateValues::AVAILABLE_STATE;
        return self::getByName($state);
    }

    public static function getExpiredAt()
    {
        $state = ActivityStateValues::EXPIRED_STATE;
        return self::getByName($state);
    }

    public static function getByName($name)
    {
        $key = self::getCacheKey($name);

        return self::withCacheForever(function () use ($name) {
            return self::getFirstOrFailBy('name', $name);
        }, $key);
    }
}
