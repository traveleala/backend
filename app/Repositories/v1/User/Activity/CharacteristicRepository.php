<?php

namespace App\Repositories\v1\Activity;

use App\Repositories\v1\BaseRepository;
use App\Models\v1\Activity\Characteristic;

class CharacteristicRepository extends BaseRepository
{
    public static $model = Characteristic::class;
}
