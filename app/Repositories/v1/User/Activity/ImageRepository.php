<?php

namespace App\Repositories\v1\Activity;

use App\Repositories\v1\BaseRepository;
use App\Traits\v1\HandleImageModelTrait;
use App\Models\v1\Activity\Image;

class ImageRepository extends BaseRepository
{
    use HandleImageModelTrait;

    public static $model = Image::class;

    public static function getActivityByImageUrl($url)
    {
        return self::$model
            ::select('activity_image.id')
            ->join('activity', function ($join) {
                $join->on('activity.id', 'activity_image.activity_id')
                    ->where('activity.user_id', user()->id);
            })
            ->whereImageUrl($url)
            ->firstOrFail();
    }

    public static function getTotalImagesOfActivityByImage($url){

        return self::$model
            ::select('id')
            ->whereIn('activity_id', function($query) use($url){
                $query->select('activity_id')
                ->from(self::getTableName())
                ->where('image_url', $url);
            })->count();
    }
}
