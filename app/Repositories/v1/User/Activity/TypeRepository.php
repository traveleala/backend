<?php

namespace App\Repositories\v1\Activity;

use App\Repositories\v1\BaseRepository;
use App\Models\v1\Activity\Type;

class TypeRepository extends BaseRepository
{
    const CACHE_PREFIX = 'activities:types';

    public static $model = Type::class;

    public static function get()
    {
        return self::withCacheForever(function () {
            return parent::all()->pluck('name');
        });
    }

    public static function getByNames($types)
    {
        return self::$model
            ::select('id')
            ->whereIn('name', $types)
            ->get();
    }

    public static function getByName($name)
    {
        $key = self::getCacheKey($name);

        return self::withCacheForever(function () use ($name) {
            return self::getFirstOrFailBy('name', $name);
        }, $key);
    }
}
