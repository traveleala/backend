<?php

namespace App\Repositories\v1\Activity;

use App\Repositories\v1\BaseRepository;
use App\Models\v1\Activity\PublishType;

class PublishTypeRepository extends BaseRepository
{
    const CACHE_PREFIX = 'activities:publishes:types';

    public static $model = PublishType::class;

    public static function get()
    {
        return self::withCacheForever(function () {
            return parent::all();
        });
    }

    public static function getByName($name)
    {
        $key = self::getCacheKey($name);

        return self::withCacheForever(function () use ($name) {
            return self::getFirstOrFailBy('name', $name);
        }, $key);
    }
}
