<?php

namespace App\Repositories\v1\Activity;

use App\Repositories\v1\BaseRepository;
use App\Models\v1\Activity\Difficulty;

class DifficultyRepository extends BaseRepository
{
    const CACHE_PREFIX = 'activities:difficulties';

    public static $model = Difficulty::class;

    public static function get()
    {
        return self::withCacheForever(function () {
            return self::$model::orderBy('id', 'asc')->get()->pluck('name');
        });
    }

    public static function getByNames($difficulties)
    {
        return self::$model
            ::select('id')
            ->whereIn('name', $difficulties)
            ->get();
    }

    public static function getByName($name)
    {
        $key = self::getCacheKey($name);

        return self::withCacheForever(function () use ($name) {
            return self::getFirstOrFailBy('name', $name);
        }, $key);
    }
}
