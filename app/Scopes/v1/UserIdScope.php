<?php

namespace App\Scopes\v1;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class UserIdScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $userLogged = user();
        if ($userLogged) {
            $builder->whereUserId($userLogged->id);
        }
    }
}
