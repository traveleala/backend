<?php

namespace App\Scopes\v1;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class ActivityQualificationIdScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->whereActivityQualificationId(request()->activity_qualification_id);
    }
}
