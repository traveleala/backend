<?php

namespace App\Traits\v1;

use App\Events\v1\User\CreatedEvent;
use App\Values\v1\EmailSubjectValues;

trait SendVerificationEmailTrait
{
    public static function send($model, $token)
    {
        $dataEmail = [
            'nameFrom' =>  conf('services.sendgrid.name_from'),
            'emailFrom' => conf('services.sendgrid.mail_from'),
            'subject' => EmailSubjectValues::WELCOME_SUBJECT,
            'token' => $token
        ];

        event(new CreatedEvent($model, $dataEmail));
    }
}
