<?php

namespace App\Traits\v1;

use App\Services\v1\FileService;
use App\Values\v1\UploadTypeValues;

trait HandleImageModelTrait
{
    private static function upload(array $images, $type)
    {
        $fileService = new FileService($type);
        $imageModels = [];

        foreach ($images['images'] as $image) {

            $properties = [
                'image_url' => $fileService->upload($image)
            ];
            $imageModels[] = self::instance($properties);
        }

        return $imageModels;
    }

    private static function rollback(array $images, $type)
    {
        $fileService = new FileService($type);
        array_map(function ($imageModel) use ($fileService) {
            return $imageModel->rollBack($fileService);
        }, $images);
    }

    public static function uploadQualificationImages(array $images)
    {
        return self::upload($images, UploadTypeValues::QUALIFICATION_TYPE);
    }

    public static function rollbackQualificationImages(array $images)
    {
        return self::rollback($images, UploadTypeValues::QUALIFICATION_TYPE);
    }
}
