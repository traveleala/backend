<?php

namespace App\Classes\v1;

use Log as LogLumen;

class Log
{
    const NOTIFICATIONS_CHANNEL = 'notifications';
    const ACTIVITIES_CHANNEL = 'activities';
    const APP_CHANNEL = 'app';

    public static function notifications()
    {
        return LogLumen::channel(self::NOTIFICATIONS_CHANNEL);
    }

    public static function activities()
    {
        return LogLumen::channel(self::ACTIVITIES_CHANNEL);
    }

    public static function app()
    {
        return LogLumen::channel(self::APP_CHANNEL);
    }
}
