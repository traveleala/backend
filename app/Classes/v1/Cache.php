<?php

namespace App\Classes\v1;

use App\Values\v1\CacheValues;

class Cache
{
    public static function rememberForever($closure, $key = '')
    {
        if (conf('app.cache')) {
            $result = cache()->rememberForever($key, $closure);
        } else {
            $result = Cache::rememberDefault($closure, $key);
        }

        return $result;
    }

    public static function rememberForAWeek($closure, $key = '')
    {
        $ttl = conf('app.cache') ? CacheValues::WEEKLY_TTL : 0;
        return self::save($closure, $ttl, $key);
    }

    public static function rememberForAnHour($closure, $key = '')
    {
        $ttl = conf('app.cache') ? CacheValues::HOURLY_TTL : 0;
        return self::save($closure, $ttl, $key);
    }

    public static function rememberDefault($closure, $key = '')
    {
        $ttl = conf('app.cache') ? CacheValues::DEFAULT_TTL : 0;
        return self::save($closure, $ttl, $key);
    }

    private static function save($closure, $ttl, $key)
    {
        return cache()->remember($key, $ttl, $closure);
    }
}
