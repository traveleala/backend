<?php

namespace App\Classes\v1;

use Hashids\Hashids;

class Hasher
{
    private static $instance;

    private function __construct()
    {
    }

    public static function instance()
    {
        if (!self::$instance) {
            self::$instance = new Hashids(conf('app.key'), 8);
        }

        return self::$instance;
    }
}
