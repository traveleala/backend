<?php

namespace App\Providers;

use App\Events\v1\User\CreatedEvent;
use App\Listeners\v1\User\CreatedListener;
use App\Events\v1\User\ResetPasswordEvent;
use App\Listeners\v1\User\ResetPasswordListener;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;
use SocialiteProviders\Manager\SocialiteWasCalled;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        CreatedEvent::class => [
            CreatedListener::class
        ],
        ResetPasswordEvent::class => [
            ResetPasswordListener::class
        ],
        SocialiteWasCalled::class => [
            'SocialiteProviders\Facebook\FacebookExtendSocialite@handle',
            'SocialiteProviders\Google\GoogleExtendSocialite@handle',
        ],
    ];
}
