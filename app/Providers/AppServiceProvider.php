<?php

namespace App\Providers;

use App\Rules\v1\AreUrlsFromBucketRule;
use App\Rules\v1\CanChangeUserNameRule;
use App\Rules\v1\CanResetPasswordRule;
use App\Rules\v1\CanResendVerificationRule;
use App\Rules\v1\CoordinatesAreInRule;
use App\Rules\v1\CaptchaRule;
use App\Rules\v1\CredentialsRule;
use App\Rules\v1\ExistsAndRule;
use App\Rules\v1\IsAvailableToResetRule;
use App\Rules\v1\IuniqueRule;
use App\Rules\v1\IsAUrlBucketRule;
use App\Rules\v1\IsVerifiedRule;
use App\Rules\v1\MinImagesInActivityRule;
use App\Rules\v1\RangeMonthsPublishTypeRule;
use App\Rules\v1\RequireOnesRule;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

    public function boot()
    {
        Validator::extend('existsAnd', function ($attribute, $value, $parameters) {
            $existsAndRule = new ExistsAndRule($parameters);
            return $existsAndRule->passes($attribute, $value);
        });

        Validator::extend('coordinatesAreIn', function ($attribute, $value, $parameters) {
            $coordinaresAreInRule = new CoordinatesAreInRule($parameters);
            return $coordinaresAreInRule->passes($attribute, $value);
        });

        Validator::extend('rangeMonthsPublishType', function ($attribute, $value, $parameters) {
            $rageMonthsPublishTypeRule = new RangeMonthsPublishTypeRule($parameters);
            return $rageMonthsPublishTypeRule->passes($attribute, $value);
        });

        Validator::extend('iunique', function ($attribute, $value, $parameters) {
            $iuniqueRule = new IuniqueRule($parameters);
            return $iuniqueRule->passes($attribute, $value);
        });

        Validator::extend('requireOnes', function ($attribute, $value, $parameters) {
            $requireOnesRule = new RequireOnesRule($parameters);
            return $requireOnesRule->passes($attribute, $value);
        });

        Validator::extend('credentials', function ($attribute, $value, $parameters) {
            $credentialsRule = new CredentialsRule($parameters);
            return $credentialsRule->passes($attribute, $value);
        });

        Validator::extend('isVerified', function ($attribute, $value, $parameters) {
            $isVerifiedRule = new IsVerifiedRule($parameters);
            return $isVerifiedRule->passes($attribute, $value);
        });

        Validator::extend('isAUrlBucket', function ($attribute, $value, $parameters) {
            $isAUrlBucketRule = new IsAUrlBucketRule($parameters);
            return $isAUrlBucketRule->passes($attribute, $value);
        });

        Validator::extend('areUrlsFromBucket', function ($attribute, $value, $parameters) {
            $areUrlsFromBucketRule = new AreUrlsFromBucketRule($parameters);
            return $areUrlsFromBucketRule->passes($attribute, $value);
        });

        Validator::extend('isAvailableToReset', function ($attribute, $value, $parameters) {
            $isAvailableToResetRule = new IsAvailableToResetRule($parameters);
            return $isAvailableToResetRule->passes($attribute, $value);
        });

        Validator::extend('canResetPassword', function ($attribute, $value, $parameters) {
            $canResetPassword = new CanResetPasswordRule($parameters);
            return $canResetPassword->passes($attribute, $value);
        });

        Validator::extend('canResendVerification', function ($attribute, $value, $parameters) {
            $canResendVerification = new CanResendVerificationRule($parameters);
            return $canResendVerification->passes($attribute, $value);
        });

        Validator::extend('canChangeUserName', function ($attribute, $value, $parameters) {
            $canChangeUserName = new CanChangeUserNameRule($parameters);
            return $canChangeUserName->passes($attribute, $value);
        });

        Validator::extend('captcha', function ($attribute, $value, $parameters) {
            $captchaRule = new CaptchaRule($parameters);
            return $captchaRule->passes($attribute, $value);
        });

        Validator::extend('minImagesInActivity', function ($attribute, $value, $parameters) {
            $minImagesInActivity = new MinImagesInActivityRule($parameters);
            return $minImagesInActivity->passes($attribute, $value);
        });        
    }
}
