<?php

namespace App\Providers;

use App\Values\v1\HttpCodeValues;
use Illuminate\Http\Response;
use Illuminate\Support\ServiceProvider;
use stdClass;

class ResponseMacroErrorServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Response::macro('errors', function ($errors, $rules, $resource) {
            $errorCodes = array();

            foreach ($rules as $fieldName => $rules) {
                $errorCodes[$fieldName] = $errors->first($fieldName);
            }

            $content = new stdClass();
            $content->status = HttpCodeValues::INVALID_REQUEST_CODE;
            $content->success = false;
            $content->errors = $errorCodes;

            return response()->json($content, HttpCodeValues::INVALID_REQUEST_CODE);
        });
    }

    public function register()
    {
        //
    }
}
