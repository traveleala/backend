<?php

namespace App\Rules\v1;

use Illuminate\Contracts\Validation\Rule;

class CanChangeUserNameRule implements Rule
{
    const LIMIT_DAYS = 7;

    public function passes($attribute, $username)
    {
        if (user()->profile->username != $username && user()->profile->change_restricted_at) {

            $diffInDays = now()->diffInDays(user()->profile->change_restricted_at);

            if ($diffInDays < self::LIMIT_DAYS) {
                return false;
            }
        }

        return true;
    }

    public function message()
    {
    }
}
