<?php

namespace App\Rules\v1;

use App\Repositories\v1\Activity\PublishTypeRepository;
use Illuminate\Contracts\Validation\Rule;
use Throwable;

class RangeMonthsPublishTypeRule implements Rule
{
    private $parameters;

    public function __construct($parameters)
    {
        $this->parameters = $parameters;
    }

    public function passes($attribute, $months)
    {
        try {
            $publishType = request()->{$this->parameters[0]};
            $publishType = PublishTypeRepository::getByName($publishType);

            $result = $publishType->min_month > $months ? false : true;
            $result = $result ? ($publishType->max_month < $months ? false : true) : false;
        } catch (Throwable $th) {
            $result = false;
        }

        return $result;
    }

    public function message()
    {
    }
}
