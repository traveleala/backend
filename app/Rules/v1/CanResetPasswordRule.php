<?php

namespace App\Rules\v1;

use App\Repositories\v1\Admin\User\UserRepository;
use Illuminate\Contracts\Validation\Rule;

class CanResetPasswordRule implements Rule
{
    public function __construct($parameters)
    {
        $this->parameters = $parameters;
    }

    public function passes($attribute, $email)
    {
        $user = UserRepository::getFirstOrFailBy('email', $email, ['id']);
        $user->load('resetPassword');

        if ($user->resetPassword) {

            if ($user->resetPassword->tries <= 0) {
                $diffInDays = now()->diffInDays($user->resetPassword->updated_at);

                if (!$diffInDays) {
                    return false;
                }
            }
        }

        return true;
    }

    public function message()
    {
    }
}
