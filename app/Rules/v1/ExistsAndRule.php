<?php

namespace App\Rules\v1;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class ExistsAndRule implements Rule
{
    private $parameters;

    public function __construct($parameters)
    {
        $this->parameters = $parameters;
    }

    public function passes($attribute, $value)
    {
        $table = $this->parameters[0];
        $column = $this->parameters[1];
        unset($this->parameters[0], $this->parameters[1]);

        $query = DB::table($table)->where($column, $value);

        foreach ($this->parameters as $parameter) {
            $parameter = explode(':', $parameter);
            $column = $parameter[0];
            $value = $parameter[1];

            $query->where($column, $value);
        }

        return $query->exists();
    }

    public function message()
    {
    }
}
