<?php

namespace App\Rules\v1;

use App\Repositories\v1\Admin\User\UserRepository;
use Illuminate\Contracts\Validation\Rule;

class CanResendVerificationRule implements Rule
{
    public function __construct($parameters)
    {
        $this->parameters = $parameters;
    }

    public function passes($attribute, $email)
    {
        $user = UserRepository::getFirstOrFailBy('email', $email, ['id']);
        $user->load('verification');

        if ($user->verification) {

            if ($user->verification->tries <= 0) {
                $diffInDays = now()->diffInDays($user->verification->updated_at);

                if (!$diffInDays) {
                    return false;
                }
            }
        }

        return true;
    }

    public function message()
    {
    }
}
