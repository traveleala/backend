<?php

namespace App\Rules\v1;

use App\Repositories\v1\Activity\ImageRepository;
use Illuminate\Contracts\Validation\Rule;

class MinImagesInActivityRule implements Rule
{
    const LIMIT_IMAGES = 8;

    public function passes($attribute, $image)
    {
        $total = ImageRepository::getTotalImagesOfActivityByImage($image);

        return !($total <= self::LIMIT_IMAGES);
    }

    public function message()
    {
    }
}
