<?php

namespace App\Rules\v1;

use Illuminate\Contracts\Validation\Rule;

class IsAUrlBucketRule implements Rule
{
    public function passes($attribute, $value)
    {
        $urlBucket = conf('filesystems.disks.s3.url');

        return strpos($value, $urlBucket) !== false;
    }

    public function message()
    {
    }
}
