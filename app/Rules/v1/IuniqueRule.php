<?php

namespace App\Rules\v1;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Validation\Rule;

class IuniqueRule implements Rule
{
    private $parameters;

    public function __construct($parameters)
    {
        $this->parameters = $parameters;
    }

    public function passes($attribute, $value)
    {
        $table = $this->parameters[0];
        $column = $this->parameters[1];
        unset($this->parameters[0], $this->parameters[1]);

        $wheres = $this->parameters;

        $result = DB::table($table)->where($column, $value)
            ->where(function ($query) use ($wheres) {

                foreach ($wheres as $where) {
                    $columnValue = explode(':', $where);
                    $query->where($columnValue[0], '!=', $columnValue[1]);
                }
            })->exists();

        return $result ? false : true;
    }

    public function message()
    {
    }
}
