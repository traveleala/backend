<?php

namespace App\Rules\v1;

use Illuminate\Contracts\Validation\Rule;
use App\Repositories\v1\User\UserRepository;

class IsVerifiedRule implements Rule
{
    public function passes($attribute, $email)
    {
        return UserRepository::hasBeenVerified($email);
    }

    public function message()
    {
    }
}
