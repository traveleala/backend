<?php

namespace App\Rules\v1;

use App\Repositories\v1\Admin\User\UserRepository;
use Illuminate\Contracts\Validation\Rule;

class IsAvailableToResetRule implements Rule
{
    public function __construct($parameters)
    {
        $this->parameters = $parameters;
    }

    public function passes($attribute, $email)
    {
        $user = UserRepository::getFirstOrFailBy('email', $email, ['reset_restricted_at']);

        if (!$user->reset_restricted_at) {
            return true;
        } else {
            return now()->diffInDays($user->reset_restricted_at) >= 1;
        }
    }

    public function message()
    {
    }
}
