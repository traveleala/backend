<?php

namespace App\Rules\v1;

use Illuminate\Contracts\Validation\Rule;

class AreUrlsFromBucketRule implements Rule
{
    public function passes($attribute, $images)
    {
        $isAUrlBucketRule = new IsAUrlBucketRule;
        $result = true;

        foreach ($images as $image) {

            if (!$isAUrlBucketRule->passes('image', $image)) {
                $result = false;
                break;
            }
        }

        return $result;
    }

    public function message()
    {
    }
}
