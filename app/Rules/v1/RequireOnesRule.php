<?php

namespace App\Rules\v1;

use Illuminate\Contracts\Validation\Rule;

class RequireOnesRule implements Rule
{
    private $parameters;

    public function __construct($parameters)
    {
        $this->parameters = $parameters;
    }

    public function passes($attribute, $value)
    {
        $key = $this->parameters[0];

        if ($value && request()->$key) {
            return false;
        }

        return true;
    }

    public function message()
    {
    }
}
