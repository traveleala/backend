<?php

namespace App\Rules\v1;

use AlbertCht\InvisibleReCaptcha\InvisibleReCaptcha;
use App\Values\v1\AppValues;
use Illuminate\Contracts\Validation\Rule;

class CaptchaRule implements Rule
{
    public function passes($attribute, $token)
    {
        $result = true;

        if (conf('app.env') == AppValues::ENV_PRODUCTION) {
            $siteKey = conf('services.captcha.public_key');
            $secretKey = conf('services.captcha.private_key');
            $domain = conf('app.url');
            $options = [
                'timeout' => conf('services.captcha.timeout')
            ];

            $captcha = new InvisibleReCaptcha($siteKey, $secretKey, $options);
            $result = $captcha->verifyResponse($token, $domain);
        }

        return $result;
    }

    public function message()
    {
    }
}
