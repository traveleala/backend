<?php

namespace App\Rules\v1;

use Illuminate\Contracts\Validation\Rule;
use YPL\Nudity\Nudity;
use Imagine\Gd\Imagine;

class IsPornRule implements Rule
{
    private $parameters;

    public function __construct($parameters)
    {
        $this->parameters = $parameters;
    }

    public function passes($attribute, $file)
    {
        $nudity = new Nudity(new Imagine);

        return !$nudity->detect($file->path());
    }

    public function message()
    {
    }
}
