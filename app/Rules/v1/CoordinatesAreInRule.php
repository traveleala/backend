<?php

namespace App\Rules\v1;

use Illuminate\Contracts\Validation\Rule;
use App\Services\v1\ReverseGeocodingService;
use Throwable;

class CoordinatesAreInRule implements Rule
{
    private $parameters;

    public function __construct($parameters)
    {
        $this->parameters = $parameters;
    }

    public function passes($attribute, $longitude)
    {
        $country = $this->parameters[0];
        $latitude = request()->{$this->parameters[1]};
        $result = false;

        try {
            $reverseGeocodingService = new ReverseGeocodingService($latitude, $longitude);
            $locationResponse = $reverseGeocodingService->getLocation();

            $result = $locationResponse['country'] == $country;
        } catch (Throwable $th) {
        }

        return $result;
    }

    public function message()
    {
    }
}
