<?php

namespace App\Rules\v1;

use App\Services\v1\AuthService;
use Illuminate\Contracts\Validation\Rule;
use Throwable;
use App\Repositories\v1\User\UserRepository;

class CredentialsRule implements Rule
{
    private $isLogin;

    public function __construct($parameters)
    {
        $this->isLogin = $parameters ? true : false;
    }

    public function passes($attribute, $value)
    {
        if ($this->isLogin) {
            $user = UserRepository::getFirstOrFailBy('email', $value, ['password']);
        } else {
            $user = user();
        }

        try {
            AuthService::attempt(request()->password, $user->password);
        } catch (Throwable $th) {
            return false;
        }

        return true;
    }

    public function message()
    {
    }
}
