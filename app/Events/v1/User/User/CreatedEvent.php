<?php

namespace App\Events\v1\User;

use App\Events\v1\Event;

class CreatedEvent extends Event
{
    public $userModel;
    public $dataEmail;

    public function __construct(object $userModel, array $dataEmail)
    {
        $this->userModel = $userModel;
        $this->dataEmail = $dataEmail;
    }
}
