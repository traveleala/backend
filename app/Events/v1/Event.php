<?php

namespace App\Events\v1;

use Illuminate\Queue\SerializesModels;

abstract class Event
{
    use SerializesModels;
}
