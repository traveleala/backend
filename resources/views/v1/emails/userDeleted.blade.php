@extends('v1.emails.base')

@section('content')
    <h1>Tu cuenta ha sido eliminada</h1>
    <p>Hemos recibido numerosos reportes sobre tu conducta en nuestro sitio, por lo cual hemos tomado la decisión de eliminar
        tu cuenta del sitio de manera permanente.</p>
@endsection