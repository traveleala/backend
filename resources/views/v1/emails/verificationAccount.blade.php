@extends('v1.emails.base')

@section('content')
    <h1>Te damos la bienvenida</h1>
    <p>Gracias por crear una cuenta en nuestro sitio, estas a un paso de poder empezar a crear tus actividades, calificar actividades, encontrar nuevos lugares para vacacionar</p>

    <p>Para verificar tu cuenta haz click en el siguiente enlace</p>

    <a href="{{conf('app.url')}}?v={{$token}}" class="button">Verifica tu cuenta</a>

    <p>Sino funciona el enlace podras hacer click en el siguiente:</p>
<a href="{{conf('app.url')}}?v={{$token}}" class="text-link">{{conf('app.url')}}?v={{$token}}</a>
@endsection