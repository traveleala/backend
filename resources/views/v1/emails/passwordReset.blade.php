@extends('v1.emails.base')

@section('content')
    <h1>Reinicio de contraseña</h1>
    <p>Has solicitado el cambio de contraseña, para poder crear tu nueva contraseña deberas ingresar al siguiente enlace para crear la nueva</p>

    <a href="{{conf('app.url')}}?rp={{$token}}" class="button">Reiniciar contraseña</a>

    <p>Sino funciona el enlace podras hacer click en el siguiente:</p>
<a href="{{conf('app.url')}}?rp={{$token}}" class="text-link">{{conf('app.url')}}?rp={{$token}}</a>
@endsection