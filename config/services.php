<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'sendgrid' => [
        'key' => env('SEND_GRID_API_KEY'),
        'mail_from' => env('MAIL_FROM'),
        'name_from' => env('NAME_FROM')
    ],

    'big_data_cloud' => [
        'key' => env('BIG_DATA_CLOUD_API_KEY'),
        'url' => env('BIG_DATA_CLOUD_API_URL')
    ],

    'mp' => [
        'access_token' => env('ACCESS_TOKEN_MP'),
        'url_payment_pending' => env('URL_PAYMENT_PENDING'),
        'url_payment_failure' => env('URL_PAYMENT_FAILURE'),
        'url_payment_success' => env('URL_PAYMENT_SUCCESS'),
    ],

    'captcha' => [
        'public_key' => env('CAPTCHA_PUBLIC_KEY'),
        'private_key' => env('CAPTCHA_PRIVATE_KEY'),
        'timeout' => env('CAPTCHA_TIMEOUT')
    ],

    'facebook' => [
        'client_id'     => env('FACEBOOK_CLIENT_ID'),
        'client_secret' => env('FACEBOOK_SECRET'),
        'redirect'      => env('APP_URL') . env('FACEBOOK_CALLBACK_PATH')
    ],

    'google' => [
        'client_id' => env('GOOGLE_CLIENT_ID'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),
        'redirect' => env('APP_URL') . env('GOOGLE_CALLBACK_URL'),
    ],

    'analytics' => [
        'email' => env('GOOGLE_ANALYTICS_EMAIL'),
        'profile_id' => env('GOOGLE_ANALYTICS_PROFILE_ID'),
        'key_file_name' => env('GOOGLE_ANALYTICS_KEY_FILE_NAME')
    ]
];
