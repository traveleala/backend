<?php

namespace Tests;

use Laravel\Lumen\Testing\TestCase as BaseTestCase;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Faker\Factory;
use App\Repositories\v1\User\ProfileRepository;
use App\Repositories\v1\User\UserRepository;

class TestCase extends BaseTestCase
{
    use DatabaseMigrations;

    protected $faker;

    public function __construct()
    {
        parent::__construct();

        $this->faker = Factory::create();
    }

    public function setUp(): void
    {
        parent::setUp();

        //$this->artisan('migrate:refresh');

        $user = factory(UserRepository::$model)->create([
            'email' => 'info@traveleala.com',
            'password' => password('info@traveleala.com')
        ]);
        factory(ProfileRepository::$model)->create([
            'user_id' => $user->id,
            'username' => 'traveleala'
        ]);

        $this->userToken = $user;

        $this->token = login($this->userToken->id);
    }

    public function createApplication()
    {
        return require __DIR__ . '/../bootstrap/app.php';
    }

    public function json($method, $uri, $data = [], $headers = [])
    {
        return parent::json(
            $method,
            $uri,
            $data,
            $headers
                ? $headers
                : [
                    'Authorization' => "Bearer {$this->token}"
                ]
        );
    }
}
