<?php

namespace Tests;

use App\Repositories\v1\User\UserRepository;
use App\Events\v1\User\ResetPasswordEvent;
use App\Repositories\v1\User\ResetPasswordRepository;

class UserResetPasswordTest extends TestCase
{
    public function testResetPasswordWithEmptyValues()
    {
        $response = $this->json('post', '/v1/users/reset/password', []);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testResetPasswordWithNonValidEmail()
    {
        $response = $this->json('post', '/v1/users/reset/password', [
            'email' => 'jacksparrow'
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testResetPasswordWithValidEmailButTheUserDoesntExist()
    {
        $response = $this->json('post', '/v1/users/reset/password', [
            'email' => 'info@travelala.com'
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testUserResetPasswordWithUserButTheUserIsNotVerificated()
    {
        $user = factory(UserRepository::$model)->create([
            'verified' => 0
        ]);

        $response = $this->json('post', '/v1/users/reset/password', [
            'email' => $user->email
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testUserResetPasswordWhenTheUserIsValid()
    {
        $this->expectsEvents(ResetPasswordEvent::class);

        $user = factory(UserRepository::$model)->create();

        $response = $this->json('post', '/v1/users/reset/password', [
            'email' => $user->email
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());

        $token = ResetPasswordRepository::getFirstOrFailBy('user_id', $user->id)->token;

        $this->seeInDatabase(ResetPasswordRepository::getTableName(), [
            'token' => $token,
            'user_id' => $user->id
        ]);
    }

    public function testUserResetPasswordLimitToSend()
    {
        $this->expectsEvents(ResetPasswordEvent::class);

        $user = factory(UserRepository::$model)->create();

        for ($i = 0; $i < 5; $i++) {
            $response = $this->json('post', '/v1/users/reset/password', [
                'email' => $user->email
            ]);

            $this->assertEquals(200, $response->response->getStatusCode());
        }

        $token = ResetPasswordRepository::getFirstOrFailBy('user_id', $user->id)->token;

        $this->seeInDatabase(ResetPasswordRepository::getTableName(), [
            'token' => $token,
            'user_id' => $user->id
        ]);
    }

    public function testUserBreakTheLimitToSend()
    {
        $this->expectsEvents(ResetPasswordEvent::class);

        $user = factory(UserRepository::$model)->create();

        for ($i = 0; $i < 5; $i++) {
            $response = $this->json('post', '/v1/users/reset/password', [
                'email' => $user->email
            ]);

            $this->assertEquals(200, $response->response->getStatusCode());
        }

        $response = $this->json('post', '/v1/users/reset/password', [
            'email' => $user->email
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());

        $token = ResetPasswordRepository::getFirstOrFailBy('user_id', $user->id)->token;

        $this->seeInDatabase(ResetPasswordRepository::getTableName(), [
            'token' => $token,
            'tries' => 0,
            'user_id' => $user->id
        ]);
    }

    public function testUserSentAllTheEmailsButIsTomorrow()
    {
        $this->expectsEvents(ResetPasswordEvent::class);

        $user = factory(UserRepository::$model)->create();

        $date = now()->subDays(2);

        factory(ResetPasswordRepository::$model)->create([
            'created_at' => $date,
            'updated_at' => $date,
            'user_id' => $user->id,
            'tries' => 0
        ]);

        for ($i = 0; $i < 5; $i++) {
            $response = $this->json('post', '/v1/users/reset/password', [
                'email' => $user->email
            ]);

            $this->assertEquals(200, $response->response->getStatusCode());
        }
    }

    public function testResetPasswordUser()
    {
        $user = factory(UserRepository::$model)->create();

        $verification = factory(ResetPasswordRepository::$model)->create([
            'user_id' => $user->id,
        ]);

        $response = $this->json('put', '/v1/users/reset/password/' . $verification->token, [
            'password' => 'radaradaone',
            'password_confirmation' => 'radaradaone'
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());

        $this->seeInDatabase(UserRepository::getTableName(), [
            'id' => $user->id,
            'verified' => 1
        ]);
    }

    public function testResetPasswordUserWithInvalidToken()
    {
        $user = factory(UserRepository::$model)->create();

        $verification = factory(ResetPasswordRepository::$model)->create([
            'user_id' => $user->id,
        ]);

        $response = $this->json('put', '/v1/users/reset/password/tokentokentoken', [
            'password' => 'radaradaone',
            'password_confirmation' => 'radaradaone'
        ]);

        $this->assertEquals(404, $response->response->getStatusCode());
    }
}
