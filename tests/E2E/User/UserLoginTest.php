<?php

namespace Tests;

use App\Repositories\v1\User\UserRepository;

class UserLoginTest extends TestCase
{
    public function testLoginUserWithEmptyData()
    {
        $response = $this->json('post', '/v1/users/login', []);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testLoginUserWithInvalidData()
    {
        $response = $this->json('post', '/v1/users/login', [
            'user' => 'info',
            'password' => 'info'
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testLoginUserWithValidEmailAndInvalidPassword()
    {
        $user = factory(UserRepository::$model)->create([
            'email' => 'password@password.com',
            'password' => password('password@password.com')
        ]);

        $response = $this->json('post', '/v1/users/login', [
            'email' => $user->email,
            'password' => 'tokentoken'
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testLoginUserWithValidDataButIsNotVerified()
    {
        $user = factory(UserRepository::$model)->create([
            'email' => 'password@password.com',
            'password' => password('password@password.com'),
            'verified' => 0
        ]);

        $response = $this->json('post', '/v1/users/login', [
            'email' => $user->email,
            'password' => 'password@password.com'
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testLoginUserWithUserDeleted()
    {
        $email = 'password@password.com';

        $user = factory(UserRepository::$model)->create([
            'email' => $email,
            'password' => password($email),
            'verified' => 0
        ]);

        $user->delete();

        $response = $this->json('post', '/v1/users/login', [
            'email' => $email,
            'password' => $email
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testLoginUserWhenItIsValid()
    {
        $email = 'password@password.com';

        factory(UserRepository::$model)->create([
            'email' => $email,
            'password' => password($email),
            'verified' => 1
        ]);

        $response = $this->json('post', '/v1/users/login', [
            'email' => $email,
            'password' => $email
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());
    }

    public function testLogoutUser()
    {
        $email = 'password@password.com';

        factory(UserRepository::$model)->create([
            'email' => $email,
            'password' => password($email),
            'verified' => 1
        ]);

        $response = $this->json('post', '/v1/users/login', [
            'email' => $email,
            'password' => $email
        ]);

        $token = json_decode($response->response->getContent(), true)['token'];

        $response = $this->json('delete', '/v1/users/me/login', [], [
            'Authorization' => "Bearer {$token}"
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());
    }

    public function testUserLogged()
    {
        $response = $this->json('get', '/v1/users/me');

        $this->assertEquals(200, $response->response->getStatusCode());
    }

    // This case is doesn't work correctly, because the logout gave a response 200 and it's not correct
    public function testUserMeWithUserIsLogout()
    {
        $email = 'password@password.com';

        factory(UserRepository::$model)->create([
            'email' => $email,
            'password' => password($email),
            'verified' => 1
        ]);

        $response = $this->json('post', '/v1/users/login', [
            'email' => $email,
            'password' => $email
        ]);

        $token = json_decode($response->response->getContent(), true)['token'];

        $response = $this->json('delete', '/v1/users/me/login', [], [
            'Authorization' => "Bearer {$token}"
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());

        $response = $this->json('get', '/v1/users/me', [], [
            'Authorization' => "Bearer {$token}"
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());
    }
}
