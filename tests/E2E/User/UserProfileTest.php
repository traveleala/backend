<?php

namespace Tests;

use App\Repositories\v1\User\ProfileRepository;
use Illuminate\Http\UploadedFile;
use App\Repositories\v1\Admin\User\UserRepository;

class UserProfileTest extends TestCase
{
    public function testWithoutToken()
    {
        $response = $this->json('put', '/v1/users/me/profile', [], [
            'Authorization' => ''
        ]);

        $this->assertEquals(401, $response->response->getStatusCode());
    }

    public function testCreateNewProfileWithEmptyValues()
    {
        $user = factory(UserRepository::$model)->create();
        $token = login($user->id);

        $response = $this->json('put', '/v1/users/me/profile', [
            'full_name' => '',
            'username' => '',
            'profile_image' => '',
            'portrait_image' => ''
        ], [
            'Authorization' => "Bearer {$token}"
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testUpdateProfileWithEmptyValues()
    {
        $response = $this->json('put', '/v1/users/me/profile', [
            'full_name' => '',
            'username' => ''
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testUpdateProfileWithDuplicatedUsername()
    {
        $user = factory(UserRepository::$model)->create();
        factory(ProfileRepository::$model)->create([
            'username' => 'radarada',
            'user_id' => $user->id
        ]);

        $response = $this->json('put', '/v1/users/me/profile', [
            'full_name' => 'jack',
            'username' => 'radarada'
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testUpdateProfileWithAnotherUsernameLessThanWeek()
    {
        $response = $this->json('put', '/v1/users/me/profile', [
            'full_name' => 'jack',
            'username' => 'radarada'
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testProfileWithValidDataWhenYouHaveProfile()
    {
        $response = $this->json('put', '/v1/users/me/profile', [
            'full_name' => 'jack',
            'username' => 'traveleala'
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());
    }

    // This doesn't work because i can't send a file with put
    // public function testCreateProfileWithValidDataWhenYouDontHaveAnyProfile()
    // {
    //     $user = factory(UserRepository::$model)->create();
    //     $token = login($user->id);
    //     $profile = new UploadedFile(__DIR__ . '/../../assets/image.jpg', 'image.jpg', 'image/jpeg', null, true);
    //     $portrait = new UploadedFile(__DIR__ . '/../../assets/image.jpg', 'image.jpg', 'image/jpeg', null, true);

    //     $response = $this->json('post', '/v1/users/me/profile', [
    //         'full_name' => 'jack',
    //         'username' => 'jacksparrow',
    //         'profile_image' => $profile,
    //         'portrait_image' => $portrait,
    //         '_method' => 'put'
    //     ], [
    //         'Authorization' => "Bearer {$token}"
    //     ]);

    //     $this->assertEquals(200, $response->response->getStatusCode());
    // }

    public function testGetProfileByUsernameWithoutToken()
    {
        $response = $this->json('get', '/v1/users/traveleala', [], [
            'Authorization' => ''
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());
    }

    public function testGetMyProfile()
    {
        $response = $this->json('get', '/v1/users/me/profile');

        $this->assertEquals(200, $response->response->getStatusCode());
    }

    public function testGetParticularProfileByUsername()
    {
        $response = $this->json('get', '/v1/users/traveleala');

        $this->assertEquals(200, $response->response->getStatusCode());
    }
}
