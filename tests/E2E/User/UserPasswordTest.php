<?php

namespace Tests;

class UserPasswordTest extends TestCase
{
    public function testWithoutToken()
    {
        $response = $this->json('put', '/v1/users/me/password', [], [
            'Authorization' => ''
        ]);

        $this->assertEquals(401, $response->response->getStatusCode());
    }

    public function testChangePasswordWithEmptyValues()
    {
        $response = $this->json('put', '/v1/users/me/password', []);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testChangePasswordWithInvalidValues()
    {
        $response = $this->json('put', '/v1/users/me/password', [
            'password' => 'a',
            'new_password' => 'a',
            'new_password_confirmation' => 'a'
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testChangePasswordWithValidNewButInvalidTheCurrent()
    {
        $response = $this->json('put', '/v1/users/me/password', [
            'password' => 'radaradaone',
            'new_password' => 'radaradaone',
            'new_password_confirmation' => 'radaradaone'
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testChangePasswordWithValidCurrentPasswordButInvalidTheNewPassword()
    {
        $response = $this->json('put', '/v1/users/me/password', [
            'password' => 'info@traveleala.com',
            'new_password' => 'radaradaones',
            'new_password_confirmation' => 'radaradaone'
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testChangePasswordWithValidCurrentPasswordButTheNewPasswordIsEqual()
    {
        $response = $this->json('put', '/v1/users/me/password', [
            'password' => 'info@traveleala.com',
            'new_password' => 'info@traveleala.com',
            'new_password_confirmation' => 'info@traveleala.com'
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testChangePasswordWithValidData()
    {
        $response = $this->json('put', '/v1/users/me/password', [
            'password' => 'info@traveleala.com',
            'new_password' => 'info@traveleala.coma',
            'new_password_confirmation' => 'info@traveleala.coma'
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());
    }

    public function testChangePasswordWithValidDataAndLoginWithTheOldPassword()
    {
        $response = $this->json('put', '/v1/users/me/password', [
            'password' => 'info@traveleala.com',
            'new_password' => 'info@traveleala.coma',
            'new_password_confirmation' => 'info@traveleala.coma'
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());

        $this->json('post', '/v1/users/login', [
            'email' => 'info@traveleala.com',
            'password' => 'info@traveleala.com',
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testChangePasswordWithValidDataAndLoginWithTheNewPassword()
    {
        $response = $this->json('put', '/v1/users/me/password', [
            'password' => 'info@traveleala.com',
            'new_password' => 'info@traveleala.coma',
            'new_password_confirmation' => 'info@traveleala.coma'
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());

        $this->json('post', '/v1/users/login', [
            'email' => 'info@traveleala.com',
            'password' => 'info@traveleala.coma',
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());
    }
}
