<?php

namespace Tests;

use App\Repositories\v1\User\UserRepository;
use App\Events\v1\User\CreatedEvent;
use App\Repositories\v1\User\VerificationRepository;

class UserVerificationTest extends TestCase
{
    public function testVerificationWithEmptyValues()
    {
        $response = $this->json('post', '/v1/users/verification', []);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testVerificationWithNonValidEmail()
    {
        $response = $this->json('post', '/v1/users/verification', [
            'email' => 'jacksparrow'
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testVerificationWithValidEmailButTheUserDoesntExist()
    {
        $response = $this->json('post', '/v1/users/verification', [
            'email' => 'info@travelala.com'
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testUserVerificationWithUserButTheUserIsVerificated()
    {
        $user = factory(UserRepository::$model)->create();

        $response = $this->json('post', '/v1/users/verification', [
            'email' => $user->email
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testUserVerificationWhenTheUserIsValid()
    {
        $this->expectsEvents(CreatedEvent::class);

        $user = factory(UserRepository::$model)->create([
            'verified' => 0
        ]);

        $response = $this->json('post', '/v1/users/verification', [
            'email' => $user->email
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());

        $token = VerificationRepository::getFirstOrFailBy('user_id', $user->id)->token;

        $this->seeInDatabase(VerificationRepository::getTableName(), [
            'token' => $token,
            'user_id' => $user->id
        ]);
    }

    public function testUserVerificationLimitToSend()
    {
        $this->expectsEvents(CreatedEvent::class);

        $user = factory(UserRepository::$model)->create([
            'verified' => 0
        ]);

        for ($i = 0; $i < 5; $i++) {
            $response = $this->json('post', '/v1/users/verification', [
                'email' => $user->email
            ]);

            $this->assertEquals(200, $response->response->getStatusCode());
        }

        $token = VerificationRepository::getFirstOrFailBy('user_id', $user->id)->token;

        $this->seeInDatabase(VerificationRepository::getTableName(), [
            'token' => $token,
            'user_id' => $user->id
        ]);
    }

    public function testUserBreakTheLimitToSend()
    {
        $this->expectsEvents(CreatedEvent::class);

        $user = factory(UserRepository::$model)->create([
            'verified' => 0
        ]);

        for ($i = 0; $i < 5; $i++) {
            $response = $this->json('post', '/v1/users/verification', [
                'email' => $user->email
            ]);

            $this->assertEquals(200, $response->response->getStatusCode());
        }

        $response = $this->json('post', '/v1/users/verification', [
            'email' => $user->email
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());

        $token = VerificationRepository::getFirstOrFailBy('user_id', $user->id)->token;

        $this->seeInDatabase(VerificationRepository::getTableName(), [
            'token' => $token,
            'tries' => 0,
            'user_id' => $user->id
        ]);
    }

    public function testUserSentAllTheEmailsButIsTomorrow()
    {
        $this->expectsEvents(CreatedEvent::class);

        $user = factory(UserRepository::$model)->create([
            'verified' => 0
        ]);

        $date = now()->subDays(2);

        factory(VerificationRepository::$model)->create([
            'created_at' => $date,
            'updated_at' => $date,
            'user_id' => $user->id,
            'tries' => 0
        ]);

        for ($i = 0; $i < 5; $i++) {
            $response = $this->json('post', '/v1/users/verification', [
                'email' => $user->email
            ]);

            $this->assertEquals(200, $response->response->getStatusCode());
        }
    }

    public function testVerificationUser()
    {
        $user = factory(UserRepository::$model)->create([
            'verified' => 0
        ]);

        $verification = factory(VerificationRepository::$model)->create([
            'user_id' => $user->id,
        ]);

        $response = $this->json('put', '/v1/users/verification/' . $verification->token);

        $this->assertEquals(200, $response->response->getStatusCode());

        $this->seeInDatabase(UserRepository::getTableName(), [
            'id' => $user->id,
            'verified' => 1
        ]);
    }

    public function testVerificationUserWithInvalidToken()
    {
        $user = factory(UserRepository::$model)->create([
            'verified' => 0
        ]);

        $verification = factory(VerificationRepository::$model)->create([
            'user_id' => $user->id,
        ]);

        $response = $this->json('put', '/v1/users/verification/tokentokentoken');

        $this->assertEquals(404, $response->response->getStatusCode());
    }
}
