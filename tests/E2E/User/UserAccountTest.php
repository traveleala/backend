<?php

namespace Tests;

class UserAccountTest extends TestCase
{
    public function testWithoutToken()
    {
        $response = $this->json('delete', '/v1/users/me', [], [
            'Authorization' => ''
        ]);

        $this->assertEquals(401, $response->response->getStatusCode());
    }

    public function testDeleteAccountWithNotPassword()
    {
        $response = $this->json('delete', '/v1/users/me');

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testDeleteAccountWithAnInvalidPassword()
    {
        $response = $this->json('delete', '/v1/users/me', [
            'password' => 'radarada'
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testWithValidPassword()
    {
        $response = $this->json('delete', '/v1/users/me', [
            'password' => 'info@traveleala.com'
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());
    }

    public function testUserMeAfterDeleteAccount()
    {
        $email = 'info@traveleala.com';

        $this->json('delete', '/v1/users/me', [
            'password' => 'info@traveleala.com'
        ]);

        $response = $this->json('post', '/v1/users/login', [
            'user' => $email,
            'password' => $email
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    /*
        Check when you delete an account
        
        profile
        reports
        qualifications
        activities
        favorites
        votes
        followers
        followings
    */
}
