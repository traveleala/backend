<?php

namespace Tests;

use App\Repositories\v1\User\UserRepository;
use App\Events\v1\User\CreatedEvent;

class UserRegisterTest extends TestCase
{
    public function testRegisterWithEmptyValues()
    {
        $response = $this->json('post', '/v1/users/register', []);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testWithDummyInput()
    {
        $response = $this->json('post', '/v1/users/register', [
            'email' => 'jacksparrow',
            'password' => 'jacksparrow'
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testWithValidEmailAndInvalidPassword()
    {
        $response = $this->json('post', '/v1/users/register', [
            'email' => $this->faker->email,
            'password' => ''
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testWithValidData()
    {
        $this->expectsEvents(CreatedEvent::class);

        $email = $this->faker->email;

        $response = $this->json('post', '/v1/users/register', [
            'email' => $email,
            'password' => $email
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());

        $this->seeInDatabase(UserRepository::getTableName(), [
            'email' => $email
        ]);
    }

    public function testWithExistingUser()
    {
        $user = factory(UserRepository::$model)->create();

        $response = $this->json('post', '/v1/users/register', [
            'email' => $user->email,
            'password' => $user->email
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testWithDeletedUser()
    {
        $user = factory(UserRepository::$model)->create();
        $email = $user->email;
        $user->delete();

        $response = $this->json('post', '/v1/users/register', [
            'email' => $email,
            'password' => $email
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }
}
