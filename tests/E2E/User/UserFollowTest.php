<?php

namespace Tests;

use App\Repositories\v1\Admin\User\UserRepository;
use App\Repositories\v1\User\FollowRepository;
use App\Repositories\v1\User\ProfileRepository;


class UserFollowTest extends TestCase
{
    public function testWithoutToken()
    {
        $response = $this->json('put', '/v1/users/altenwerth.herminio/follow', [], [
            'Authorization' => ''
        ]);

        $this->assertEquals(401, $response->response->getStatusCode());
    }

    public function testFollowUserWhenItDoesNotExist()
    {
        $response = $this->json('put', '/v1/users/altenwerth.herminio/follow');

        $this->assertEquals(404, $response->response->getStatusCode());
    }

    public function testFollowTheSameUserWhenItHasBeenLoggin()
    {
        $response = $this->json('put', '/v1/users/traveleala/follow');

        $this->assertEquals(404, $response->response->getStatusCode());
    }

    public function testFollowUserWhenItHasDeleted()
    {
        $user = factory(UserRepository::$model)->create();
        $profile = factory(ProfileRepository::$model)->create([
            'user_id' => $user->id
        ]);

        $username = $profile->username;
        $user->delete();

        $response = $this->json('put', "/v1/users/{$username}/follow");

        $this->assertEquals(404, $response->response->getStatusCode());
    }

    public function testFollowUserWhenItIsValid()
    {
        $user = factory(UserRepository::$model)->create();
        $profile = factory(ProfileRepository::$model)->create([
            'user_id' => $user->id
        ]);

        $username = $profile->username;

        $response = $this->json('put', "/v1/users/{$username}/follow");

        $this->assertEquals(200, $response->response->getStatusCode());

        $this->seeInDatabase(FollowRepository::getTableName(), [
            'user_follower_id' => $this->userToken->id,
            'user_following_id' => $profile->user_id
        ]);
    }

    public function testFollowingUserWhenItIsValid()
    {
        $user = factory(UserRepository::$model)->create();
        $profile = factory(ProfileRepository::$model)->create([
            'user_id' => $user->id
        ]);
        $token = login($user->id);

        $response = $this->json('put', "/v1/users/traveleala/follow", [], [
            'Authorization' => "Bearer {$token}"
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());

        $this->seeInDatabase(FollowRepository::getTableName(), [
            'user_follower_id' => $profile->user_id,
            'user_following_id' => $this->userToken->id,
        ]);
    }

    public function testFollowUserWhenTheUserHasNotAProfile()
    {
        $user = factory(UserRepository::$model)->create();
        $token = login($user->id);

        $response = $this->json('put', "/v1/users/traveleala/follow", [], [
            'Authorization' => "Bearer {$token}"
        ]);

        $this->assertEquals(403, $response->response->getStatusCode());
    }
}
