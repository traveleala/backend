<?php

namespace Tests;

use App\Repositories\v1\Activity\ActivityRepository;
use App\Repositories\v1\Activity\ContactRepository;
use App\Repositories\v1\Activity\DifficultyRepository;
use App\Repositories\v1\Favorite\FavoriteRepository;
use App\Repositories\v1\Activity\ImageRepository;
use App\Repositories\v1\Activity\PublishTypeRepository;
use App\Repositories\v1\Activity\StateRepository;
use App\Repositories\v1\Activity\SubscriptionRepository;
use App\Repositories\v1\Activity\TypeRepository;
use App\Repositories\v1\Activity\VisitRepository;
use Illuminate\Http\UploadedFile;
use App\Repositories\v1\Admin\User\UserRepository;
use App\Repositories\v1\Qualification\QualificationRepository;
use App\Values\v1\PublishTypeValues;

class ActivityTest extends TestCase
{
    public function testGetActivitiesByUsername()
    {
        $activities = factory(ActivityRepository::$model, 15)->create([
            'user_id' => $this->userToken->id
        ]);

        foreach ($activities as $activity) {
            factory(ImageRepository::$model, 8)->create([
                'activity_id' => $activity->id
            ]);
        }

        $response = $this->json('get', 'v1/users/traveleala/activities');

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['activities'];

        $this->assertEquals(count($activities), count($responseJson));
    }

    public function testDeleteActivityOfTheUser()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('delete', "v1/users/me/activities/{$id}");

        $this->assertEquals(200, $response->response->getStatusCode());
    }

    public function testDeleteActivityHasBeenDeleted()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);

        $id = encode($activity->id);

        $activity->delete();

        $response = $this->json('delete', "v1/users/me/activities/{$id}");

        $this->assertEquals(404, $response->response->getStatusCode());
    }

    public function testDeleteANonUserActivity()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $user->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('delete', "v1/users/me/activities/{$id}");

        $this->assertEquals(404, $response->response->getStatusCode());
    }

    public function testDeleteActivityWithoutToken()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $user->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('delete', "v1/users/me/activities/{$id}", [], [
            'Authorization' => ''
        ]);

        $this->assertEquals(401, $response->response->getStatusCode());
    }

    public function testGetFavoritesOfTheUser()
    {
        $user = factory(UserRepository::$model)->create();
        $activities = factory(ActivityRepository::$model, 5)->create([
            'user_id' => $user->id
        ]);
        factory(ImageRepository::$model)->create([
            'activity_id' => $activities[0]->id
        ]);

        factory(FavoriteRepository::$model)->create([
            'activity_id' => $activities[0]->id,
            'user_id' => $this->userToken->id
        ]);

        $response = $this->json('get', "v1/users/me/favorites/activities");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['favorites'];

        $this->assertEquals(1, count($responseJson));
    }

    public function testIsAnActivityIsMyFavorite()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $user->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('get', "v1/users/me/favorites/activities?activity_id={$id}");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['favorite'];

        $this->assertFalse($responseJson);

        factory(FavoriteRepository::$model)->create([
            'activity_id' => $activity->id,
            'user_id' => $this->userToken->id
        ]);

        $response = $this->json('get', "v1/users/me/favorites/activities?activity_id={$id}");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['favorite'];

        $this->assertTrue($responseJson);
    }

    public function testAddToMyFavoriteOneActivity()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('put', "v1/activities/{$id}/favorites");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['favorite'];

        $this->assertTrue($responseJson);

        $response = $this->json('put', "v1/activities/{$id}/favorites");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['favorite'];

        $this->assertFalse($responseJson);
    }

    public function testAddToMyFavoriteWithoutToken()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('put', "v1/activities/{$id}/favorites", [], [
            'Authorization' => ''
        ]);

        $this->assertEquals(401, $response->response->getStatusCode());
    }

    public function testAddToMyFavoriteAnActivityDeleted()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);

        $id = encode($activity->id);

        $activity->delete();

        $response = $this->json('put', "v1/activities/{$id}/favorites");

        $this->assertEquals(404, $response->response->getStatusCode());
    }

    public function testUploadImageWithEmptyData()
    {
        $response = $this->json('post', "v1/users/me/activities/images", [
            'image' => ''
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    // public function testUploadImageAndGetTheUrl()
    // {
    //     $image = new UploadedFile(__DIR__ . '/../../assets/image.jpg', 'image.jpg', 'image/jpeg', null, true);

    //     $response = $this->json('post', "v1/activities/images", [
    //         'image' => $image
    //     ]);

    //     $this->assertEquals(200, $response->response->getStatusCode());
    // }

    public function testUploadImageWithoutToken()
    {
        $response = $this->json('post', "v1/users/me/activities/images", [
            'image' => ''
        ], [
            'Authorization' => ''
        ]);

        $this->assertEquals(401, $response->response->getStatusCode());
    }

    public function testDeleteImageWithInvalidData()
    {
        $response = $this->json('delete', "v1/users/me/activities/images", [
            'image' => ""
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testDeleteImage()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);

        $image = factory(ImageRepository::$model)->create([
            'image_url' => 'https://s3.sa-east-1.amazonaws.com/develop-traveleala/activities/FwoNcO5sXDNNeGk0jkhO0DnWCP7EeH91ZGjPT2mq.jpeg',
            'activity_id' => $activity->id
        ]);

        $response = $this->json('delete', "v1/users/me/activities/images", [
            'image' => $image->image_url
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());
    }

    public function testDeleteImagesForADifferentActivity()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $user->id
        ]);

        $image = factory(ImageRepository::$model)->create([
            'image_url' => 'https://s3.sa-east-1.amazonaws.com/develop-traveleala/activities/FwoNcO5sXDNNeGk0jkhO0DnWCP7EeH91ZGjPT2mq.jpeg',
            'activity_id' => $activity->id
        ]);

        $response = $this->json('delete', "v1/users/me/activities/images", [
            'image' => $image->image_url
        ]);

        $this->assertEquals(404, $response->response->getStatusCode());
    }

    public function testDeleteImageWithoutToken()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $user->id
        ]);

        $image = factory(ImageRepository::$model)->create([
            'image_url' => 'https://s3.sa-east-1.amazonaws.com/develop-traveleala/activities/FwoNcO5sXDNNeGk0jkhO0DnWCP7EeH91ZGjPT2mq.jpeg',
            'activity_id' => $activity->id
        ]);

        $response = $this->json('delete', "v1/users/me/activities/images", [
            'image' => $image->image_url
        ], [
            'Authorization' => ''
        ]);

        $this->assertEquals(401, $response->response->getStatusCode());
    }

    public function testGetMyActivities()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);

        factory(ImageRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        factory(SubscriptionRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        $response = $this->json('get', "v1/users/me/activities");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['activities'];

        $this->assertEquals(1, count($responseJson));
    }

    public function testGetMyActivitiesWithDeletedActivities()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);

        factory(ImageRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        factory(SubscriptionRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        $activity->delete();

        $response = $this->json('get', "v1/users/me/activities");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['activities'];

        $this->assertEquals(0, count($responseJson));
    }

    public function testGetMyActivitiesWithoutToken()
    {
        $response = $this->json('get', "v1/users/me/activities", [], [
            'Authorization' => ''
        ]);

        $this->assertEquals(401, $response->response->getStatusCode());
    }

    public function testGetTypeOfPublishes()
    {
        $response = $this->json('get', "v1/users/me/activities/publishes");

        $this->assertEquals(200, $response->response->getStatusCode());
    }

    public function testGetTypeOfPublishesWithoutToken()
    {
        $response = $this->json('get', "v1/users/me/activities/publishes", [], [
            'Authorization' => ''
        ]);

        $this->assertEquals(401, $response->response->getStatusCode());
    }

    public function testGetRecentActivities()
    {
        $activities = factory(ActivityRepository::$model, 10)->create([
            'user_id' => $this->userToken->id
        ]);

        foreach ($activities as $activity) {
            factory(ImageRepository::$model)->create([
                'activity_id' => $activity->id
            ]);

            factory(SubscriptionRepository::$model)->create([
                'activity_id' => $activity->id
            ]);
        }

        $response = $this->json('get', "v1/activities/recents");

        $this->assertEquals(200, $response->response->getStatusCode());


        $responseJson = json_decode($response->response->getContent(), true)['activities'];

        $this->assertEquals(10, count($responseJson));
    }

    public function testGetRelatedActivitiesOfActivity()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);

        factory(ImageRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        $id = encode($activity->id);

        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
            'location' => $activity->location
        ]);

        factory(ImageRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        $response = $this->json('get', "v1/activities/{$id}/relateds");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['activities'];

        $this->assertEquals(1, count($responseJson));
    }

    public function testGetRelatedActivitiesOfActivityWithoutRelated()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);

        factory(ImageRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        $id = encode($activity->id);

        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
            'location' => 'test'
        ]);

        $response = $this->json('get', "v1/activities/{$id}/relateds");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['activities'];

        $this->assertEquals(0, count($responseJson));
    }

    public function testGetRelatedActivitiesOfActivityWhenTheActivityIsDeleted()
    {
        $activityOne = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);

        factory(ImageRepository::$model)->create([
            'activity_id' => $activityOne->id
        ]);

        $id = encode($activityOne->id);

        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
            'location' => $activityOne->location
        ]);

        factory(ImageRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        $activityOne->delete();

        $response = $this->json('get', "v1/activities/{$id}/relateds");

        $this->assertEquals(404, $response->response->getStatusCode());
    }

    public function testSearchActivityByLocation()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);

        factory(ImageRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        factory(SubscriptionRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        $response = $this->json('get', "v1/activities?location={$activity->location}");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['activities'];

        $this->assertEquals(1, count($responseJson));
    }

    public function testSearchActivityByTitle()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);

        factory(ImageRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        factory(SubscriptionRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        $response = $this->json('get', "v1/activities?title={$activity->title}");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['activities'];

        $this->assertEquals(1, count($responseJson));
    }

    public function testSearchActivityByPrice()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
            'price' => 0
        ]);

        factory(ImageRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        factory(SubscriptionRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        $response = $this->json('get', "v1/activities?minPrice=0&maxPrice=0");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['activities'];

        $this->assertEquals(1, count($responseJson));

        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
            'price' => 10
        ]);

        factory(ImageRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        factory(SubscriptionRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        $response = $this->json('get', "v1/activities?minPrice=9&maxPrice=11");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['activities'];

        $this->assertEquals(1, count($responseJson));

        $response = $this->json('get', "v1/activities?minPrice=5");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['activities'];

        $this->assertEquals(1, count($responseJson));

        $response = $this->json('get', "v1/activities?maxPrice=11");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['activities'];

        $this->assertEquals(2, count($responseJson));
    }

    public function testSearchActivityByDistance()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
            'latitude' => '-32.347710',
            'longitude' => '-65.015018',
        ]);

        factory(ImageRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        factory(SubscriptionRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        $response = $this->json('get', "v1/activities?coordinates=-32.349621,-64.979662&distance=10");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['activities'];

        $this->assertEquals(1, count($responseJson));

        $response = $this->json('get', "v1/activities?coordinates=-32.349621,-64.979662&distance=1");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['activities'];

        $this->assertEquals(0, count($responseJson));
    }

    public function testSearchActivityByLandscape()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
            'is_landscape' => true,
        ]);

        factory(ImageRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        factory(SubscriptionRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        $response = $this->json('get', "v1/activities?landscape=true");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['activities'];

        $this->assertEquals(1, count($responseJson));

        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
            'is_landscape' => false,
        ]);

        factory(ImageRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        factory(SubscriptionRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        $response = $this->json('get', "v1/activities?landscape=false");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['activities'];

        $this->assertEquals(1, count($responseJson));

        $response = $this->json('get', "v1/activities");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['activities'];

        $this->assertEquals(2, count($responseJson));
    }

    public function testGetActivityById()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
            'is_landscape' => true,
        ]);

        factory(ImageRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        factory(SubscriptionRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        factory(ContactRepository::$model)->create([
            'activity_id' => $activity->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('get', "v1/activities/{$id}");

        $this->assertEquals(200, $response->response->getStatusCode());
    }

    public function testGetActivityDeletedById()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
            'is_landscape' => true,
        ]);

        $id = encode($activity->id);

        $activity->delete();

        $response = $this->json('get', "v1/activities/{$id}");

        $this->assertEquals(404, $response->response->getStatusCode());
    }

    public function testGetActivityNotAvailable()
    {
        $state = StateRepository::getPaymentPending();

        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
            'activity_state_id' => $state->id,
        ]);

        $id = encode($activity->id);

        $response = $this->json('get', "v1/activities/{$id}");

        $this->assertEquals(404, $response->response->getStatusCode());
    }

    public function testStepsWithoutToken()
    {
        $response = $this->json('post', "v1/users/me/activities/steps/1", [], [
            'Authorization' => ''
        ]);

        $this->assertEquals(401, $response->response->getStatusCode());
    }

    public function testStepsWithoutData()
    {
        $response = $this->json('post', "v1/users/me/activities/steps/1");

        $this->assertEquals(400, $response->response->getStatusCode());

        $response = $this->json('post', "v1/users/me/activities/steps/2");

        $this->assertEquals(400, $response->response->getStatusCode());

        $response = $this->json('post', "v1/users/me/activities/steps/3");

        $this->assertEquals(400, $response->response->getStatusCode());

        $response = $this->json('post', "v1/users/me/activities/steps/4");

        $this->assertEquals(400, $response->response->getStatusCode());

        $response = $this->json('post', "v1/users/me/activities/steps/5");

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testStoreStep1()
    {
        $response = $this->json('post', "v1/users/me/activities/steps/1", [
            'title' => 'title',
            'description' => 'bum bum bum bum',
            'price' => '',
            'characteristics' => [
                [
                    'concept' => '',
                    'value' => ''
                ]
            ],
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());

        $activityDifficultyName = DifficultyRepository::getFirstOrFailBy('id', 1)->name;
        $activityTypeName = TypeRepository::getFirstOrFailBy('id', 1)->name;

        $response = $this->json('post', "v1/users/me/activities/steps/1", [
            'title' => 'title',
            'activity_difficulty' => $activityDifficultyName,
            'activity_type' => $activityTypeName
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());

        $response = $this->json('post', "v1/users/me/activities/steps/1", [
            'title' => 'title',
            'description' => 'bum bum bum bum',
            'price' => '',
            'characteristics' => [
                [
                    'concept' => 'buum',
                    'value' => 'aaa'
                ]
            ],
            'activity_difficulty' => $activityDifficultyName,
            'activity_type' => $activityTypeName
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());
    }

    public function testStoreStep2()
    {
        $response = $this->json('post', "v1/users/me/activities/steps/2", [
            'latitude' => '-29.109104',
            'longitude' => '-70.451188',
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());

        $response = $this->json('post', "v1/users/me/activities/steps/2", [
            'latitude' => '-29.542078',
            'longitude' => '-69.451931',
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());
    }

    public function testStoreStep3()
    {
        $response = $this->json('post', "v1/users/me/activities/steps/3", [
            'images' => [
                'https://i.picsum.photos/id/50/536/354.jpg',
                'https://i.picsum.photos/id/50/536/354.jpg',
                'https://i.picsum.photos/id/50/536/354.jpg',
                'https://i.picsum.photos/id/50/536/354.jpg',
                'https://i.picsum.photos/id/50/536/354.jpg',
                'https://i.picsum.photos/id/50/536/354.jpg',
                'https://i.picsum.photos/id/50/536/354.jpg',
                'https://i.picsum.photos/id/50/536/354.jpg'
            ]
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());

        $response = $this->json('post', "v1/users/me/activities/steps/3", [
            'images' => [
                'https://s3.sa-east-1.amazonaws.com/develop-traveleala/activities/FwoNcO5sXDNNeGk0jkhO0DnWCP7EeH91ZGjPT2mq.jpeg',
                'https://s3.sa-east-1.amazonaws.com/develop-traveleala/activities/FwoNcO5sXDNNeGk0jkhO0DnWCP7EeH91ZGjPT2mq.jpeg',
                'https://s3.sa-east-1.amazonaws.com/develop-traveleala/activities/FwoNcO5sXDNNeGk0jkhO0DnWCP7EeH91ZGjPT2mq.jpeg',
                'https://s3.sa-east-1.amazonaws.com/develop-traveleala/activities/FwoNcO5sXDNNeGk0jkhO0DnWCP7EeH91ZGjPT2mq.jpeg',
                'https://s3.sa-east-1.amazonaws.com/develop-traveleala/activities/FwoNcO5sXDNNeGk0jkhO0DnWCP7EeH91ZGjPT2mq.jpeg',
                'https://s3.sa-east-1.amazonaws.com/develop-traveleala/activities/FwoNcO5sXDNNeGk0jkhO0DnWCP7EeH91ZGjPT2mq.jpeg',
                'https://s3.sa-east-1.amazonaws.com/develop-traveleala/activities/FwoNcO5sXDNNeGk0jkhO0DnWCP7EeH91ZGjPT2mq.jpeg',
                'https://s3.sa-east-1.amazonaws.com/develop-traveleala/activities/FwoNcO5sXDNNeGk0jkhO0DnWCP7EeH91ZGjPT2mq.jpeg'
            ]
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());
    }

    public function testStoreStep4()
    {
        $response = $this->json('post', "v1/users/me/activities/steps/4", [
            'phone_number' => '',
            'telephone_number' => '',
            'facebook_url' => '',
            'instagram_url' => '',
            'email' => '',
            'web' => ''
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());

        $response = $this->json('post', "v1/users/me/activities/steps/4", [
            'phone_number' => '',
            'telephone_number' => '',
            'facebook_url' => 'www.google.com.ar',
            'instagram_url' => 'www.google.com.ar',
            'email' => '',
            'web' => ''
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());

        $response = $this->json('post', "v1/users/me/activities/steps/4", [
            'phone_number' => '46503734',
            'telephone_number' => '1152594466',
            'facebook_url' => 'www.facebook.com/traveleala',
            'instagram_url' => 'instagram.com/traveleala',
            'email' => 'info@traveleala.com',
            'web' => 'www.google.com.ar'
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());
    }

    public function testStoreStep5()
    {
        $response = $this->json('post', "v1/users/me/activities/steps/5", [
            'publish_type' => '',
            'month' => '',
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());

        $publishTypeName = PublishTypeRepository::getByName(PublishTypeValues::BASIC_TYPE)->name;

        $response = $this->json('post', "v1/users/me/activities/steps/5", [
            'publish_type' => $publishTypeName,
            'month' => '14',
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());

        $response = $this->json('post', "v1/users/me/activities/steps/5", [
            'publish_type' => $publishTypeName,
            'month' => '1',
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());
    }

    public function testStoreAll()
    {
        $publishTypeName = PublishTypeRepository::getByName(PublishTypeValues::BASIC_TYPE)->name;
        $this->createActivity($publishTypeName);

        $publishTypeName = PublishTypeRepository::getByName(PublishTypeValues::STANDARD_TYPE)->name;
        $this->createActivity($publishTypeName, 2);

        $publishTypeName = PublishTypeRepository::getByName(PublishTypeValues::PREMIUM_TYPE,)->name;
        $this->createActivity($publishTypeName, 6);

        $publishTypeName = PublishTypeRepository::getByName(PublishTypeValues::PREMIUM_TYPE,)->name;
        $this->createActivity($publishTypeName, 2, 400);
    }

    public function testStoreWithoutToken()
    {
        $response = $this->json('post', "v1/users/me/activities", [], [
            'Authorization' => ''
        ]);

        $this->assertEquals(401, $response->response->getStatusCode());
    }

    public function testPayActivity()
    {
        $stateId = StateRepository::getPaymentPending()->id;

        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
            'activity_state_id' => $stateId
        ]);

        factory(SubscriptionRepository::$model)->create([
            'activity_id' => $activity->id,
            'paid_out' => 0,
            'amount' => '1'
        ]);

        $id = encode($activity->id);

        $response = $this->json('post', "v1/users/me/activities/{$id}/subscriptions/pay");

        $this->assertEquals(200, $response->response->getStatusCode());
    }

    public function testPayActivityWithoutToken()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
        ]);

        $id = encode($activity->id);

        $response = $this->json('post', "v1/users/me/activities/{$id}/subscriptions/pay", [], [
            'Authorization' => ''
        ]);

        $this->assertEquals(401, $response->response->getStatusCode());
    }

    public function testPayActivityFree()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
        ]);

        $id = encode($activity->id);

        $response = $this->json('post', "v1/activities/{$id}/subscriptions/pay");

        $this->assertEquals(404, $response->response->getStatusCode());
    }

    public function testPayOtherActivity()
    {
        $stateId = StateRepository::getPaymentPending()->id;
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $user->id,
            'activity_state_id' => $stateId
        ]);

        factory(SubscriptionRepository::$model)->create([
            'activity_id' => $activity->id,
            'paid_out' => 0,
            'amount' => '1'
        ]);

        $id = encode($activity->id);

        $response = $this->json('post', "v1/activities/{$id}/subscriptions/pay");

        $this->assertEquals(404, $response->response->getStatusCode());
    }

    public function testExecutionCommandTopActivities()
    {
        $result = $this->artisan('top:activities');

        $this->assertEquals($result, 0);
    }

    public function getTopActivities()
    {
    }

    public function testUsernameByActivityId()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
        ]);

        $id = encode($activity->id);

        $response = $this->json('get', "v1/activities/{$id}/users");

        $this->assertEquals(200, $response->response->getStatusCode());
    }

    public function testUsernameByActivityIdDeleted()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
        ]);

        $id = encode($activity->id);

        $activity->delete();

        $response = $this->json('get', "v1/activities/{$id}/users");

        $this->assertEquals(404, $response->response->getStatusCode());
    }

    public function testGetTotalVisitsOfTheAllActivities()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
        ]);

        factory(VisitRepository::$model, 10)->create([
            'activity_id' => $activity->id
        ]);

        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
        ]);

        factory(VisitRepository::$model, 10)->create([
            'activity_id' => $activity->id
        ]);

        $response = $this->json('get', "v1/users/me/activities/visits/total");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['total'];

        $this->assertEquals(20, $responseJson);
    }

    public function testGetTotalVisitsOfTheAllActivitiesPerYearPerMonth()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
        ]);

        factory(VisitRepository::$model, 10)->create([
            'activity_id' => $activity->id,
            'created_at' => now()
        ]);

        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
        ]);

        factory(VisitRepository::$model, 5)->create([
            'activity_id' => $activity->id,
            'created_at' => now()->subMonth()
        ]);

        $year = now()->format('Y');

        $response = $this->json('get', "v1/users/me/activities/visits/{$year}/per-month");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['visits'];

        $this->assertEquals(5, $responseJson[0]['total']);
        $this->assertEquals(now()->subMonth()->format('m'), '0' . $responseJson[0]['month']);
        $this->assertEquals(10, $responseJson[1]['total']);
        $this->assertEquals(now()->format('m'), '0' . $responseJson[1]['month']);
    }

    public function testGetTotalVisitsOfActivityOfUser()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
        ]);

        factory(SubscriptionRepository::$model)->create([
            'amount' => '1',
            'activity_id' => $activity->id
        ]);

        factory(VisitRepository::$model, 10)->create([
            'activity_id' => $activity->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('get', "v1/users/me/activities/{$id}/visits/total");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['total'];

        $this->assertEquals(10, $responseJson);
    }

    public function testGetTotalVisitsOfNonActivityUser()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $user->id,
        ]);

        factory(SubscriptionRepository::$model)->create([
            'amount' => '1',
            'activity_id' => $activity->id
        ]);

        factory(VisitRepository::$model, 10)->create([
            'activity_id' => $activity->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('get', "v1/users/me/activities/{$id}/visits/total");

        $this->assertEquals(404, $response->response->getStatusCode());
    }

    public function testGetTotalVisitsOfActivityOfUserPerYearPerMonth()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
        ]);

        factory(VisitRepository::$model, 10)->create([
            'activity_id' => $activity->id,
            'created_at' => now()
        ]);

        factory(SubscriptionRepository::$model)->create([
            'amount' => '1',
            'activity_id' => $activity->id
        ]);

        $id = encode($activity->id);

        $year = now()->format('Y');

        $response = $this->json('get', "v1/users/me/activities/{$id}/visits/{$year}/per-month");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['visits'];

        $this->assertEquals(10, $responseJson[0]['total']);
        $this->assertEquals(now()->format('m'), '0' . $responseJson[0]['month']);
    }

    public function testGetTotalQualificationsOfActivityOfUser()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
        ]);

        factory(SubscriptionRepository::$model)->create([
            'amount' => '1',
            'activity_id' => $activity->id
        ]);

        factory(QualificationRepository::$model, 10)->create([
            'activity_id' => $activity->id,
            'user_id' => $this->userToken->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('get', "v1/users/me/activities/{$id}/qualifications/total");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['total'];

        $this->assertEquals(10, $responseJson);
    }

    public function testGetTotalQualificationsOfNonActivityUser()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $user->id,
        ]);

        factory(SubscriptionRepository::$model)->create([
            'amount' => '1',
            'activity_id' => $activity->id
        ]);

        factory(QualificationRepository::$model, 10)->create([
            'activity_id' => $activity->id,
            'user_id' => $this->userToken->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('get', "v1/users/me/activities/{$id}/qualifications/total");

        $this->assertEquals(404, $response->response->getStatusCode());
    }

    public function testGetTotalQualificationsOfActivityOfUserPerYearPerMonth()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
        ]);

        factory(SubscriptionRepository::$model)->create([
            'amount' => '1',
            'activity_id' => $activity->id
        ]);

        factory(QualificationRepository::$model, 10)->create([
            'activity_id' => $activity->id,
            'created_at' => now(),
            'user_id' => $this->userToken->id,
        ]);

        factory(QualificationRepository::$model, 5)->create([
            'activity_id' => $activity->id,
            'user_id' => $this->userToken->id,
            'created_at' => now()->subMonth()
        ]);

        $id = encode($activity->id);

        $year = now()->format('Y');

        $response = $this->json('get', "v1/users/me/activities/{$id}/qualifications/{$year}/per-month");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['qualifications'];

        $this->assertEquals(5, $responseJson[0]['total']);
        $this->assertEquals(now()->subMonth()->format('m'), '0' . $responseJson[0]['month']);
        $this->assertEquals(10, $responseJson[1]['total']);
        $this->assertEquals(now()->format('m'), '0' . $responseJson[1]['month']);
    }

    public function testGetTotalQualificationOfActivityOfUser()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
        ]);

        factory(SubscriptionRepository::$model)->create([
            'amount' => '1',
            'activity_id' => $activity->id
        ]);

        factory(QualificationRepository::$model, 10)->create([
            'activity_id' => $activity->id,
            'user_id' => $this->userToken->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('get', "v1/users/me/activities/{$id}/qualification/total");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['total'];
    }

    public function testGetTotalQualificationOfNonActivityUser()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $user->id,
        ]);

        factory(SubscriptionRepository::$model)->create([
            'amount' => '1',
            'activity_id' => $activity->id
        ]);

        factory(QualificationRepository::$model, 10)->create([
            'activity_id' => $activity->id,
            'user_id' => $this->userToken->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('get', "v1/users/me/activities/{$id}/qualification/total");

        $this->assertEquals(404, $response->response->getStatusCode());
    }

    public function testGetTotalQualificationOfActivityOfUserPerYearPerMonth()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id,
        ]);

        factory(SubscriptionRepository::$model)->create([
            'amount' => '1',
            'activity_id' => $activity->id
        ]);

        factory(QualificationRepository::$model, 10)->create([
            'activity_id' => $activity->id,
            'created_at' => now(),
            'user_id' => $this->userToken->id,
        ]);

        factory(QualificationRepository::$model, 5)->create([
            'activity_id' => $activity->id,
            'user_id' => $this->userToken->id,
            'created_at' => now()->subMonth()
        ]);

        $id = encode($activity->id);

        $year = now()->format('Y');

        $response = $this->json('get', "v1/users/me/activities/{$id}/qualification/{$year}/per-month");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['qualifications'];

        $this->assertEquals(now()->subMonth()->format('m'), '0' . $responseJson[0]['month']);
        $this->assertEquals(now()->format('m'), '0' . $responseJson[1]['month']);
    }

    private function createActivity($publishTypeName, $month = 1, $status = 200)
    {
        $activityDifficultyName = DifficultyRepository::getFirstOrFailBy('id', 1)->name;
        $activityTypeName = TypeRepository::getFirstOrFailBy('id', 1)->name;

        $response = $this->json('post', "v1/users/me/activities", [
            'title' => 'esto es un ejemplo',
            'description' => 'hola hola hola',
            'price' => '123',
            'characteristics' => [
                [
                    'concept' => 'rada',
                    'value' => 'rada',
                ]
            ],
            'activity_difficulty' => $activityDifficultyName,
            'activity_type' => $activityTypeName,
            'latitude' => '-32.348181',
            'longitude' => '-65.010255',
            'images' => [
                'https://s3.sa-east-1.amazonaws.com/develop-traveleala/activities/FwoNcO5sXDNNeGk0jkhO0DnWCP7EeH91ZGjPT2mq.jpeg', 'https://s3.sa-east-1.amazonaws.com/develop-traveleala/activities/FwoNcO5sXDNNeGk0jkhO0DnWCP7EeH91ZGjPT2mq.jpeg', 'https://s3.sa-east-1.amazonaws.com/develop-traveleala/activities/FwoNcO5sXDNNeGk0jkhO0DnWCP7EeH91ZGjPT2mq.jpeg', 'https://s3.sa-east-1.amazonaws.com/develop-traveleala/activities/FwoNcO5sXDNNeGk0jkhO0DnWCP7EeH91ZGjPT2mq.jpeg', 'https://s3.sa-east-1.amazonaws.com/develop-traveleala/activities/FwoNcO5sXDNNeGk0jkhO0DnWCP7EeH91ZGjPT2mq.jpeg', 'https://s3.sa-east-1.amazonaws.com/develop-traveleala/activities/FwoNcO5sXDNNeGk0jkhO0DnWCP7EeH91ZGjPT2mq.jpeg', 'https://s3.sa-east-1.amazonaws.com/develop-traveleala/activities/FwoNcO5sXDNNeGk0jkhO0DnWCP7EeH91ZGjPT2mq.jpeg', 'https://s3.sa-east-1.amazonaws.com/develop-traveleala/activities/FwoNcO5sXDNNeGk0jkhO0DnWCP7EeH91ZGjPT2mq.jpeg'
            ],
            'phone_number' => '01146503734',
            'telephone_number' => '5491173603479',
            'facebook_url' => 'https://facebook.com/turismoalternativomerlo/?business_id=258098021843676&ref=bookmarks',
            'instagram_url' => 'https://www.instagram.com/explore/tags/bike/?hl=es',
            'email' => 'fasfas41241@hotmail.com',
            'publish_type' => $publishTypeName,
            'month' => $month
        ]);

        $this->assertEquals($status, $response->response->getStatusCode());

        if ($status == 200) {
            $responseJson = json_decode($response->response->getContent(), true)['activity'];

            $this->seeInDatabase(ActivityRepository::getTableName(), [
                'id' => decode($responseJson)
            ]);
        }
    }
}
