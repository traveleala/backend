<?php

namespace Tests;

use App\Repositories\v1\Activity\ActivityRepository;
use App\Repositories\v1\Admin\Report\ReportRepository;
use App\Values\v1\ReportTypeValues;
use App\Repositories\v1\Admin\User\UserRepository;
use App\Repositories\v1\Qualification\QualificationRepository;

class ReportTest extends TestCase
{
    public function testWithoutToken()
    {
        $response = $this->json('get', 'v1/reports/types', [], [
            'Authorization' => ''
        ]);

        $this->assertEquals(401, $response->response->getStatusCode());
    }

    public function testReportActivityWithoutToken()
    {
        $response = $this->json('post', "v1/activities/12/reports", [], [
            'Authorization' => ''
        ]);

        $this->assertEquals(401, $response->response->getStatusCode());
    }

    public function testReportQualificationWithoutToken()
    {
        $response = $this->json('post', "v1/qualifications/12/reports", [], [
            'Authorization' => ''
        ]);

        $this->assertEquals(401, $response->response->getStatusCode());
    }

    public function testGetTypesToReport()
    {
        $response = $this->json('get', 'v1/reports/types');

        $this->assertEquals(200, $response->response->getStatusCode());
    }

    public function testReportActivityWithoutUsernameSetted()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);

        $user = factory(UserRepository::$model)->create();
        $token = login($user->id);

        $id = encode($activity->id);

        $response = $this->json('post', "v1/activities/{$id}/reports", [
            'comment' => 'buum',
            'report_type' => ReportTypeValues::CONTENT_DUPLICATED_TYPE
        ], [
            'Authorization' => "Bearer {$token}"
        ]);

        $this->assertEquals(403, $response->response->getStatusCode());
    }

    public function testReportActivityWithEmptyData()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $user->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('post', "v1/activities/{$id}/reports");

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testReportMyActivity()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('post', "v1/activities/{$id}/reports");

        $this->assertEquals(404, $response->response->getStatusCode());
    }

    public function testReportValidActivity()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $user->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('post', "v1/activities/{$id}/reports", [
            'comment' => 'buum',
            'report_type' => ReportTypeValues::CONTENT_DUPLICATED_TYPE
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());

        $this->seeInDatabase(ReportRepository::getTableName(), [
            'comment' => 'buum',
            'user_id' => $this->userToken->id,
            'activity_id' => $activity->id
        ]);
    }

    public function testReportActivityTwoTimes()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $user->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('post', "v1/activities/{$id}/reports", [
            'comment' => 'buum',
            'report_type' => ReportTypeValues::CONTENT_DUPLICATED_TYPE
        ]);

        $this->seeInDatabase(ReportRepository::getTableName(), [
            'comment' => 'buum',
            'user_id' => $this->userToken->id,
            'activity_id' => $activity->id
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());

        $response = $this->json('post', "v1/activities/{$id}/reports", [
            'comment' => 'buum',
            'report_type' => ReportTypeValues::CONTENT_DUPLICATED_TYPE
        ]);

        $this->assertEquals(429, $response->response->getStatusCode());
    }

    public function testReportDeletedActivity()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $user->id
        ]);

        $activity->delete();

        $id = encode($activity->id);

        $response = $this->json('post', "v1/activities/{$id}/reports", [
            'comment' => 'buum',
            'report_type' => ReportTypeValues::CONTENT_DUPLICATED_TYPE
        ]);

        $this->assertEquals(404, $response->response->getStatusCode());
    }

    public function testReportActivityWithInvalidReportType()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $user->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('post', "v1/activities/{$id}/reports", [
            'comment' => 'buum',
            'report_type' => "buum"
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testReportActivityWithValidReportTypeAndNullableComment()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $user->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('post', "v1/activities/{$id}/reports", [
            'report_type' => ReportTypeValues::CONTENT_DUPLICATED_TYPE
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());

        $this->seeInDatabase(ReportRepository::getTableName(), [
            'user_id' => $this->userToken->id,
            'activity_id' => $activity->id
        ]);
    }

    public function testReportMyQualification()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);
        $qualification = factory(QualificationRepository::$model)->create([
            'user_id' => $this->userToken->id,
            'activity_id' => $activity->id
        ]);

        $id  = encode($qualification->id);

        $response = $this->json('post', "v1/qualifications/{$id}/reports", [
            'report_type' => ReportTypeValues::CONTENT_DUPLICATED_TYPE
        ]);

        $this->assertEquals(404, $response->response->getStatusCode());
    }

    public function testReportQualificationWhenTheUsernameIsNotSetted()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);
        $qualification = factory(QualificationRepository::$model)->create([
            'user_id' => $this->userToken->id,
            'activity_id' => $activity->id
        ]);

        $id = encode($qualification->id);
        $token = login($user->id);

        $response = $this->json('post', "v1/qualifications/{$id}/reports", [
            'report_type' => ReportTypeValues::CONTENT_DUPLICATED_TYPE
        ], [
            'Authorization' => "Bearer {$token}"
        ]);

        $this->assertEquals(403, $response->response->getStatusCode());
    }

    public function testReportQualificationDeleted()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);
        $qualification = factory(QualificationRepository::$model)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id
        ]);

        $id = encode($qualification->id);

        $qualification->delete();

        $response = $this->json('post', "v1/qualifications/{$id}/reports", [
            'report_type' => ReportTypeValues::CONTENT_DUPLICATED_TYPE
        ]);

        $this->assertEquals(404, $response->response->getStatusCode());
    }

    public function testReportQualificationTwoTimes()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);
        $qualification = factory(QualificationRepository::$model)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id
        ]);

        $id = encode($qualification->id);

        $response = $this->json('post', "v1/qualifications/{$id}/reports", [
            'report_type' => ReportTypeValues::CONTENT_DUPLICATED_TYPE
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());

        $response = $this->json('post', "v1/qualifications/{$id}/reports", [
            'report_type' => ReportTypeValues::CONTENT_DUPLICATED_TYPE
        ]);

        $this->assertEquals(429, $response->response->getStatusCode());
    }

    public function testReportQualificationWhenItIsValid()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);
        $qualification = factory(QualificationRepository::$model)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id
        ]);

        $id = encode($qualification->id);

        $response = $this->json('post', "v1/qualifications/{$id}/reports", [
            'report_type' => ReportTypeValues::CONTENT_DUPLICATED_TYPE
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());
    }
}
