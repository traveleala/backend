<?php

namespace Tests;

use App\Repositories\v1\Activity\ActivityRepository;
use App\Repositories\v1\Admin\User\UserRepository;
use App\Repositories\v1\Qualification\QualificationRepository;
use App\Repositories\v1\Qualification\VoteRepository;
use App\Repositories\v1\User\ProfileRepository;


class QualificationTest extends TestCase
{
    public function testListQualificationsOfActivity()
    {
        $user = factory(UserRepository::$model)->create();
        factory(ProfileRepository::$model)->create([
            'user_id' => $user->id
        ]);
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $user->id
        ]);
        $qualifications = factory(QualificationRepository::$model, 10)->create([
            'activity_id' => $activity->id,
            'user_id' => $user->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('get', "v1/activities/{$id}/qualifications");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['qualifications'];

        $this->assertEquals(count($qualifications), count($responseJson));

        $response = $this->json('get', "v1/activities/{$id}/qualifications", [], [
            'Authorization' => ''
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['qualifications'];

        $this->assertEquals(count($qualifications), count($responseJson));
    }

    public function testListQualificationOfActivityIsDeleted()
    {
        $user = factory(UserRepository::$model)->create();
        factory(ProfileRepository::$model)->create([
            'user_id' => $user->id
        ]);
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $user->id
        ]);
        $qualifications = factory(QualificationRepository::$model, 10)->create([
            'activity_id' => $activity->id,
            'user_id' => $user->id
        ]);

        $activity->delete();

        $id = encode($activity->id);

        $response = $this->json('get', "v1/activities/{$id}/qualifications");

        $this->assertEquals(200, $response->response->getStatusCode());

        $responseJson = json_decode($response->response->getContent(), true)['qualifications'];

        $this->assertEquals(0, count($responseJson));
    }

    public function testQualificationWithoutUsername()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);

        $token = login($user->id);

        $id = encode($activity->id);

        $response = $this->json('post', "v1/activities/{$id}/qualifications", [
            'review' => 'no hay nada que decir',
            'qualification' => '1'
        ], [
            'Authorization' => "Bearer {$token}"
        ]);

        $this->assertEquals(403, $response->response->getStatusCode());
    }

    public function testQualificationWithoutData()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $user->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('post', "v1/activities/{$id}/qualifications", [
            'review' => '',
            'qualification' => ''
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testQualificationWithDataButTheQualificationIsInvalid()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $user->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('post', "v1/activities/{$id}/qualifications", [
            'review' => 'token token',
            'qualification' => '6'
        ]);

        $this->assertEquals(400, $response->response->getStatusCode());
    }

    public function testQualificationActivityWhenIsValid()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $user->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('post', "v1/activities/{$id}/qualifications", [
            'review' => 'token token',
            'qualification' => '5'
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());

        $this->seeInDatabase(QualificationRepository::getTableName(), [
            'review' => 'token token',
            'qualification' => '5'
        ]);
    }

    public function testQualificationActivityWhenIsDeleted()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $user->id
        ]);

        $id = encode($activity->id);

        $activity->delete();

        $response = $this->json('post', "v1/activities/{$id}/qualifications", [
            'review' => 'token token',
            'qualification' => '5'
        ]);

        $this->assertEquals(404, $response->response->getStatusCode());
    }

    public function testQualificationActivityWhenIsMy()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('post', "v1/activities/{$id}/qualifications", [
            'review' => 'token token',
            'qualification' => '5'
        ]);

        $this->assertEquals(404, $response->response->getStatusCode());
    }

    public function testVoteQualificationWhenIsMy()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $user->id
        ]);

        $id = encode($activity->id);

        $response = $this->json('post', "v1/activities/{$id}/qualifications", [
            'review' => 'token token',
            'qualification' => '5'
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());

        $qualification = QualificationRepository::getFirstOrFailBy('activity_id', $activity->id);

        $qualificationId = encode($qualification->id);

        $response = $this->json('put', "v1/qualifications/{$qualificationId}/votes", [
            'vote' => 'down',
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());

        $this->seeInDatabase(VoteRepository::getTableName(), [
            'vote' => '-1',
            'activity_qualification_id' => $activity->id,
            'user_id' => $this->userToken->id
        ]);
    }

    public function testVoteQualificationWhenIsOther()
    {
        $user = factory(UserRepository::$model)->create();
        factory(ProfileRepository::$model)->create([
            'user_id' => $user->id
        ]);
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);

        $id = encode($activity->id);
        $token = login($user->id);

        $response = $this->json('post', "v1/activities/{$id}/qualifications", [
            'review' => 'token token',
            'qualification' => '5'
        ], [
            'Authorization' => "Bearer {$token}"
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());

        $qualification = QualificationRepository::getFirstOrFailBy('activity_id', $activity->id);

        $qualificationId = encode($qualification->id);

        $response = $this->json('put', "v1/qualifications/{$qualificationId}/votes", [
            'vote' => 'up',
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());

        $this->seeInDatabase(VoteRepository::getTableName(), [
            'vote' => '1',
            'activity_qualification_id' => $activity->id,
            'user_id' => $this->userToken->id
        ]);
    }

    public function testVoteQualificationWhenIsDeleted()
    {
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);

        $qualification = factory(QualificationRepository::$model)->create([
            'user_id' => $this->userToken->id,
            'activity_id' => $activity->id
        ]);

        $qualificationId = encode($qualification->id);

        $qualification->delete();

        $response = $this->json('put', "v1/qualifications/{$qualificationId}/votes", [
            'vote' => 'down',
        ]);

        $this->assertEquals(404, $response->response->getStatusCode());
    }

    public function testVoteQualificationWithoutUsername()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);

        $qualification = factory(QualificationRepository::$model)->create([
            'user_id' => $this->userToken->id,
            'activity_id' => $activity->id
        ]);

        $qualificationId = encode($qualification->id);
        $token = login($user->id);

        $response = $this->json('put', "v1/qualifications/{$qualificationId}/votes", [
            'vote' => 'down',
        ], [
            'Authorization' => "Bearer {$token}"
        ]);

        $this->assertEquals(200, $response->response->getStatusCode());
    }

    public function testQualificationsTotalOfTheUser()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);

        $qualifications = factory(QualificationRepository::$model, 30)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id
        ]);

        $response = $this->json('get', "v1/users/me/qualifications/total");

        $this->assertEquals(200, $response->response->getStatusCode());

        $total = json_decode($response->response->getContent(), true)['total'];

        $this->assertEquals($total, count($qualifications));
    }

    public function testQualificationsOfTheYearPerMonthOfTheUser()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);

        $qualifications = factory(QualificationRepository::$model, 30)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id
        ]);

        $year = now()->format('Y');
        $month = now()->format('m');

        $response = $this->json('get', "v1/users/me/qualifications/{$year}/per-month");

        $this->assertEquals(200, $response->response->getStatusCode());

        $statPerMonth = json_decode($response->response->getContent(), true)['qualifications'][0];

        $this->assertEquals($statPerMonth['total'], count($qualifications));
        $this->assertEquals($statPerMonth['month'], $month);

        $qualifications = factory(QualificationRepository::$model, 30)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
            'created_at' => now()->subYear()->format('Y-m-d')
        ]);

        $year = now()->subYear()->format('Y');
        $month = now()->subYear()->format('m');

        $response = $this->json('get', "v1/users/me/qualifications/{$year}/per-month");

        $this->assertEquals(200, $response->response->getStatusCode());

        $statPerMonth = json_decode($response->response->getContent(), true)['qualifications'][0];

        $this->assertEquals($statPerMonth['total'], count($qualifications));
        $this->assertEquals($statPerMonth['month'], $month);
    }

    public function testQualificationTotalOfTheUser()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);

        $qualifications = factory(QualificationRepository::$model, 30)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
            'qualification' => 5
        ]);

        $response = $this->json('get', "v1/users/me/qualification/total");

        $this->assertEquals(200, $response->response->getStatusCode());

        $total = json_decode($response->response->getContent(), true)['total'];

        $this->assertEquals($total, '5,0');
    }

    public function testQualificationOfTheYearPerMonthOfTheUser()
    {
        $user = factory(UserRepository::$model)->create();
        $activity = factory(ActivityRepository::$model)->create([
            'user_id' => $this->userToken->id
        ]);

        $qualifications = factory(QualificationRepository::$model, 30)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
            'qualification' => 5,
        ]);

        $year = now()->format('Y');
        $month = now()->format('m');

        $response = $this->json('get', "v1/users/me/qualification/{$year}/per-month");

        $this->assertEquals(200, $response->response->getStatusCode());

        $statPerMonth = json_decode($response->response->getContent(), true)['qualifications'][0];

        $this->assertEquals($statPerMonth['total'], '5,0');
        $this->assertEquals($statPerMonth['month'], $month);

        $qualifications = factory(QualificationRepository::$model, 30)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
            'created_at' => now()->subYear()->format('Y-m-d'),
            'qualification' => 5
        ]);

        $year = now()->subYear()->format('Y');
        $month = now()->subYear()->format('m');

        $response = $this->json('get', "v1/users/me/qualification/{$year}/per-month");

        $this->assertEquals(200, $response->response->getStatusCode());

        $statPerMonth = json_decode($response->response->getContent(), true)['qualifications'][0];

        $this->assertEquals($statPerMonth['total'], '5,0');
        $this->assertEquals($statPerMonth['month'], $month);
    }
}
