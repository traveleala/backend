# Events

## Event listener

php artisan event:generate

## Create an event

    php artisan make:event :name

## Create a listener

    php artisan make:listener :name

## Create table queue

    php artisan queue:table
    php artisan migrate

# Queue

## Queue work

    php artisan queue:work

# Migrations

## Execute migrations

    php artisan migrate

## Create migration

    php artisan make:migration :name

## Refresh database

    php artisan migrate fresh

## Create an exception

php artisan make:exception :name

## Clean models

php artisan clean:models
